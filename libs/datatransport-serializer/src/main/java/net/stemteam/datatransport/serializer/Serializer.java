package net.stemteam.datatransport.serializer;

import java.beans.Introspector;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * POJO <-> DataTransport (кастомный CSV-подобный формат)
 *
 * @author Andrey Nikolaev
 */
public class Serializer {

    public static final String COLUMN_SEPARATOR = "\t";
    public static final String RECORD_SEPARATOR = "\n";

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd");
    private final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyMMddHHmmss");
    private final String dateTimeFormatMsPattern = "yyMMddHHmmss.SSS";
    private final SimpleDateFormat dateTimeFormatMs = new SimpleDateFormat(dateTimeFormatMsPattern);
    private final SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    private final String timeStampFormatMsPattern = "yyyyMMddHHmmss.SSS";
    private final SimpleDateFormat timeStampFormatMs = new SimpleDateFormat(timeStampFormatMsPattern);

    /**
     * Сериализация коллекций. Для параметризованных коллекций следует указывать класс параметра, например для List&lt;Car&gt; следует указать
     * Car.class
     *
     * @param collection
     * @param collectionParameterClass
     * @return
     * @throws Exception
     */
    public String serializeCollection(Collection collection, Class collectionParameterClass) throws Exception {
        if (collection == null) {
            return "";
        }
        StringBuilder result = new StringBuilder();

        // сериализованная коллекция должна выглядеть таблицей с полями = свойства обьекта, параметризующего коллекцию
        if (collectionParameterClass == null) {
            collectionParameterClass = collection.iterator().next().getClass(); // из первого элемента
        }

        // коллекция|массив может быть параметризована примитивом, в этом случае поле будет одно и с кастомным именем
        if (isPrimitiveClass(collectionParameterClass)) {
            DataColumnType colType = resolveType(collectionParameterClass);
            result.append("arr").append(RECORD_SEPARATOR).append(colType);
            for (Object o : collection) {
                result.append(RECORD_SEPARATOR).append(getAsString(colType, o));
            }
        } else {
            boolean first = true;
            for (Object o : collection) {
                if (!first) {
                    result.append(RECORD_SEPARATOR);
                }
                result.append(serialize(o, collectionParameterClass, first));
                first = false;
            }
        }
        return result.toString();
    }

    /**
     * Сериализация обьектных массивов. Для примитивов int[] и т.д. будет исключение.
     *
     * @param array
     * @return
     * @throws Exception
     */
    public String serializeArray(Object array) throws Exception {
        if (array == null) {
            return "";
        }
        StringBuilder result = new StringBuilder();
        Class collectionParameterClass = array.getClass().getComponentType();
        Object[] arr = (Object[]) array;

        // коллекция|массив может быть параметризована примитивом, в этом случае поле будет одно и с кастомным именем
        if (isPrimitiveClass(collectionParameterClass)) {
            DataColumnType colType = resolveType(collectionParameterClass);
            result.append("arr").append(RECORD_SEPARATOR).append(colType);
            for (Object o : arr) {
                result.append(RECORD_SEPARATOR).append(getAsString(colType, o));
            }
        } else {
            boolean first = true;
            for (Object o : arr) {
                if (!first) {
                    result.append(RECORD_SEPARATOR);
                }
                result.append(serialize(o, collectionParameterClass, first));
                first = false;
            }
        }
        return result.toString();
    }

    /**
     * Сериализация объектов в кастомный Jaxis формат. Для сериализации коллекций и массивов следует использовать serializeArray() и
     *
     * @param obj
     * @return
     * @throws Exception
     */
    public String serialize(Object obj) throws Exception {
        return serialize(obj, null, true);
    }

    /**
     * Сериализация объектов в кастомный Jaxis формат
     *
     * @param obj
     * @param collectionParameterClass
     * @param headers должны ли попадать в сериализованную строку данные по именам и типам полей
     * @return
     * @throws Exception
     */
    private String serialize(Object obj, Class collectionParameterClass, boolean headers) throws Exception {
        if (obj == null) {
            return "";
        }

        StringBuilder columns = new StringBuilder();
        StringBuilder types = new StringBuilder();
        StringBuilder rows = new StringBuilder();
        boolean first = true;

        // может быть передана коллекция или массив, в этом случае не интересны ее методы, интересен лишь класс, которым она параметризована
        if (Collection.class.isAssignableFrom(obj.getClass())) {
            return serializeCollection((Collection) obj, collectionParameterClass);
        } else if (obj.getClass().isArray()) {
            return serializeArray(obj);
        }

        // на сериализацию может быть передан примитив (его не заворачиваем в датасет)
        if (isPrimitiveClass(obj.getClass())) {
            DataColumnType colType = resolveType(obj.getClass());
            getAsString(colType, obj);
        }

        for (Method method : Reflect.getClassMethodsOrderedByDeclaration(obj.getClass())) {
            if (Modifier.isPublic(method.getModifiers())
                    && method.getParameterTypes().length == 0
                    && method.getReturnType() != void.class
                    && (method.getName().startsWith("get") || method.getName().startsWith("is"))) {

                int pref = method.getName().startsWith("is") ? 2 : 3;

                // проверяем что есть сеттер
                String setterName = "set" + method.getName().substring(pref);
                String fieldName = Introspector.decapitalize(method.getName().substring(pref));
                Class<?> fieldClass = method.getReturnType();

                try {
                    Method setter = obj.getClass().getMethod(setterName, fieldClass);
                } catch (NoSuchMethodException ex) {
                    continue;
                }

                if (!first) {
                    if (headers) {
                        columns.append(COLUMN_SEPARATOR);
                        types.append(COLUMN_SEPARATOR);
                    }
                    rows.append(COLUMN_SEPARATOR);
                } else {
                    first = false;
                }

                DataColumnType colType = resolveType(fieldClass); // определяем Jaxis-тип

                if (headers) {
                    columns.append(fieldName);
                    types.append(colType);
                }

                Object value = method.invoke(obj); // значение (вызываем getter)

                // для примитивов сразу получаем значения
                if (!DataColumnType.DataSet.equals(colType)) {
                    rows.append(getAsString(colType, value)); // тру значение
                } else { // рекурсия в датасет
                    String hardValue = null;
                    if (Collection.class.isAssignableFrom(method.getReturnType())) { // для коллекций мы сможем определить тип параметра
                        ParameterizedType pType = (ParameterizedType) method.getGenericReturnType(); // для параметров можно использовать getGenericParameterTypes()
                        Class<?> clazz = (Class<?>) pType.getActualTypeArguments()[0];
                        hardValue = serialize(value, clazz, true);
                    } else {
                        hardValue = serialize(value);
                    }
                    rows.append(Base64.encode(hardValue.getBytes("utf-8")));
                }
            }
        }

        if (headers) {
            columns.append(RECORD_SEPARATOR).append(types).append(RECORD_SEPARATOR).append(rows);
            return columns.toString();
        } else {
            return rows.toString();
        }

    }

    /**
     * Определяет является ли класс примитивом
     *
     * @param c
     * @return
     */
    private boolean isPrimitiveClass(Class c) {
        // проверка 8-ми примитивов java
        if (c.isPrimitive() || c.isEnum()) {
            return true;
        }

        // ссылочные типы
        return c.equals(Boolean.class)
                || c.equals(Double.class)
                || c.equals(Float.class)
                || c.equals(Character.class)
                || c.equals(Integer.class)
                || c.equals(Long.class)
                || c.equals(Byte.class)
                || c.equals(Short.class)
                || c.equals(String.class)
                || Date.class.isAssignableFrom(c);
    }

    /**
     * Определить Jaxis-тип класса
     *
     * @param c
     * @return
     */
    public DataColumnType resolveType(Class c) {
        if (c.isArray() || Collection.class.isAssignableFrom(c)) {
            return DataColumnType.DataSet;
        }
        if (String.class.equals(c) || Character.TYPE == c || Character.class == c || c.isEnum()) {
            return DataColumnType.String;
        }
        if (Long.class.equals(c) || Long.TYPE.equals(c)) {
            return DataColumnType.Int64;
        }
        if (Integer.class.equals(c) || Integer.TYPE.equals(c)) {
            return DataColumnType.Int32;
        }
        if (Short.class.equals(c) || Short.TYPE.equals(c)
                || Byte.class.equals(c) || Byte.TYPE.equals(c)) {
            return DataColumnType.Int16;
        }
        if (Boolean.class.equals(c) || Boolean.TYPE.equals(c)) {
            return DataColumnType.Boolean;
        }
        if (Double.class.equals(c) || Double.TYPE.equals(c)
                || Float.class.equals(c) || Float.TYPE.equals(c)) {
            return DataColumnType.Decimal;
        }
        if (Date.class.equals(c) || Time.class.equals(c) || Timestamp.class.equals(c) || java.sql.Date.class.equals(c)) {
            return DataColumnType.TimeStamp;
        }
        return DataColumnType.DataSet; // POJO
    }

    /**
     * Получить значение примитива в виде строки
     *
     * @param type
     * @param value
     * @return
     */
    private String getAsString(DataColumnType type, Object value) throws UnsupportedEncodingException {
        if (value == null) {
            return "";
        }
        if (DataColumnType.String.equals(type)) {
            return Base64.encode(value.toString().getBytes("utf-8"));
        }
        if (DataColumnType.TimeStamp.equals(type)) {
            if (Date.class.isAssignableFrom(value.getClass())) {
                return timeStampFormatMs.format((Date) value);
            } else {
                return value.toString();
            }
        }
        if (DataColumnType.DateTime.equals(type)) {
            if (Date.class.isAssignableFrom(value.getClass())) {
                return dateTimeFormatMs.format((Date) value);
            } else {
                return value.toString();
            }
        }
        if (DataColumnType.Date.equals(type)) {
            if (Date.class.isAssignableFrom(value.getClass())) {
                return dateFormat.format((Date) value);
            } else {
                return value.toString();
            }
        }
        if (DataColumnType.Int16.equals(type) || DataColumnType.Int32.equals(type) || DataColumnType.Int64.equals(type) || DataColumnType.Decimal.
                equals(type)) {
            return value.toString();
        }
        if (DataColumnType.Boolean.equals(type)) {
            return ((Boolean) value) ? "1" : "0";
        }
        return String.valueOf(value);
    }

    /**
     * Десериализация Jaxis-дататранспорта в POJO для обьекта.
     *
     * @param <T>
     * @param jaxisDataTransport
     * @param clazz
     * @return
     * @throws Exception
     */
    public <T> T deserialize(String jaxisDataTransport, Class<T> clazz) throws Exception {
        if ((null == jaxisDataTransport) || "".equals(jaxisDataTransport)) {
            return null;
        }
        T obj;
        try {
            obj = clazz.newInstance();
        } catch (InstantiationException ex) {
            throw new Exception("Класс " + clazz.getName() + " должен иметь конструктор без аргументов");
        }

        String[] dataList = jaxisDataTransport.split(RECORD_SEPARATOR);
        if (dataList.length < 3) {
            return obj; // нет данных - вернули инстанс объекта без свойств
        } else if (dataList.length > 3) {
            throw new Exception("Слишком много данных для десериализации в объект (является коллекцией?)");
        }

        String[] columnNames = dataList[0].split(COLUMN_SEPARATOR, -1);
        String[] columnTypes = dataList[1].split(COLUMN_SEPARATOR, -1);

        int columnCount = columnNames.length;
        String[] rowString = dataList[2].split(COLUMN_SEPARATOR, -1);
        for (int j = 0; j < columnCount; j++) {
            DataColumnType type = DataColumnType.valueOf(columnTypes[j]);

            String fieldName = columnNames[j];
            String setterName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);

            Field field = Reflect.getFieldWithClassRecurse(clazz, fieldName);
            Method setter = clazz.getMethod(setterName, field.getType());

            Object value;
            if (!DataColumnType.DataSet.equals(type)) {
                value = getValueFromString(type, rowString[j], field.getType()); // TODO возможно следует передавать field.getType(), для уточнения типа (к примеру Int16 => Short, Byte)
            } else {
                String str = null;
                if (rowString[j] != null && !"".equals(rowString[j])) {
                    str = new String(Base64.decode(rowString[j]), "utf-8");
                } else {
                    str = rowString[j];
                }

                if (Collection.class.isAssignableFrom(field.getType())) {
                    ParameterizedType pType = (ParameterizedType) field.getGenericType();
                    Class<?> pClass = (Class<?>) pType.getActualTypeArguments()[0];
                    value = deserializeCollection(str, pClass);
                } else {
                    value = deserialize(str, field.getType());
                }
            }
            setter.invoke(obj, value);
        }

        return obj;
    }

    /**
     * Десериализация примитивов
     *
     * @param type
     * @param value
     * @param typeClass используется для разрешения типов (Short, Byte пакуются в Int32 и т.д.) и нужно вернуть объект правильного типа
     * @return
     * @throws Exception
     */
    private Object getValueFromString(DataColumnType type, String value, Class typeClass) throws Exception {
        if (value == null) {
            return value;
        }

        if (DataColumnType.String.equals(type)) {
            return new String(Base64.decode(value), "utf-8");
        }
        if (DataColumnType.DateTime.equals(type)) {
            // парсер парсит даты весьма особенно
            Date d;
            if (value.contains(".")) {
                // дополняем нулями при необходимости (иначе .6 будет разобрана как 6 мс а не как 600)
                while (value.length() < dateTimeFormatMsPattern.length()) {
                    value += "0";
                }
                d = dateTimeFormatMs.parse(value);
            } else {
                d = dateTimeFormat.parse(value);
            }

            // определим конкретный тип
            if (Timestamp.class.equals(typeClass)) {
                return new Timestamp(d.getTime());
            } else if (java.sql.Date.class.equals(typeClass)) {
                return new java.sql.Date(d.getTime());
            } else if (java.sql.Time.class.equals(typeClass)) {
                return new java.sql.Time(d.getTime());
            } else {
                return d;
            }

        }
        if (DataColumnType.TimeStamp.equals(type)) {

            Date d;
            if (value.contains(".")) {
                // дополняем нулями при необходимости (иначе .6 будет разобрана как 6 мс а не как 600)
                while (value.length() < timeStampFormatMsPattern.length()) {
                    value += "0";
                }
                d = timeStampFormatMs.parse(value);
            } else {
                d = timeStampFormat.parse(value);
            }

            // определим конкретный тип
            if (Timestamp.class.equals(typeClass)) {
                return new Timestamp(d.getTime());
            } else if (java.sql.Date.class.equals(typeClass)) {
                return new java.sql.Date(d.getTime());
            } else if (java.sql.Time.class.equals(typeClass)) {
                return new java.sql.Time(d.getTime());
            } else {
                return d;
            }

        }
        if (DataColumnType.Date.equals(type)) {
            Date d = dateFormat.parse(value);
            
            // определим конкретный тип
            if (Timestamp.class.equals(typeClass)) {
                return new Timestamp(d.getTime());
            } else if (java.sql.Date.class.equals(typeClass)) {
                return new java.sql.Date(d.getTime());
            } else if (java.sql.Time.class.equals(typeClass)) {
                return new java.sql.Time(d.getTime());
            } else {
                return d;
            }
        }
        if (DataColumnType.Int16.equals(type)) {
            if (Short.class.equals(typeClass) || Short.TYPE.equals(typeClass)) {
                return Short.valueOf(value);
            } else {
                return Byte.valueOf(value);
            }
        }
        if (DataColumnType.Int32.equals(type)) {
            return Integer.valueOf(value);
        }
        if (DataColumnType.Int64.equals(type)) {
            return Long.valueOf(value);
        }
        if (DataColumnType.Decimal.equals(type)) {
            if (Double.class.equals(typeClass) || Double.TYPE.equals(typeClass)) {
                return Double.valueOf(value);
            } else {
                return Float.valueOf(value);
            }
        }
        if (DataColumnType.Boolean.equals(type)) {
            return value.equals("1");
        }

        throw new Exception("Неизвестный тип данных: " + type);
    }

    /**
     * Десериализация коллекции
     *
     * @param <T>
     * @param jaxisDataTransport сериализованные данные
     * @param collectionParameterClass тип, параметризцующий коллекцию
     * @return
     * @throws Exception
     */
    public <T> List<T> deserializeCollection(String jaxisDataTransport, Class<T> collectionParameterClass) throws Exception {
        if ((null == jaxisDataTransport) || "".equals(jaxisDataTransport)) {
            return null;
        }
        List<T> result = new ArrayList<>();

        String[] dataList = jaxisDataTransport.split(RECORD_SEPARATOR);
        if (dataList.length < 3) {
            return result; // нет данных - вернули пустую коллекцию
        }

        String headers = dataList[0] + RECORD_SEPARATOR + dataList[1] + RECORD_SEPARATOR; // заголовки

        if (isPrimitiveClass(collectionParameterClass)) {
            DataColumnType colType = resolveType(collectionParameterClass);
            for (int i = 2; i < dataList.length; i++) {
                Object item = getValueFromString(colType, dataList[i], collectionParameterClass);
                result.add((T) item);
            }
        } else {
            // коллекция у нас сериализована в виде таблицы, с полями - свойствами параметризующего ее класса
            for (int i = 2; i < dataList.length; i++) {
                String row = dataList[i];
                Object item = deserialize(headers + row, collectionParameterClass);
                result.add((T) item);
            }
        }
        return result;
    }
}
