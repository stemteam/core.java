/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.serializer;

/**
 * Тип колонки в транспорте
 */
public enum DataColumnType {

    String,
    Int16,
    Int32,
    Int64,
    Decimal,
    Boolean,
    DateTime,
    Date,
    TimeStamp,
    Geometry,
    DataSet;

}
