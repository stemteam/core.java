package net.stemteam.datatransport.serializer;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Andrey Nikolaev
 */
public class Reflect {

    /**
     * Возвращает методы, в порядке определенном в классе (затем в порядке определенном в суперклассе и т.д.). Основывается на том, что начиная с jdk6
     * методы getFields и getDeclaredFields() возвращают поля, отсортированные в порядке определения в классе, хотя документация и утверждает что
     * данная сортировка не гарантируется, но разработчики считают что это достаточно уверенное правило, которое соблюдается в старших версиях jdk и
     * будет соблюдаться в будующем.
     *
     * @param clazz
     * @return
     */
    public static List<Method> getClassMethodsOrderedByDeclaration(Class clazz) {
        List<Method> methods = new ArrayList<>();

        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            // проверим есть ли геттер и сеттер
            String setterName = "set" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
            String getterName;
            if (field.getType().equals(Boolean.TYPE)) { // для boolean примитивов (для класса Boolean должен быть метод get, поскольку он может вернуть null)
                getterName = "is" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
            } else {
                getterName = "get" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
            }

            try {
                Method getter = clazz.getMethod(getterName);
                Method setter = clazz.getMethod(setterName, field.getType());
                methods.add(getter);
            } catch (NoSuchMethodException ex) {
                // это норма (c) Елена Малышева
            }
        }

        // суперкласс
        if (clazz.getSuperclass() != null) {
            List<Method> superMethods = getClassMethodsOrderedByDeclaration(clazz.getSuperclass());
            if (!superMethods.isEmpty()) {
                methods.addAll(superMethods);
            }
        }
        return methods;
    }

    /**
     * Возвращает поле класса по имени (проваливается в суперклассы рекурсией)
     *
     * @param clazz
     * @param fieldName
     * @return
     * @throws NoSuchFieldException
     */
    public static Field getFieldWithClassRecurse(Class clazz, String fieldName) throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException ex) {
            // проваливаемся в суперкласс (рекурсия)
            return getFieldWithClassRecurse(clazz.getSuperclass(), fieldName);
        }
    }

}
