package net.stemteam.datatransport.serializer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Andrey Nikolaev
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SerializerTest {

    private SimpleDateFormat dateTimeFormatMs = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private Car car1;
    private Car car2;

    public SerializerTest() throws ParseException {
        this.car1 = new Car("Лада калина", 90, 1200, 2.1, dateTimeFormatMs.parse("2001-03-08 11:23:35.234"), (short) 3, false);
        car1.setBaseField("azazazazaza");
        this.car2 = new Car("Лада приора", 93, 1150, 2.3, dateTimeFormatMs.parse("2003-02-23 00:00:00.6"), (short) 5, true);
        car2.setBaseField("azazazazaza");
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void test001_SimpleSerialize() throws Exception {
        System.out.println("\ntest001_SimpleSerialize");
        Serializer serializer = new Serializer();
        String s = serializer.serialize(car1);
        System.out.println(s);

        Assert.assertEquals("model	maxSpeed	weight	worldIndex	releaseDate	doorsCount	perfect	baseField\n"
                + "String	Int32	Int32	Decimal	TimeStamp	Int16	Boolean	String\n"
                + "0JvQsNC00LAg0LrQsNC70LjQvdCw	90	1200	2.1	20010308112335.234	3	0	YXphemF6YXphemE=", s);
    }

    @Test
    public void test002_NestedSerialize() throws Exception {
        System.out.println("\ntest002_NestedSerialize");
        CarOwner carOwner = new CarOwner("Иван Петров", car1);
        Serializer serializer = new Serializer();
        String s = serializer.serialize(carOwner);
        System.out.println(s);
    }

    @Test
    public void test003_PrimitiveListSerialize() throws Exception {
        System.out.println("\ntest003_PrimitiveListSerialize");
        List<Integer> cars = new ArrayList<>();
        cars.add(123);
        cars.add(456);
        cars.add(789);
        Serializer serializer = new Serializer();
        String s = serializer.serialize(cars);
        System.out.println(s);
    }

    @Test
    public void test004_ObjectListSerialize() throws Exception {
        System.out.println("\ntest004_ObjectListSerialize");
        List<Car> cars = new ArrayList<>();
        cars.add(car1);
        cars.add(car2);
        Serializer serializer = new Serializer();
        String s = serializer.serializeCollection(cars, Car.class);
        System.out.println(s);
    }

    @Test
    public void test005_ObjectArraySerialize() throws Exception {
        System.out.println("\ntest005_ObjectArraySerialize");
        Car[] cars = new Car[2];
        cars[0] = car1;
        cars[1] = car2;
        Serializer serializer = new Serializer();
        String s = serializer.serialize(cars);
        System.out.println(s);
    }

    @Test
    public void test006_PrimitiveArraySerialize() throws Exception {
        System.out.println("\ntest006_PrimitiveArraySerialize");
        Integer[] cars = new Integer[]{1, 2};
        Serializer serializer = new Serializer();
        String s = serializer.serialize(cars);
        System.out.println(s);
    }

    @Test
    public void test007_ObjectWithNestedCollectionSerialize() throws Exception {
        System.out.println("\ntest007_ObjectWithNestedCollectionSerialize");
        Shop shop = new Shop("автомагазин");
        shop.setCarsForSale(new ArrayList());
        shop.getCarsForSale().add(car1);
        shop.getCarsForSale().add(car2);
        Serializer serializer = new Serializer();
        String s = serializer.serialize(shop);
        System.out.println(s);
    }

    @Test
    public void test008_ObjectWithNestedEmptyCollectionSerialize() throws Exception {
        System.out.println("\ntest008_ObjectWithNestedEmptyCollectionSerialize");
        Shop shop = new Shop("автомагазин");
        shop.setCarsForSale(new ArrayList());
        Serializer serializer = new Serializer();
        String s = serializer.serialize(shop);
        System.out.println(s);
    }

    @Test
    public void test009_SimpleDeserialize() throws Exception {
        System.out.println("\ntest009_SimpleDeserialize");

        String s = "model	maxSpeed	weight	worldIndex	releaseDate	doorsCount	perfect	baseField\n"
                + "String	Int32	Int32	Decimal	TimeStamp	Int16	Boolean	String\n"
                + "0JvQsNC00LAg0LrQsNC70LjQvdCw	90	1200	2.1	20010308112335.234	3	0	YXphemF6YXphemE=";

        Serializer serializer = new Serializer();
        Car car = serializer.deserialize(s, Car.class);
        System.out.println(car);
    }

    @Test
    public void test010_NestedDeserialize() throws Exception {
        System.out.println("\ntest010_NestedDeserialize");

        String s = "ownedCar	name\n" + "DataSet	String\n"
                + "d29ybGRJbmRleAltb2RlbAltYXhTcGVlZAlyZWxlYXNlRGF0ZQlkb29yc0NvdW50CXBlcmZlY3QJd2VpZ2h0CkRlY2ltYWwJU3RyaW5nCUludDMyCVRpbWVTdGFtcAlJbnQxNglCb29sZWFuCUludDMyCjIuMQkwSnZRc05DMDBMQWcwTHJRc05DNzBMalF2ZEN3CTkwCTIwMDEwMzA4MTEyMzM1LjIzNAkzCTAJMTIwMA==	0JjQstCw0L0g0J/QtdGC0YDQvtCy";

        Serializer serializer = new Serializer();
        CarOwner carOwner = serializer.deserialize(s, CarOwner.class);
        System.out.println(carOwner);
    }

    @Test
    public void test011_PrimitiveListDeserialize() throws Exception {
        System.out.println("\ntest011_PrimitiveListDeserialize");
        String s = "arr\n" + "Int32\n" + "123\n" + "456\n" + "789";
        Serializer serializer = new Serializer();
        List<Integer> data = serializer.deserializeCollection(s, Integer.class);
        System.out.println(data);
    }

    @Test
    public void test012_ObjectListDeserialize() throws Exception {
        System.out.println("\ntest012_ObjectListDeserialize");
        String s = "doorsCount	worldIndex	weight	maxSpeed	perfect	model	releaseDate\n" + "Int16	Decimal	Int32	Int32	Boolean	String	TimeStamp\n"
                + "3	2.1	1200	90	0	0JvQsNC00LAg0LrQsNC70LjQvdCw	20010308112335.234\n"
                + "5	2.3	1150	93	1	0JvQsNC00LAg0L/RgNC40L7RgNCw	20030223000000.006";
        Serializer serializer = new Serializer();
        List<Car> cars = serializer.deserializeCollection(s, Car.class);
        System.out.println(cars);
    }

    @Test
    public void test013_ObjectWithNestedCollectionDeserialize() throws Exception {
        System.out.println("\ntest013_ObjectWithNestedCollectionDeserialize");
        String s = "carsForSale	name\n" + "DataSet	String\n"
                + "d29ybGRJbmRleAlyZWxlYXNlRGF0ZQlkb29yc0NvdW50CXBlcmZlY3QJbWF4U3BlZWQJd2VpZ2h0CW1vZGVsCkRlY2ltYWwJVGltZVN0YW1wCUludDE2CUJvb2xlYW4JSW50MzIJSW50MzIJU3RyaW5nCjIuMQkyMDAxMDMwODExMjMzNS4yMzQJMwkwCTkwCTEyMDAJMEp2UXNOQzAwTEFnMExyUXNOQzcwTGpRdmRDdwoyLjMJMjAwMzAyMjMwMDAwMDAuMDA2CTUJMQk5MwkxMTUwCTBKdlFzTkMwMExBZzBML1JnTkM0MEw3UmdOQ3c=	0LDQstGC0L7QvNCw0LPQsNC30LjQvQ==";
        Serializer serializer = new Serializer();
        Shop shop = serializer.deserialize(s, Shop.class);
        System.out.println(shop);
    }

    @Test
    public void test014_ObjectWithNestedEmptyCollectionDeserialize() throws Exception {
        System.out.println("\ntest014_ObjectWithNestedEmptyCollectionDeserialize");
        String s = "carsForSale	name\n" + "DataSet	String\n" + "	0LDQstGC0L7QvNCw0LPQsNC30LjQvQ==";
        Serializer serializer = new Serializer();
        Shop shop = serializer.deserialize(s, Shop.class);
        System.out.println(shop);
    }

}
