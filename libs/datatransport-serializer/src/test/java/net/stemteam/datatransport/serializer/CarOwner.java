package net.stemteam.datatransport.serializer;

/**
 * Класс ,содержащий объект (для тестирования)
 * @author Andrey Nikolaev
 */
public class CarOwner {

    private String name;
    private Car ownedCar;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Car getOwnedCar() {
        return ownedCar;
    }

    public void setOwnedCar(Car ownedCar) {
        this.ownedCar = ownedCar;
    }

    public CarOwner() {
    }

    public CarOwner(String name, Car ownedCar) {
        this.name = name;
        this.ownedCar = ownedCar;
    }

    @Override
    public String toString() {
        return "CarOwner{" + "name=" + name + ", ownedCar=" + ownedCar + '}';
    }

}
