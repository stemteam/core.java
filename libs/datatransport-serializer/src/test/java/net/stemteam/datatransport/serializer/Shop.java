package net.stemteam.datatransport.serializer;

import java.util.List;

/**
 * Класс, содержащий список обьектов для тестирования
 *
 * @author Andrey Nikolaev
 */
public class Shop {

    private String name;
    private List<Car> carsForSale;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Car> getCarsForSale() {
        return carsForSale;
    }

    public void setCarsForSale(List<Car> carsForSale) {
        this.carsForSale = carsForSale;
    }

    public Shop() {
    }

    public Shop(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Shop{" + "name=" + name + ", carsForSale=" + carsForSale + '}';
    }

}
