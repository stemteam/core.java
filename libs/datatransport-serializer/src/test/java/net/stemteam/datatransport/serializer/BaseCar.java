package net.stemteam.datatransport.serializer;

/**
 *
 * @author Andrey Nikolaev
 */
public class BaseCar {

    private String baseField;

    public String getBaseField() {
        return baseField;
    }

    public void setBaseField(String baseField) {
        this.baseField = baseField;
    }

}
