package net.stemteam.datatransport.serializer;

import java.util.Date;

/**
 * Класс с примитивными свойствами (для тестирования)
 *
 * @author Andrey Nikolaev
 */
public class Car extends BaseCar {

    private String model;
    private Integer maxSpeed;
    private int weight;
    private double worldIndex;
    private Date releaseDate;
    private short doorsCount;
    private Boolean perfect;

    public Car() {
    }

    public Car(String model, Integer maxSpeed, int weight, double worldIndex, Date releaseDate, short doorsCount, Boolean perfect) {
        this.model = model;
        this.maxSpeed = maxSpeed;
        this.weight = weight;
        this.worldIndex = worldIndex;
        this.releaseDate = releaseDate;
        this.doorsCount = doorsCount;
        this.perfect = perfect;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Integer maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getWorldIndex() {
        return worldIndex;
    }

    public void setWorldIndex(double worldIndex) {
        this.worldIndex = worldIndex;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public short getDoorsCount() {
        return doorsCount;
    }

    public void setDoorsCount(short doorsCount) {
        this.doorsCount = doorsCount;
    }

    public Boolean getPerfect() {
        return perfect;
    }

    public void setPerfect(Boolean perfect) {
        this.perfect = perfect;
    }

    @Override
    public String toString() {
        return "Car{" + "model=" + model + ", maxSpeed=" + maxSpeed + ", weight=" + weight + ", worldIndex=" + worldIndex + ", releaseDate="
                + releaseDate + ", doorsCount=" + doorsCount + ", perfect=" + perfect + ", baseField=" + getBaseField() + '}';
    }

}
