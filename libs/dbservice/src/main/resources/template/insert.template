package %1$s;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumn;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.datatransport.transport.FieldTranslator;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.db.crud.mapper.TypeMapper;
import net.stemteam.jaxis.db.crud.policy.MethodAccessPolicy;
import net.stemteam.jaxis.db.crud.service.ValidatedService;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.db.crud.validate.Validator;

public class %2$s extends ValidatedService {
	private DataSource dataSource;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private final String table = "%3$s";

	public %2$s(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
	}

	public %2$s(DataSource dataSource, Validator validator) {
		super();
		this.dataSource = dataSource;
		setValidator(validator);
	}
	
	private final String sql = "%4$s";

	@Override
	public String prepareResponce(HashMap params, JaxisContentType contentType)
			throws HttpUnauthorizedException, HttpForbiddenException,
			HttpInternalException, HttpNotFoundException,
			HttpBadRequestException {
		getValidator().validate(params, table, MethodAccessPolicy.UPDATE);

		logger.debug("Preparing and processing data...");
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(sql, new String[]{"%5$s"})) {

%6$s

			int rows = ps.executeUpdate();
			if (!conn.getAutoCommit()){
				conn.commit();
			}
			
			if (rows > 0) {
				logger.debug("Table [{}] successfully updated ", table);
			} else {
				logger.warn("table [{}] was not updated!", table);
			}
			DataSet result = new DataSet();
			try (ResultSet rs = ps.getGeneratedKeys()){
				result.packDataDbmsIndependently(rs,null);
			}
			result.setAsJson2Array(false);
			return getResponceAsString(result, contentType);
		} catch (SQLException | DataSetException ex) {
			logger.error("", ex);
			throw new HttpInternalException(ex);
		}
	}
}
