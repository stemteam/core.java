package net.stemteam.dbservice.helper.template;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.SQLException;
import java.util.List;

public class JaxisPublisherTemplateHelper {
    private String classTemplate;

    public JaxisPublisherTemplateHelper() throws SQLException, IOException,
            FileNotFoundException {
        File file = new File(
                "src/main/resources/template/jaxis_publish.template");
        if (!file.exists()) {
            file = new File("templates/jaxis_publish.template");
        }
        try (InputStream is = new FileInputStream(file);
                Reader streamReader = new InputStreamReader(is);
                BufferedReader reader = new BufferedReader(streamReader)) {
            StringBuilder builder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append("\n");
            }
            classTemplate = builder.toString();
        }
    }

    public String generateSource(String pkg, String path, List<String> methods) {
        StringBuilder publishCommands = new StringBuilder();
        StringBuilder removeCommands = new StringBuilder();
        for (String method : methods) {
            publishCommands.append("\t\t").append("jaxis.createContext(\"/")
                    .append(path).append(method).append("\", new ")
                    .append(method).append("(dataSource,validator));\n");
            removeCommands.append("\t\t").append("jaxis.removeContext(\"/")
                    .append(path).append(method).append("\");\n");
        }
        removeCommands.append("\t\t").append("jaxis.removeContext(\"/")
        .append(path).append("index").append("\");\n");
        publishCommands.append("\t\t").append("jaxis.createContext(\"/")
                .append(path).append("index\",new IndexService(validator));");
        return String.format(classTemplate, pkg, publishCommands.toString(),
                removeCommands.toString());
    }
}
