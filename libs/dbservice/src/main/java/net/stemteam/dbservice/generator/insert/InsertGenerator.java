package net.stemteam.dbservice.generator.insert;

import java.sql.Connection;
import java.util.List;

import net.stemteam.datatransport.transport.JaxisTypesSQLResolver;
import net.stemteam.jaxis.db.crud.policy.MethodAccessPolicy;

import net.stemteam.dbservice.cli.options.Configurator;
import net.stemteam.dbservice.generator.AbstractGenerator;
import net.stemteam.dbservice.helper.DatabaseHelper;
import net.stemteam.dbservice.helper.symbolic.PreparedStatementSymbolicFactory;
import net.stemteam.dbservice.helper.symbolic.SQLQueryFactory;
import net.stemteam.dbservice.helper.template.InsertTemplateHelper;
import net.stemteam.dbservice.metadata.MethodDescription;
import net.stemteam.dbservice.metadata.MethodParameterDescription;
import net.stemteam.dbservice.model.Column;
import net.stemteam.dbservice.model.Table;

public class InsertGenerator extends AbstractGenerator {
    private InsertTemplateHelper templateHelper;

    public InsertGenerator(Configurator congfigurator) {
        super(congfigurator);
    }

    @Override
    public void generate() throws Exception {
        if (templateHelper == null) {
            templateHelper = new InsertTemplateHelper();
        }
        try (Connection conn = establishConnection()) {
            List<Table> tables = DatabaseHelper.getDataBaseTables(conn,
                    configurator);
            for (Table table : tables) {
                String keyColumn = table.getKeyColumnName() != null ? table
                        .getKeyColumnName() : table.getColumns().get(0)
                        .getColumnName();
                String pkg = configurator.getDefaultPackage();
                String methodName = getUniqueMethodName(table.getTableName(),
                        MethodAccessPolicy.INSERT, false);
                String sql = SQLQueryFactory.createInsertQuery(table);
                String psFill = PreparedStatementSymbolicFactory
                        .generateInsertStatementFill(table);
                String source = templateHelper.generateSource(pkg, methodName,
                        table.getTableName(), sql, keyColumn, psFill);
                sources.put(methodName, source);
                MethodDescription desc = describeInsertMethod(table, methodName);
                configurator.getDescription().add(desc);
                dumpToFile();
                sources.clear();
            }
        }
        
    }

    private MethodDescription describeInsertMethod(Table table,
            String methodName) {
        MethodDescription description = new MethodDescription(
                MethodAccessPolicy.INSERT);
        description.setDescription("Вставка записи в таблицу "
                + table.getTableName());
        description.setMethodName(methodName);
        description.setTableName(table.getTableName());
        description.addTokenParameter();
        description.addApiKeyParameter();
        description.addVersionParameter();

        for (Column col : table.getColumns()) {
            MethodParameterDescription paramDesc = new MethodParameterDescription();
            paramDesc.setParameterDescription(col.getColumnName());
            paramDesc.setParameterName(col.getColumnName());
            paramDesc.setParameterType(JaxisTypesSQLResolver
                    .resolveNavstatType(col.getDataType()));
            description.addInParameter(paramDesc);
        }
        Column col = table.getKeyColumn();

        MethodParameterDescription id = new MethodParameterDescription();
        id.setParameterDescription("Сгенерированный идентификатор записей");
        id.setParameterName(col != null ? col.getColumnName() : table
                .getColumns().get(0).getColumnName());
        id.setParameterType(col != null ? col.getDataType() : table
                .getColumns().get(0).getDataType());
        description.addOutParameter(id);
        return description;
    }

}
