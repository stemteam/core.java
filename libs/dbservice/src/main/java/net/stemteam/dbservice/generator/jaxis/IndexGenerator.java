package net.stemteam.dbservice.generator.jaxis;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.stemteam.dbservice.cli.options.Configurator;
import net.stemteam.dbservice.helper.template.IndexTemplateHelper;
import net.stemteam.dbservice.metadata.MethodDescription;

import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumn;
import net.stemteam.datatransport.transport.DataRecord;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.db.crud.policy.MethodAccessPolicy;
import net.stemteam.jaxis.db.crud.service.ValidatedService;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;

public class IndexGenerator {
    private Configurator configurator;
    private IndexTemplateHelper templateHelper;

    public IndexGenerator(Configurator configurator) {
        super();
        this.configurator = configurator;
    }

    public void generate() throws Exception {
        if (templateHelper == null) {
            templateHelper = new IndexTemplateHelper();
        }
        List<String> methodNames = new ArrayList<>();
        for (MethodDescription desc : configurator.getDescription()) {
            methodNames.add(desc.getMethodName());
        }

        String res = templateHelper.generateSource(
                configurator.getDefaultPackage(),
                configurator.getServicePath(), methodNames);
        try (FileWriter fw = new FileWriter(new File(
                configurator.getTargetDir(), "IndexService.java"))) {
            fw.write(res);
        }
    }
}
