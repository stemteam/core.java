package net.stemteam.dbservice.helper.template;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.SQLException;

public class UpdateTemplateHelper {
	private String classTemplate;

	public UpdateTemplateHelper() throws SQLException, IOException,
			FileNotFoundException {
		File file = new File("src/main/resources/template/update.template");
		if (!file.exists()) {
			file = new File("templates/update.template");
		}
		try (InputStream is = new FileInputStream(file);
				Reader streamReader = new InputStreamReader(is);
				BufferedReader reader = new BufferedReader(streamReader)) {
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append("\n");
			}
			classTemplate = builder.toString();
		}
	}

	public String generateSource(String pkg, String methodName,
			String tableName, String sql, String fillStatement) {
		return String.format(classTemplate, pkg, methodName, tableName, sql,
				fillStatement);
	}
}
