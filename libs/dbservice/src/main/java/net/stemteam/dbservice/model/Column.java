package net.stemteam.dbservice.model;

public class Column {
    private String columnName;

    private int dataType;

    private boolean primaryKey;

    private boolean foreignKey;

    private String relatedTable;

    private String relatedField;

    public String getColumnName() {
        return columnName;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public boolean isForeignKey() {
        return foreignKey;
    }

    public void setForeignKey(boolean foreignKey) {
        this.foreignKey = foreignKey;
    }

    public String getRelatedTable() {
        return relatedTable;
    }

    public void setRelatedTable(String relatedTable) {
        this.relatedTable = relatedTable;
    }

    public String getRelatedField() {
        return relatedField;
    }

    public void setRelatedField(String relatedField) {
        this.relatedField = relatedField;
    }

    @Override
    public String toString() {
        return "Column [columnName=" + columnName + ", dataType=" + dataType
                + ", primaryKey=" + primaryKey + ", foreignKey=" + foreignKey
                + ", relatedTable=" + relatedTable + ", relatedField="
                + relatedField + "]";
    }

}
