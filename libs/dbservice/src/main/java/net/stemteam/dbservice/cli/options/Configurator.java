package net.stemteam.dbservice.cli.options;

import java.io.File;
import java.io.IOException;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import org.apache.commons.cli.CommandLine;

import net.stemteam.dbservice.metadata.MethodDescription;
import net.stemteam.dbservice.util.FileUtils;

public class Configurator {
	private List<MethodDescription> description = new ArrayList<>();

	private boolean generateSelect = false;
	private boolean generateUpdate = false;
	private boolean generateDelete = false;
	private boolean generateInsert = false;
	private boolean generateMetadata = false;

	private boolean generateWiki = false;

	private boolean enableViews;

	private String defaultSchema;

	private String defaultPackage;

	private String connectionString;

	private String driverClass;

	private String servicePath;

	private String wikinamespace;

	private File targetDir;

	private File wikiDir;
	
	private long maxMethodId;
	
	private long maxFieldId;
	
	private long methodGroupId;

	public Configurator(CommandLine line) throws IOException {
		connectionString = line.getOptionValue("s");
		driverClass = line.getOptionValue("j");
		if (line.hasOption("p")) {
			defaultPackage = line.getOptionValue("p");
			if (defaultPackage.endsWith(".")) {
				defaultPackage = defaultPackage.substring(0,
						defaultPackage.length() - 1);
			}
		} else {
			defaultPackage = "ru.telemediclab";
		}

		if (line.hasOption("wr")) {
			wikinamespace = line.getOptionValue("wr");
		}

		if (line.hasOption("h")) {
			defaultSchema = line.getOptionValue("h");
		}

		if (line.hasOption("sp")) {
			servicePath = line.getOptionValue("sp");
			if (!servicePath.endsWith("/")) {

			}
		} else {
			servicePath = "auto/";
		}

		String[] fullPath = defaultPackage.split("\\.");
		if (fullPath.length >= 1) {
			FileUtils.deleteDirectory(new File(fullPath[0]));
		}

		if (fullPath.length == 1) {
			targetDir = new File(fullPath[0]);
			if (!targetDir.mkdir()) {
				throw new RuntimeException("Can ot create folder "
						+ fullPath[0]);
			}
		} else {
			File child = null;
			File parent = new File(fullPath[0]);
			if (!parent.mkdir()) {
				throw new RuntimeException("Can ot create folder "
						+ fullPath[0]);
			}

			for (int i = 1; i < fullPath.length; i++) {
				child = new File(parent, fullPath[i]);
				if (!child.mkdir()) {
					throw new RuntimeException("Can ot create folder "
							+ fullPath[i]);
				}
				parent = child;
			}
			targetDir = parent;
		}

		generateWiki = line.hasOption("w");
		if (generateWiki) {
			wikiDir = new File(targetDir, "wiki");
			if (!wikiDir.exists()) {
				wikiDir.mkdir();
			}
		}

		generateMetadata = line.hasOption("m");
		generateInsert = line.hasOption("c");
		generateSelect = line.hasOption("r");
		generateUpdate = line.hasOption("u");
		generateDelete = line.hasOption("d");
		enableViews = line.hasOption("v");
		
		if (line.hasOption("mfi")){
			maxFieldId = Long.parseLong(line.getOptionValue("mfi"));
		}
		if (line.hasOption("mmi")){
			maxMethodId = Long.parseLong(line.getOptionValue("mmi"));
		}
		if (line.hasOption("mgi")){
			methodGroupId = Long.parseLong(line.getOptionValue("mgi"));
		}
		
		
	}

	public void registerAndTestDB() throws Exception {
		System.out.println(driverClass);
		Object driver = Class.forName(driverClass).newInstance();
		DriverManager.registerDriver((Driver)driver);
		DriverManager.getConnection(connectionString).close();
	}

	public boolean isGenerateSelect() {
		return generateSelect;
	}

	public boolean isGenerateUpdate() {
		return generateUpdate;
	}

	public boolean isGenerateDelete() {
		return generateDelete;
	}

	public boolean isGenerateInsert() {
		return generateInsert;
	}

	public boolean isGenerateWiki() {
		return generateWiki;
	}

	public String getDefaultPackage() {
		return defaultPackage;
	}

	public String getConnectionString() {
		return connectionString;
	}

	public String getDriverClass() {
		return driverClass;
	}

	public boolean isGenerateMetadata() {
		return generateMetadata;
	}

	public File getTargetDir() {
		return targetDir;
	}

	public String getDefaultSchema() {
		return defaultSchema;
	}

	public boolean isEnableViews() {
		return enableViews;
	}

	public String getServicePath() {
		return servicePath;
	}

	public List<MethodDescription> getDescription() {
		return description;
	}

	public File getWikiDir() {
		return wikiDir;
	}

	public String getWikinamespace() {
		return wikinamespace;
	}

	public void setWikinamespace(String wikinamespace) {
		this.wikinamespace = wikinamespace;
	}

	public long getMaxMethodId() {
		return maxMethodId;
	}

	public long getMaxFieldId() {
		return maxFieldId;
	}

	public long getMethodGroupId() {
		return methodGroupId;
	}

}
