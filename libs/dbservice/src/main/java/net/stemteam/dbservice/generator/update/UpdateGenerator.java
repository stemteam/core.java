package net.stemteam.dbservice.generator.update;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

import net.stemteam.datatransport.transport.JaxisTypesSQLResolver;
import net.stemteam.jaxis.db.crud.policy.MethodAccessPolicy;

import net.stemteam.dbservice.cli.options.Configurator;
import net.stemteam.dbservice.generator.AbstractGenerator;
import net.stemteam.dbservice.helper.DatabaseHelper;
import net.stemteam.dbservice.helper.symbolic.PreparedStatementSymbolicFactory;
import net.stemteam.dbservice.helper.symbolic.SQLQueryFactory;
import net.stemteam.dbservice.helper.template.UpdateTemplateHelper;
import net.stemteam.dbservice.metadata.MethodDescription;
import net.stemteam.dbservice.metadata.MethodParameterDescription;
import net.stemteam.dbservice.model.Column;
import net.stemteam.dbservice.model.Table;

public class UpdateGenerator extends AbstractGenerator {
    private UpdateTemplateHelper helper;

    public UpdateGenerator(Configurator congfigurator) {
        super(congfigurator);
    }

    @Override
    public void generate() throws Exception {
        if (helper == null) {
            helper = new UpdateTemplateHelper();
        }
        try (Connection conn = establishConnection()) {
            List<Table> tables = DatabaseHelper.getDataBaseTables(conn,
                    configurator);
            for (Table table : tables) {
                String pkg = configurator.getDefaultPackage();
                String methodName = getUniqueMethodName(table.getTableName(),
                        MethodAccessPolicy.UPDATE, false);
                String sql = SQLQueryFactory.createUpdateQuery(table);
                if (sql != null && !sql.isEmpty()) {
                    String psFill = PreparedStatementSymbolicFactory
                            .generateUpdateStatementFill(table);
                    String source = helper.generateSource(pkg, methodName,
                            table.getTableName(), sql, psFill);
                    sources.put(methodName, source);
                    MethodDescription desc = describeUpdateMethod(table,
                            methodName);
                    configurator.getDescription().add(desc);
                    dumpToFile();
                    sources.clear();
                }
            }
        }
        
    }

    private MethodDescription describeUpdateMethod(Table table,
            String methodName) {
        MethodDescription description = new MethodDescription(
                MethodAccessPolicy.UPDATE);
        description.setDescription("Обновление таблицы " + table.getTableName()
                + " по ключу");
        description.setMethodName(methodName);
        description.setTableName(table.getTableName());
        description.addTokenParameter();
        description.addApiKeyParameter();
        description.addVersionParameter();

        for (Column col : table.getColumns()) {
            MethodParameterDescription paramDesc = new MethodParameterDescription();
            if (col.isPrimaryKey()) {
                paramDesc
                        .setParameterDescription("Ключ, по которому происходит обновление");
            } else {
                paramDesc.setParameterDescription(col.getColumnName());
            }

            paramDesc.setParameterName(col.getColumnName());
            paramDesc.setParameterType(JaxisTypesSQLResolver
                    .resolveNavstatType(col.getDataType()));
            description.addInParameter(paramDesc);
        }

        return description;
    }

}
