package net.stemteam.dbservice.generator.wiki;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.stemteam.dbservice.cli.options.Configurator;
import net.stemteam.dbservice.metadata.MethodDescription;
import net.stemteam.dbservice.metadata.MethodParameterDescription;

public class WikiGenerator {
    private Configurator configurator;

    private Map<String, String> methods = new HashMap<>();

    private String deleteIndex;
    private String insertIndex;
    private String updateIndex;
    private String selectIndex;

    public WikiGenerator(Configurator configurator) {
        super();
        this.configurator = configurator;
    }

    public void generate() throws IOException {
        generateWiki();
        if (selectIndex != null && !selectIndex.isEmpty()) {
            dumpToFile(configurator.getWikinamespace() + "_select.txt",
                    selectIndex);
        }
        if (deleteIndex != null && !deleteIndex.isEmpty()) {
            dumpToFile(configurator.getWikinamespace() + "_delete.txt",
                    deleteIndex);
        }
        if (updateIndex != null && !updateIndex.isEmpty()) {
            dumpToFile(configurator.getWikinamespace() + "_update.txt",
                    updateIndex);
        }
        if (insertIndex != null && !insertIndex.isEmpty()) {
            dumpToFile(configurator.getWikinamespace() + "_insert.txt",
                    insertIndex);
        }
    }

    private void dumpToFile(File file, String content) throws IOException {
        try (FileWriter fw = new FileWriter(file)) {
            fw.write(content);
            fw.flush();
        }
    }

    private void dumpToFile(String filename, String content) throws IOException {
        File wikiFile = new File(configurator.getWikiDir(), filename);
        try (FileWriter fw = new FileWriter(wikiFile)) {
            fw.write(content);
            fw.flush();
        }
    }

    private void generateWiki() throws IOException {
        StringBuilder builder = new StringBuilder();
        StringBuilder selectIndexBuilder = new StringBuilder();
        StringBuilder insertIndexBuilder = new StringBuilder();
        StringBuilder updateIndexBuilder = new StringBuilder();
        StringBuilder deleteIndexBuilder = new StringBuilder();

        for (MethodDescription desc : configurator.getDescription()) {
            StringBuilder indexBuilder = null;
            switch (desc.getPolicy()) {
            case DELETE:
                indexBuilder = deleteIndexBuilder;
                break;
            case INSERT:
                indexBuilder = insertIndexBuilder;
                break;
            case SELECT:
                indexBuilder = selectIndexBuilder;
                break;
            case UPDATE:
                indexBuilder = updateIndexBuilder;
            }
            indexBuilder.append("[[").append(configurator.getWikinamespace())
                    .append("_").append(desc.getPolicy()).append(":")
                    .append(desc.getMethodName()).append("|")
                    .append(desc.getMethodName()).append("]] - ")
                    .append(desc.getDescription()).append("\n").append("\n");

            builder.append("==== ").append(desc.getMethodName())
                    .append(" ====\n\n").append(desc.getDescription())
                    .append("\n\n");
            if (desc.getInParameters() != null
                    && desc.getInParameters().size() > 0) {
                builder.append("Входные параметры:\n");
                builder.append("^Параметр^Тип^Комментарий^\n");
                for (MethodParameterDescription inParam : desc
                        .getInParameters()) {
                    builder.append("| ").append(inParam.getParameterName())
                            .append(" | ").append(inParam.getParameterType())
                            .append(" | ")
                            .append(inParam.getParameterDescription())
                            .append(" |\n");
                }
            }
            builder.append("\n");
            if (desc.getOutParameters() != null
                    && desc.getOutParameters().size() > 0) {
                builder.append("Возвращаемые параметры:\n");
                builder.append("^Параметр^Тип^Комментарий^\n");
                for (MethodParameterDescription outParam : desc
                        .getOutParameters()) {
                    builder.append("| ").append(outParam.getParameterName())
                            .append(" | ").append(outParam.getParameterType())
                            .append(" | ")
                            .append(outParam.getParameterDescription())
                            .append(" |\n");
                }
            }
            builder.append("\n");
            File targetFolderFile = new File(configurator.getWikiDir(),
                    configurator.getWikinamespace() + "_" + desc.getPolicy());
            if (!targetFolderFile.exists()) {
                targetFolderFile.mkdir();
            }
            dumpToFile(new File(targetFolderFile, desc.getMethodName()
                    .toLowerCase() + ".txt"), builder.toString());

            builder = new StringBuilder();
        }
        selectIndex = selectIndexBuilder.toString();
        deleteIndex = deleteIndexBuilder.toString();
        insertIndex = insertIndexBuilder.toString();
        updateIndex = updateIndexBuilder.toString();
    }
}
