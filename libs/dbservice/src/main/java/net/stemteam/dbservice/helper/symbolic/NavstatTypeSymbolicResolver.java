package net.stemteam.dbservice.helper.symbolic;

import net.stemteam.datatransport.transport.DataColumn;
import net.stemteam.datatransport.transport.JaxisTypesSQLResolver;

public class NavstatTypeSymbolicResolver {

    public static String resolveSqlType(int sqlType) {
        String type = JaxisTypesSQLResolver.resolveNavstatType(sqlType);
        switch (type) {
        case DataColumn.ARRAY_SUFFIX:
            return "DataColumn.ARRAY_SUFFIX";
        case DataColumn.NAVSTAT_BOOLEAN:
            return "DataColumn.NAVSTAT_BOOLEAN";
        case DataColumn.NAVSTAT_DATASET:
            return "DataColumn.NAVSTAT_DATASET";
        case DataColumn.NAVSTAT_DATE:
            return "DataColumn.NAVSTAT_DATE";
        case DataColumn.NAVSTAT_DATETIME:
            return "DataColumn.NAVSTAT_DATETIME";
        case DataColumn.NAVSTAT_FLOAT:
            return "DataColumn.NAVSTAT_FLOAT";
        case DataColumn.NAVSTAT_GEOMETRY:
            return "DataColumn.NAVSTAT_GEOMETRY";
        case DataColumn.NAVSTAT_INT16:
            return "DataColumn.NAVSTAT_INT16";
        case DataColumn.NAVSTAT_INT32:
            return "DataColumn.NAVSTAT_INT32";
        case DataColumn.NAVSTAT_INT64:
            return "DataColumn.NAVSTAT_INT64";
        case DataColumn.NAVSTAT_STRING:
            return "DataColumn.NAVSTAT_STRING";
        case DataColumn.NAVSTAT_TIMESTAMP:
            return "DataColumn.NAVSTAT_TIMESTAMP";
        default:
            return "";
        }
    }
}
