package net.stemteam.dbservice.generator.jaxis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.stemteam.dbservice.cli.options.Configurator;
import net.stemteam.dbservice.helper.template.JaxisPublisherTemplateHelper;
import net.stemteam.dbservice.metadata.MethodDescription;

public class JaxisGenerator {
    private Configurator configurator;

    private JaxisPublisherTemplateHelper templateHelper = null;

    public JaxisGenerator(Configurator configurator) {
        this.configurator = configurator;
    }

    public void generate() throws FileNotFoundException, SQLException,
            IOException {
        if (templateHelper == null) {
            templateHelper = new JaxisPublisherTemplateHelper();
        }
        List<String> methodNames = new ArrayList<>();
        for (MethodDescription desc : configurator.getDescription()) {
            methodNames.add(desc.getMethodName());
        }
        String res = templateHelper.generateSource(
                configurator.getDefaultPackage(),
                configurator.getServicePath(), methodNames);
        try (FileWriter fw = new FileWriter(new File(
                configurator.getTargetDir(), "JaxisPublisher.java"))) {
            fw.write(res);
        }
    }
}
