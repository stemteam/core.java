package net.stemteam.dbservice.model;

import java.util.ArrayList;
import java.util.List;

public class Table {
    private String tableName;

    private List<Column> columns = new ArrayList<>(0);

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String table) {
        this.tableName = table;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public Column getColumnByName(String name) {
        for (Column column : columns) {
            if (column.getColumnName().equalsIgnoreCase(name)) {
                return column;
            }
        }
        return null;
    }

    public Column getKeyColumn() {
        for (Column column : columns) {
            if (column.isPrimaryKey()) {
                return column;
            }
        }
        return null;
    }

    public String getKeyColumnName() {
        Column keyColumn = getKeyColumn();
        return keyColumn == null ? null : keyColumn.getColumnName();
    }

    @Override
    public String toString() {
        return "Table [tableName=" + tableName + ", columns=" + columns + "]";
    }

}
