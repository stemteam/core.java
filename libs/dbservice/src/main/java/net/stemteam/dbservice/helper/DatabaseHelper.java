package net.stemteam.dbservice.helper;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.stemteam.dbservice.cli.options.Configurator;
import net.stemteam.dbservice.model.Column;
import net.stemteam.dbservice.model.Table;

public class DatabaseHelper {
    private static List<Table> tables;

    public static synchronized List<Table> getDataBaseTables(Connection conn,
            Configurator configurator) throws SQLException {
        if (tables == null) {
            tables = getTables(conn, configurator);
        }
        return tables;
    }

    private static List<Table> getTables(Connection conn,
            Configurator configurator) throws SQLException {
        DatabaseMetaData metadata = conn.getMetaData();
        String[] tableTypes;
        if (configurator.isEnableViews()) {
            tableTypes = new String[] { "TABLE", "VIEW" };
        } else {
            tableTypes = new String[] { "TABLE" };
        }
        try (ResultSet rs = metadata.getTables(null,
                configurator.getDefaultSchema(), null, tableTypes)) {
            List<Table> tables = new ArrayList<>();
            while (rs.next()) {
                String tableName = rs.getString("TABLE_NAME").toLowerCase();
                if (tableName.contains("$")) {
                    continue;
                }
                Table table = new Table();
                table.setTableName(tableName);
                System.out.println("exploring table " + tableName + "...");
                List<Column> tabColumns = getColumnsForTable(conn,
                        configurator.getDefaultSchema(),
                        configurator.getDriverClass(), tableName);
                table.setColumns(tabColumns);
                tables.add(table);
            }
            return tables;
        }
    }

    public static List<Column> getColumnsForTable(Connection conn,
            String schema, String driverClass, String table)
            throws SQLException {
        DatabaseMetaData metadata = conn.getMetaData();
        try (ResultSet rs = metadata.getColumns(null, null, table, null)) {
            List<Column> columns = new ArrayList<>();
            while (rs.next()) {
                Column column = new Column();
                if (rs.getString("COLUMN_NAME").contains("$")) {
                    continue;
                }
                column.setColumnName(rs.getString("COLUMN_NAME").toLowerCase());
                column.setPrimaryKey(primaryKeyColumn(conn, schema, table,
                        column.getColumnName()));
                column.setForeignKey(isColumnAForeignKey(table,
                        column.getColumnName(), schema, metadata));
                if (column.isForeignKey()) {
                    column.setRelatedField(getRelatedField(table,
                            column.getColumnName(), schema, metadata));
                    column.setRelatedTable(getRelatedTable(table,
                            column.getColumnName(), schema, metadata));
                }
                column.setDataType(rs.getInt("DATA_TYPE"));
                columns.add(column);
            }
            return columns;
        }
    }

    private static boolean primaryKeyColumn(Connection conn, String schema,
            Table table, Column column) throws SQLException {
        return primaryKeyColumn(conn, schema, table.getTableName(),
                column.getColumnName());
    }

    private static boolean primaryKeyColumn(Connection conn, String schema,
            String table, String column) throws SQLException {
        DatabaseMetaData metadata = conn.getMetaData();
        try (ResultSet rs = metadata.getPrimaryKeys(null, schema, table)) {
            while (rs.next()) {
                String columnName = rs.getString("COLUMN_NAME");
                if (columnName.equalsIgnoreCase(column)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static String getKeyColumnName(Table table) {
        for (Column column : table.getColumns()) {
            if (column.isPrimaryKey()) {
                return column.getColumnName();
            }
        }
        return null;
    }

    private static boolean isColumnAForeignKey(String tableName, String column,
            String schema, DatabaseMetaData metadata) throws SQLException {
        ResultSet rs = metadata.getImportedKeys(null, schema, tableName);
        boolean result = false;
        while (rs.next()) {
            if (rs.getString("fkcolumn_name").equalsIgnoreCase(column)) {
                result = true;
            }
        }
        rs.close();
        return result;
    }

    private static String getRelatedTable(String tableName, String column,
            String schema, DatabaseMetaData metadata) throws SQLException {
        try (ResultSet rs = metadata.getImportedKeys(null, schema, tableName)) {
            String result = "";
            while (rs.next()) {
                if (rs.getString("fkcolumn_name").equalsIgnoreCase(column)) {
                    result = rs.getString("PKTABLE_NAME");
                    return result;
                }
            }
            return null;
        }
    }

    private static String getRelatedField(String tableName, String column,
            String schema, DatabaseMetaData metadata) throws SQLException {
        try (ResultSet rs = metadata.getImportedKeys(null, schema, tableName)) {
            String result = "";
            while (rs.next()) {
                if (rs.getString("fkcolumn_name").equalsIgnoreCase(column)) {
                    result = rs.getString("PKCOLUMN_NAME");
                    return result;
                }
            }
            return null;
        }
    }
}
