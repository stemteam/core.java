package net.stemteam.dbservice.generator;

import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import net.stemteam.dbservice.cli.options.Configurator;

import net.stemteam.jaxis.db.crud.mapper.MethodNameMapper;
import net.stemteam.jaxis.db.crud.mapper.TelemedicMethodsNameMapper;
import net.stemteam.jaxis.db.crud.policy.MethodAccessPolicy;

public abstract class AbstractGenerator {
    protected MethodNameMapper mapper = new TelemedicMethodsNameMapper();
    protected Map<String, String> sources = new HashMap<String, String>();
    protected Configurator configurator;
    private File target = null;

    public AbstractGenerator(Configurator configurator) {
        this.configurator = configurator;
        target = configurator.getTargetDir();
    }

    protected Connection establishConnection() throws SQLException {
        Connection conn = DriverManager.getConnection(configurator
                .getConnectionString());
        return conn;
    }

    public abstract void generate() throws Exception;

    protected String getUniqueMethodName(String table,
            MethodAccessPolicy policy, boolean array) {
        String originalMethodName = mapper.getMethodName(table, policy, array);
        if (!containsIgnoreCase(originalMethodName, sources)) {
            return originalMethodName;
        } else {
            int i = 2;
            while (containsIgnoreCase(
                    mapper.getMethodName(table + "" + i, policy, array),
                    sources)) {
                i++;
            }
            return mapper.getMethodName(table + "" + i, policy, array);
        }
    }

    protected boolean containsIgnoreCase(String string, Map<String, String> map) {
        for (String s : map.keySet()) {
            if (string.equalsIgnoreCase(s)) {
                return true;
            }
        }
        return false;
    }

    protected void dumpToFile() throws Exception {
        System.out.println(sources.keySet().size()
                + " files would be generated for CRUD methods");
        for (String key : sources.keySet()) {
            String source = sources.get(key);
            File sourceFile = new File(target, key + ".java");
            try (FileWriter writer = new FileWriter(sourceFile)) {
                System.out.println("Writing class "+key+" with "+source.replaceAll("\n", "").substring(source.indexOf("class"),source.indexOf("class")+50)+"...");
                writer.write(source);
                writer.flush();
            }
        }
        System.out.println(sources.keySet().size()
                + " files generated for CRUD methods to "
                + configurator.getTargetDir().getAbsoluteFile()
                        .getAbsolutePath());
    }
}
