package net.stemteam.dbservice.helper.template;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.SQLException;

public class SQLSelectByIdTemplateHelper {
	private String classTemplate;

	public SQLSelectByIdTemplateHelper() throws SQLException, IOException,
			FileNotFoundException {
		File file = new File("src/main/resources/template/key_select.template");
		if (!file.exists()){
			file = new File("templates/key_select.template");
		}
		try (InputStream is = new FileInputStream(file);
				Reader streamReader = new InputStreamReader(is);
				BufferedReader reader = new BufferedReader(streamReader)) {
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append("\n");
			}
			classTemplate = builder.toString();
		}
	}

	public String generateSource(String pkg, String className,
			String tableName, String fieldTranslatorSource, String sqlQuery,
			String primaryKeyColumn) {
		return String.format(classTemplate, pkg, className, tableName,
				fieldTranslatorSource, sqlQuery, primaryKeyColumn);
	}

}
