package net.stemteam.dbservice.generator.select;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.stemteam.jaxis.db.crud.mapper.TelemedicMethodsNameMapper;
import net.stemteam.jaxis.db.crud.policy.MethodAccessPolicy;

import net.stemteam.dbservice.cli.options.Configurator;
import net.stemteam.dbservice.generator.AbstractGenerator;
import net.stemteam.dbservice.helper.DatabaseHelper;
import net.stemteam.dbservice.helper.symbolic.FieldTranslatorFactorySource;
import net.stemteam.dbservice.helper.symbolic.SQLQueryFactory;
import net.stemteam.dbservice.helper.template.SQLSelectByIdTemplateHelper;
import net.stemteam.dbservice.helper.template.SQLSelectListTemplateHelper;
import net.stemteam.dbservice.metadata.MethodDescription;
import net.stemteam.dbservice.metadata.MethodParameterDescription;
import net.stemteam.dbservice.model.Column;
import net.stemteam.dbservice.model.Table;

public class SelectGenerator extends AbstractGenerator {
    private SQLSelectByIdTemplateHelper templateIdHelper = null;
    private SQLSelectListTemplateHelper templateListHelper = null;

    public SelectGenerator(Configurator congfigurator) {
        super(congfigurator);
    }

    public void generate() throws Exception {
        System.out.println("start generating select classes");
        if (templateIdHelper == null) {
            templateIdHelper = new SQLSelectByIdTemplateHelper();
        }
        if (templateListHelper == null) {
            templateListHelper = new SQLSelectListTemplateHelper();
        }

        try (Connection conn = establishConnection()) {
            List<Table> tables = DatabaseHelper.getDataBaseTables(conn,
                    configurator);
            for (Table table : tables) {
                // создаем методы на основе таблиц
                String keyColumn = DatabaseHelper.getKeyColumnName(table);
                String pkg = configurator.getDefaultPackage();

                if (keyColumn != null) {
                    // есть первичный ключ. делаем Get method
                    String name1 = getUniqueMethodName(table.getTableName(),
                            MethodAccessPolicy.SELECT, false);
                    String fieldTranslatorSource = FieldTranslatorFactorySource
                            .getFieldTranslatorSource(table);
                    String sqlQuery = SQLQueryFactory
                            .createSelectByIdQuery(table);

                    String source = templateIdHelper.generateSource(pkg, name1,
                            table.getTableName(), fieldTranslatorSource,
                            sqlQuery, keyColumn);
                    sources.put(name1, source);
                    describeGetMethod(name1, keyColumn, table);
                    
                }
                String name2 = getUniqueMethodName(table.getTableName(),
                        MethodAccessPolicy.SELECT, true);
                String listFieldTransaltor = FieldTranslatorFactorySource
                        .getFieldTranslatorSourceWithCountColumn(table);
                String sqlLimitOffset = SQLQueryFactory
                        .createSelectListLimitOffsetQuery(table, configurator);

                String sqlConditionalLimitOffset = SQLQueryFactory
                        .createConditionalSelectListLimitOffsetQuery(table,
                                configurator);

                String source2 = templateListHelper.generateSource(pkg, name2,
                        table.getTableName(), sqlLimitOffset,
                        listFieldTransaltor,
                        SQLQueryFactory.selectableColumns(table, null),
                        sqlConditionalLimitOffset);
                sources.put(name2, source2);
                describeListGetMethod(name2, table);
                dumpToFile();
                sources.clear();
            }
        }
        
    }

    private void describeGetMethod(String methodName, String keyColumn,
            Table table) {
        MethodDescription desc = new MethodDescription(
                MethodAccessPolicy.SELECT);
        desc.addTokenParameter();
        desc.addApiKeyParameter();
        desc.addVersionParameter();

        desc.setMethodName(methodName);
        desc.setTableName(table.getTableName());
        desc.setDescription("Получение записей из таблицы "
                + table.getTableName() + " по ключу");
        desc.addOutParameters(table.getColumns());

        Column idColumn = table.getColumnByName(keyColumn);

        MethodParameterDescription id = new MethodParameterDescription();
        id.setParameterDescription("Первичный ключ для доступа к записи в таблице");
        id.setParameterName(keyColumn);
        id.setParameterType(idColumn.getDataType());

        desc.addInParameter(id);
        configurator.getDescription().add(desc);
    }

    private void describeListGetMethod(String methodName, Table table) {
        // добавим описание метода
        MethodDescription desc = new MethodDescription(
                MethodAccessPolicy.SELECT);
        desc.addTokenParameter();
        desc.addApiKeyParameter();
        desc.addVersionParameter();

        desc.setMethodName(methodName);
        desc.setTableName(table.getTableName());
        desc.setDescription("Получение списка записей из таблицы "
                + table.getTableName());

        desc.addOutParameters(table.getColumns());

        MethodParameterDescription count = new MethodParameterDescription();
        count.setParameterDescription("Общее количество записей в таблице");
        count.setParameterName("count");
        count.setParameterType("Int64");
        desc.addOutParameter(count);

        // Limit + Offset
        MethodParameterDescription limit = new MethodParameterDescription();
        limit.setParameterDescription("Ограничение на количество возвращаемых записей");
        limit.setParameterName("Limit");
        limit.setParameterType("Int32");
        desc.addInParameter(limit);

        MethodParameterDescription offset = new MethodParameterDescription();
        offset.setParameterDescription("Сдвиг выборки из таблице в аналитическом окне");
        offset.setParameterName("Offset");
        offset.setParameterType("Int32");
        desc.addInParameter(offset);

        configurator.getDescription().add(desc);
    }

}
