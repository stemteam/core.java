package net.stemteam.dbservice.helper.symbolic;

import net.stemteam.datatransport.transport.FieldTranslator;

import net.stemteam.dbservice.model.Column;
import net.stemteam.dbservice.model.Table;

public class FieldTranslatorFactorySource {
    public static String getFieldTranslatorSource(Table table) {
        StringBuilder builder = new StringBuilder();
        builder.append("FieldTranslator fieldTranslator = new FieldTranslator();\n");
        for (Column column : table.getColumns()) {
            builder.append("		fieldTranslator.AddItem(\"")
                    .append(column.getColumnName())
                    .append("\",\"")
                    .append(column.getColumnName())
                    .append("\",")
                    .append(NavstatTypeSymbolicResolver.resolveSqlType(column
                            .getDataType())).append(");\n");
        }
        return builder.toString();
    }

    public static String getFieldTranslatorSourceWithCountColumn(Table table) {
        StringBuilder builder = new StringBuilder();
        builder.append("FieldTranslator fieldTranslator = new FieldTranslator();\n");
        for (Column column : table.getColumns()) {
            builder.append("		fieldTranslator.AddItem(\"")
                    .append(column.getColumnName())
                    .append("\",\"")
                    .append(column.getColumnName())
                    .append("\",")
                    .append(NavstatTypeSymbolicResolver.resolveSqlType(column
                            .getDataType())).append(");\n");
        }
        builder.append("		fieldTranslator.AddItem(\"count\",\"count\",DataColumn.NAVSTAT_INT64);");
        return builder.toString();
    }
}
