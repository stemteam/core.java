package net.stemteam.dbservice.helper.symbolic;

import net.stemteam.jaxis.db.crud.mapper.TypeMapper;

import net.stemteam.dbservice.model.Column;
import net.stemteam.dbservice.model.Table;

public class PreparedStatementSymbolicFactory {
    public static String generateUpdateStatementFill(Table table) {
        StringBuilder psBuilder = new StringBuilder();
        String keyColumnName = null;
        int paramIndex = 0;
        for (Column column : table.getColumns()) {
            if (!column.isPrimaryKey()) {
                paramIndex++;
                psBuilder
                        .append("\t\t\tps.setObject(")
                        .append(paramIndex)
                        .append(",TypeMapper.createProperObject(ps.getParameterMetaData().getParameterType(")
                        .append(paramIndex).append("),(String) params.get(\"")
                        .append(column.getColumnName())
                        .append("\"),contentType));\n");
            } else {
                keyColumnName = column.getColumnName();
            }
        }
        paramIndex++;
        psBuilder
                .append("\t\t\tps.setObject(")
                .append(paramIndex)
                .append(",TypeMapper.createProperObject(ps.getParameterMetaData().getParameterType(")
                .append(paramIndex).append("),(String) params.get(\"")
                .append(keyColumnName).append("\"),contentType));\n");
        return psBuilder.toString();
    }

    public static String generateInsertStatementFill(Table table) {
        StringBuilder psBuilder = new StringBuilder();
        for (int i = 0; i < table.getColumns().size(); i++) {
            psBuilder
                    .append("\t\t\tps.setObject(")
                    .append(i + 1)
                    .append(",TypeMapper.createProperObject(ps.getParameterMetaData().getParameterType(")
                    .append(i + 1).append("),(String) params.get(\"")
                    .append(table.getColumns().get(i).getColumnName())
                    .append("\"),contentType));\n");
        }
        return psBuilder.toString();
    }
}
