package net.stemteam.dbservice.helper.template;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.SQLException;

public class SQLSelectListTemplateHelper {
	private String classTemplate;

	public SQLSelectListTemplateHelper() throws SQLException, IOException,
			FileNotFoundException {
		File file = new File("src/main/resources/template/list_select.template");
		if (!file.exists()){
			file = new File("templates/list_select.template");
		}
		try (InputStream is = new FileInputStream(file);
				Reader streamReader = new InputStreamReader(is);
				BufferedReader reader = new BufferedReader(streamReader)) {
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append("\n");
			}
			classTemplate = builder.toString();
		}
	}
	
	public String generateSource(String pkg, String className,
			String tableName,String sqlQuery, String fieldTranslatorSource,String selectableFields,String sqlConditionalQuery) {
		return String.format(classTemplate, pkg, className, tableName,
				sqlQuery, fieldTranslatorSource,selectableFields,sqlConditionalQuery);
	}
}
