package net.stemteam.dbservice.helper.symbolic;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import net.stemteam.dbservice.cli.options.Configurator;
import net.stemteam.dbservice.helper.DatabaseHelper;
import net.stemteam.dbservice.model.Column;
import net.stemteam.dbservice.model.Table;

public class SQLQueryFactory {
    public static String createDeleteQuery(Table table) {
        List<Column> columns = table.getColumns();
        StringBuilder builder = new StringBuilder();
        builder.append("delete from ").append(table.getTableName())
                .append(" where ");
        for (int i = 0; i < columns.size(); i++) {
            Column column = columns.get(i);
            if (column.isPrimaryKey()) {
                builder.append(column.getColumnName()).append(" = ?");
                break;
            }
        }
        return builder.toString();
    }

    public static String createUpdateQuery(Table table) {
        StringBuilder sqlUpdate = new StringBuilder();
        sqlUpdate.append("update ").append(table.getTableName())
                .append(" set ");
        String keyColumn = "";
        for (Column column : table.getColumns()) {
            if (!column.isPrimaryKey()) {
                sqlUpdate.append(column.getColumnName()).append(" = ?, ");
            } else {
                keyColumn = column.getColumnName();
            }
        }
        if (keyColumn == null || keyColumn.isEmpty()) {
            return null;
        }
        // уберем лишнюю запятую
        if (!table.getColumns().isEmpty()) {
            sqlUpdate.deleteCharAt(sqlUpdate.length() - 2);
        }

        sqlUpdate.append("   where ").append(keyColumn).append(" = ?");
        return sqlUpdate.toString();
    }

    public static String createInsertQuery(Table table) {
        StringBuilder builder = new StringBuilder();
        builder.append("insert into ").append(table.getTableName())
                .append(" ( ");

        for (int i = 0; i < table.getColumns().size(); i++) {
            Column col = table.getColumns().get(i);
            if (i != table.getColumns().size() - 1) {
                builder.append(col.getColumnName()).append(", ");
            } else {
                builder.append(col.getColumnName()).append(") ");
            }
        }
        builder.append(" values (");
        for (int i = 0; i < table.getColumns().size(); i++) {
            if (i != table.getColumns().size() - 1) {
                builder.append("?, ");
            } else {
                builder.append("?) ");
            }
        }
        return builder.toString();
    }

    public static String createSelectByIdQuery(Table table) {
        List<Column> columns = table.getColumns();
        StringBuilder builder = new StringBuilder();
        builder.append("select ");
        for (int i = 0; i < columns.size(); i++) {
            Column column = columns.get(i);
            if (i == columns.size() - 1) {
                builder.append(column.getColumnName());
            } else {
                builder.append(column.getColumnName()).append(",");
            }
        }
        builder.append(" from ").append(table.getTableName());
        for (int i = 0; i < columns.size(); i++) {
            Column column = columns.get(i);
            if (column.isPrimaryKey()) {
                builder.append(" where ").append(column.getColumnName())
                        .append(" = ?");
                break;
            }
        }
        return builder.toString();
    }

    // 1й параметр - offset
    // 2й - limit
    public static String createSelectListLimitOffsetQuery(Table table,
            Configurator configurator) throws SQLException {
        String driverClass = configurator.getDriverClass();
        switch (driverClass) {
        case "com.microsoft.sqlserver.jdbc.SQLServerDriver":
            return createSelectLimitOffsetMSSQL(table, configurator);
        case "org.postgresql.Driver":
            return createSelectLimitOffsetPostgres(table);
        case "org.firebirdsql.jdbc.FBDriver":
            return createSelectLimitOffsetFB(table);
        default:
            throw new RuntimeException("Not implemented for driver class "
                    + driverClass);
        }
    }

    public static String createConditionalSelectListLimitOffsetQuery(Table table,
            Configurator configurator) throws SQLException {
        String driverClass = configurator.getDriverClass();
        switch (driverClass) {
        case "com.microsoft.sqlserver.jdbc.SQLServerDriver":
            return createConditionalSelectLimitOffsetMSSQL(table, configurator);
        case "org.postgresql.Driver":
            return createConditionalSelectLimitOffsetPostgres(table);
        case "org.firebirdsql.jdbc.FBDriver":
            return createConditionalSelectLimitOffsetFB(table);
        default:
            throw new RuntimeException("Not implemented for driver class "
                    + driverClass);
        }
    }

    private static String createSelectLimitOffsetMSSQL(Table table,
            Configurator configurator) throws SQLException {
        try (Connection conn = DriverManager.getConnection(configurator
                .getConnectionString())) {
            String comparableColumnName = "";
            DatabaseMetaData metadata = conn.getMetaData();
            ResultSet rs = metadata.getColumns(null, null,
                    table.getTableName(), null);
            while (rs.next()) {
                String columnName = rs.getString("COLUMN_NAME");
                int sqlType = rs.getInt("DATA_TYPE");
                if (sqlType == Types.BIGINT || sqlType == Types.DATE
                        || sqlType == Types.DECIMAL || sqlType == Types.DOUBLE
                        || sqlType == Types.FLOAT || sqlType == Types.INTEGER
                        || sqlType == Types.NUMERIC || sqlType == Types.REAL
                        || sqlType == Types.TIME || sqlType == Types.TIMESTAMP
                        || sqlType == Types.SMALLINT
                        || sqlType == Types.TINYINT || sqlType == Types.VARCHAR
                        || sqlType == Types.CHAR) {
                    comparableColumnName = columnName;
                    break;
                }
            }
            rs.close();
            String query = ";with res as "
                    + "(select "
                    + selectableColumns(table, null)
                    + ", "
                    + "row_number() over (order by "
                    + comparableColumnName
                    + ") as rownum,"
                    + "COUNT(1) over () as count "
                    + "from "
                    + table.getTableName()
                    + ") "
                    + "select * from res where rownum > %1$d and rownum <= (%1$d + %2$d)";
            return query;
        }
    }

    private static String createConditionalSelectLimitOffsetMSSQL(Table table,
            Configurator configurator) throws SQLException {
        try (Connection conn = DriverManager.getConnection(configurator
                .getConnectionString())) {
            String comparableColumnName = "";
            DatabaseMetaData metadata = conn.getMetaData();
            ResultSet rs = metadata.getColumns(null, null,
                    table.getTableName(), null);
            while (rs.next()) {
                String columnName = rs.getString("COLUMN_NAME");
                int sqlType = rs.getInt("DATA_TYPE");
                if (sqlType == Types.BIGINT || sqlType == Types.DATE
                        || sqlType == Types.DECIMAL || sqlType == Types.DOUBLE
                        || sqlType == Types.FLOAT || sqlType == Types.INTEGER
                        || sqlType == Types.NUMERIC || sqlType == Types.REAL
                        || sqlType == Types.TIME || sqlType == Types.TIMESTAMP
                        || sqlType == Types.SMALLINT
                        || sqlType == Types.TINYINT || sqlType == Types.VARCHAR
                        || sqlType == Types.CHAR) {
                    comparableColumnName = columnName;
                    break;
                }
            }
            rs.close();
            String query = ";with res as "
                    + "(select "
                    + selectableColumns(table, null)
                    + ", "
                    + "row_number() over (order by "
                    + comparableColumnName
                    + ") as rownum,"
                    + "COUNT(1) over () as count "
                    + "from "
                    + table.getTableName()
                    + " where %3$s = ?) "
                    + "select * from res where rownum > %1$d and rownum <= (%1$d + %2$d)";
            return query;
        }
    }
    
    private static String createSelectLimitOffsetPostgres(Table table) {
        String orderColumnName = "1";
        for (Column col : table.getColumns()) {
            int sqlType = col.getDataType();
            if (sqlType == Types.BIGINT || sqlType == Types.DATE
                    || sqlType == Types.DECIMAL || sqlType == Types.DOUBLE
                    || sqlType == Types.FLOAT || sqlType == Types.INTEGER
                    || sqlType == Types.NUMERIC || sqlType == Types.REAL
                    || sqlType == Types.TIME || sqlType == Types.TIMESTAMP
                    || sqlType == Types.SMALLINT || sqlType == Types.TINYINT
                    || sqlType == Types.VARCHAR || sqlType == Types.CHAR) {
                orderColumnName = col.getColumnName();
                break;
            }
        }
        String query = "select * from (select " + selectableColumns(table, "c")
                + ",count(*) over () as count from " + table.getTableName()
                + " c order by " + orderColumnName
                + " ) t limit %2$d offset %1$d";
        return query;
    }
    
    private static String createConditionalSelectLimitOffsetPostgres(Table table) {
        String orderColumnName = "1";
        for (Column col : table.getColumns()) {
            int sqlType = col.getDataType();
            if (sqlType == Types.BIGINT || sqlType == Types.DATE
                    || sqlType == Types.DECIMAL || sqlType == Types.DOUBLE
                    || sqlType == Types.FLOAT || sqlType == Types.INTEGER
                    || sqlType == Types.NUMERIC || sqlType == Types.REAL
                    || sqlType == Types.TIME || sqlType == Types.TIMESTAMP
                    || sqlType == Types.SMALLINT || sqlType == Types.TINYINT
                    || sqlType == Types.VARCHAR || sqlType == Types.CHAR) {
                orderColumnName = col.getColumnName();
                break;
            }
        }
        String query = "select * from (select " + selectableColumns(table, "c")
                + ",count(*) over () as count from " + table.getTableName() +" c "
                + " where c.%3$s = ? "
                + "  order by " + orderColumnName
                + " ) t limit %2$d offset %1$d";
        return query;
    }

    private static String createSelectLimitOffsetFB(Table table) {
        String orderColumnName = "1";
        for (Column col : table.getColumns()) {
            int sqlType = col.getDataType();
            if (sqlType == Types.BIGINT || sqlType == Types.DATE
                    || sqlType == Types.DECIMAL || sqlType == Types.DOUBLE
                    || sqlType == Types.FLOAT || sqlType == Types.INTEGER
                    || sqlType == Types.NUMERIC || sqlType == Types.REAL
                    || sqlType == Types.TIME || sqlType == Types.TIMESTAMP
                    || sqlType == Types.SMALLINT || sqlType == Types.TINYINT
                    || sqlType == Types.VARCHAR || sqlType == Types.CHAR) {
                orderColumnName = col.getColumnName();
                break;
            }
        }
        String query = "select first %2$d skip %1$d "
                + selectableColumns(table, "t") + ",(select count(1) from "
                + table.getTableName() + ") as \\\"count\\\"  from  ("
                + "select " + selectableColumns(table, null) + " from "
                + table.getTableName() + " order by " + orderColumnName
                + " ) t";
        return query;
    }
    
    private static String createConditionalSelectLimitOffsetFB(Table table) {
        String orderColumnName = "1";
        for (Column col : table.getColumns()) {
            int sqlType = col.getDataType();
            if (sqlType == Types.BIGINT || sqlType == Types.DATE
                    || sqlType == Types.DECIMAL || sqlType == Types.DOUBLE
                    || sqlType == Types.FLOAT || sqlType == Types.INTEGER
                    || sqlType == Types.NUMERIC || sqlType == Types.REAL
                    || sqlType == Types.TIME || sqlType == Types.TIMESTAMP
                    || sqlType == Types.SMALLINT || sqlType == Types.TINYINT
                    || sqlType == Types.VARCHAR || sqlType == Types.CHAR) {
                orderColumnName = col.getColumnName();
                break;
            }
        }
        String query = "select first %2$d skip %1$d "
                + selectableColumns(table, "t") + ",(select count(1) from "
                + table.getTableName() + "kk where kk.%3$s = t.%3$s) as \\\"count\\\"  from  ("
                + "select " + selectableColumns(table, null) + " from "
                + table.getTableName() + "where %3$s = ? order by " + orderColumnName
                + " ) t";
        return query;
    }

    public static String selectableColumns(Table table, String alias) {
        StringBuilder builder = new StringBuilder();
        for (Column col : table.getColumns()) {
            if (!col.getColumnName().contains("$")) {
                if (alias == null || alias.isEmpty()) {
                    builder.append(col.getColumnName()).append(", ");
                } else {
                    builder.append(alias).append(".")
                            .append(col.getColumnName()).append(", ");
                }
            }
        }
        // уберем лишнюю запятую
        if (!table.getColumns().isEmpty()) {
            builder.deleteCharAt(builder.length() - 2);
        }
        return builder.toString();

    }

}
