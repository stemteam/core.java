package net.stemteam.dbservice.generator.metadata;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import net.stemteam.datatransport.transport.DataColumn;
import net.stemteam.dbservice.cli.options.Configurator;
import net.stemteam.dbservice.generator.AbstractGenerator;
import net.stemteam.dbservice.helper.DatabaseHelper;
import net.stemteam.dbservice.metadata.MethodDescription;
import net.stemteam.dbservice.metadata.MethodParameterDescription;
import net.stemteam.dbservice.model.Table;
import net.stemteam.jaxis.db.crud.mapper.MethodNameMapper;
import net.stemteam.jaxis.db.crud.mapper.TelemedicMethodsNameMapper;
import net.stemteam.jaxis.db.crud.policy.MethodAccessPolicy;

import org.apache.commons.io.FileUtils;

public class MetadataSQLGenerator extends AbstractGenerator {

    private long methodGroupId;

    private long methodMaxId;

    private long fieldMaxId;

    public MetadataSQLGenerator(Configurator configurator) {
        super(configurator);
        methodGroupId = configurator.getMethodGroupId();
        methodMaxId = configurator.getMaxMethodId();
    }

    @Override
    public void generate() throws Exception {
        String request = generateSQL();
        File sqlFolder = new File(configurator.getTargetDir(), "metadata");
        sqlFolder.mkdir();
        File sqlText = new File(sqlFolder, "metadata.sql");
        FileUtils.write(sqlText, request);
    }

    private Long generateMethodId() {
        return ++methodMaxId;
    }

    private String generateSQL() throws SQLException {
        StringBuilder methodsBuilder = new StringBuilder();
        StringBuilder fieldsBuilder = new StringBuilder();
        List<MethodDescription> methods = configurator.getDescription();
        for (int i = 0; i < methods.size(); i++) {
            MethodDescription desc = methods.get(i);

            Long methodId = generateMethodId();
            methodsBuilder
                    .append("insert into stem_meta_method (id_stem_meta_method,name,description,id_stem_meta_methodgroup, is_immutable) values (");
            methodsBuilder.append(methodId).append(",'")
                    .append(configurator.getServicePath())
                    .append(desc.getMethodName()).append("','")
                    .append(desc.getDescription()).append("',")
                    .append(methodGroupId).append(",0);\n");
            fieldsBuilder.append("-- for method ").append(desc.getMethodName())
                    .append("\n");
            if (desc.getPolicy() == MethodAccessPolicy.SELECT) {
                for (MethodParameterDescription param : desc.getOutParameters()) {
                    fieldsBuilder
                            .append("insert into stem_meta_field (" + "id_stem_meta_method,"
                                    + "name," + "caption," + "is_id,"
                                    + "datatype," + "is_readonly,"
                                    + "is_hidden," + "is_treeparent,"
                                    + "is_treefield," + "is_mandatory,"
                                    + "relationmethod," + "relationfield) "
                                    + "values (").append(methodId).append(",'")
                            .append(param.getParameterName()).append("','")
                            .append(param.getParameterDescription())
                            .append("',");
                    if (param.isId()) {
                        fieldsBuilder.append("1,");
                    } else {
                        fieldsBuilder.append("0,");
                    }
                    fieldsBuilder.append("'");
                    if (param.getRelatedFieldName() != null
                            && !param.getRelatedFieldName().isEmpty()
                            && param.getRelatedMethodName() != null
                            && !param.getRelatedMethodName().isEmpty()) {
                        fieldsBuilder.append(DataColumn.NAVSTAT_DATASET);
                    } else {
                        fieldsBuilder.append(param.getParameterType());
                    }
                    fieldsBuilder.append("',").append("0,0,0,0,0,");
                    if (param.getRelatedFieldName() != null
                            && !param.getRelatedFieldName().isEmpty()
                            && param.getRelatedMethodName() != null
                            && !param.getRelatedMethodName().isEmpty()) {
                        fieldsBuilder.append("'")
                                .append(configurator.getServicePath())
                                .append(param.getRelatedMethodName())
                                .append("','")
                                .append(param.getRelatedFieldName())
                                .append("');\n");
                    } else {
                        fieldsBuilder.append("null,null);\n");
                    }
                }
                fieldsBuilder.append("\n");
            }
        }
        try (Connection conn = DriverManager.getConnection(configurator.getConnectionString())){
            List<Table> tables = DatabaseHelper.getDataBaseTables(conn, configurator);
            MethodNameMapper mapper = new TelemedicMethodsNameMapper();
            for (Table table:tables){
                String tableName = table.getTableName();
                String selectList = mapper.getMethodName(tableName, MethodAccessPolicy.SELECT, true);
                String selectOne = mapper.getMethodName(tableName, MethodAccessPolicy.SELECT, false);
                String edit = mapper.getMethodName(tableName, MethodAccessPolicy.UPDATE, false);
                String add = mapper.getMethodName(tableName, MethodAccessPolicy.INSERT, false);
                String delete = mapper.getMethodName(tableName, MethodAccessPolicy.DELETE, false);
                fieldsBuilder.append("insert into stem_meta_frame (get_method,list_get_method,edit_method,add_method,delete_method) values ('")
                .append(configurator.getServicePath()).append(selectOne).append("','")
                .append(configurator.getServicePath()).append(selectList).append("','")
                .append(configurator.getServicePath()).append(edit).append("','")
                .append(configurator.getServicePath()).append(add).append("','")
                .append(configurator.getServicePath()).append(delete).append("');\n");
            }
        }
        
        
        
        return methodsBuilder.append("\n").append(fieldsBuilder).toString();
    }
}
