package net.stemteam.dbservice.cli.options;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class OptionsFactory{
	private OptionsFactory(){};
	
	private static Options options; 
	
	public static synchronized Options getInstance(){
		if (options!= null){
			return options;
		} else {
			options = new Options();
			Option connectnionStringOption = new Option("s","connectionstring",true,"Connection string");
			connectnionStringOption.setRequired(false);
			options.addOption(connectnionStringOption);
			
			Option driverClassOption = new Option("j","driverclass",true,"JDBC Driver class");
			driverClassOption.setRequired(true);
			options.addOption(driverClassOption);
			
			options.addOption("h","schema",true,"Database schema name");
			options.addOption("v","view",false,"Include VIEWS to table list");
			
			options.addOption("sp","service-path",true,"Web service root context");
			
			options.addOption("p", "package", true,
					"Default package of generated classes");
		
			options.addOption("w", "wiki", false, "Generate docuWiki");
			options.addOption("wr","wiki-root",true,"Wiki documentation namespace");
			
			options.addOption("m", "metadata" ,false, "Generate metadata");
			options.addOption("c", "create", false, "Generate INSERT methods");
			options.addOption("r", "read",false, "Generate SELECT methods");
			options.addOption("u", "update", false,
					"Generate UPDATE methods");
			options.addOption("d", "delete", false, "Generate DELETE methods");
			
			options.addOption("mmi","max-method-id",true,"Max method id (need for metadata generation)");
			options.addOption("mfi","max-field-id",true,"Max field id (need for metadata generation)");
			options.addOption("mgi","method-group-id",true,"Max field id (need for metadata generation)");
			
			return options;
		}
	}
}
