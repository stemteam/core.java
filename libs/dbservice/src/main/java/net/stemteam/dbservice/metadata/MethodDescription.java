package net.stemteam.dbservice.metadata;

import java.util.ArrayList;
import java.util.List;

import net.stemteam.datatransport.transport.JaxisTypesSQLResolver;
import net.stemteam.jaxis.db.crud.mapper.MethodNameMapper;
import net.stemteam.jaxis.db.crud.mapper.TelemedicMethodsNameMapper;
import net.stemteam.jaxis.db.crud.policy.MethodAccessPolicy;

import net.stemteam.dbservice.model.Column;

public class MethodDescription {
    private String methodName;

    private String tableName;

    private String description;

    private List<MethodParameterDescription> inParameters = new ArrayList<>();

    private List<MethodParameterDescription> outParameters = new ArrayList<>();

    private MethodNameMapper mapper = new TelemedicMethodsNameMapper();

    private MethodAccessPolicy policy;

    public MethodDescription(MethodAccessPolicy policy) {
        this.policy = policy;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<MethodParameterDescription> getInParameters() {
        return inParameters;
    }

    public void addInParameter(MethodParameterDescription inParameter) {
        this.inParameters.add(inParameter);
    }

    public List<MethodParameterDescription> getOutParameters() {
        return outParameters;
    }

    public void addOutParameter(MethodParameterDescription outParameter) {
        this.outParameters.add(outParameter);
    }

    public void addOutParameters(List<Column> columns) {
        for (Column column : columns) {
            MethodParameterDescription desc = new MethodParameterDescription();
            desc.setParameterDescription(column.getColumnName());
            desc.setParameterName(column.getColumnName());
            desc.setParameterType(JaxisTypesSQLResolver
                    .resolveNavstatType(column.getDataType()));
            if (column.isForeignKey()) {
                desc.setRelatedFieldName(column.getRelatedField());
                desc.setRelatedMethodName(mapper.getMethodName(
                        column.getRelatedTable(), MethodAccessPolicy.SELECT,
                        true));
            }
            desc.setId(column.isPrimaryKey());
            outParameters.add(desc);
        }
    }

    public void addInParameters(List<Column> columns) {
        for (Column column : columns) {
            MethodParameterDescription desc = new MethodParameterDescription();
            desc.setParameterDescription(column.getColumnName());
            desc.setParameterName(column.getColumnName());
            desc.setParameterType(JaxisTypesSQLResolver
                    .resolveNavstatType(column.getDataType()));
            if (column.isForeignKey()) {
                desc.setRelatedFieldName(column.getRelatedField());
                desc.setRelatedMethodName(mapper.getMethodName(tableName,
                        MethodAccessPolicy.SELECT, true));
            }
            desc.setId(column.isPrimaryKey());
            inParameters.add(desc);
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addTokenParameter() {
        MethodParameterDescription token = new MethodParameterDescription();
        token.setParameterDescription("Token авторизации");
        token.setParameterName("Token");
        token.setParameterType("String");

        inParameters.add(token);
    }

    public void addApiKeyParameter() {
        MethodParameterDescription apikey = new MethodParameterDescription();
        apikey.setParameterDescription("API Ключ");
        apikey.setParameterName("ApyKey");
        apikey.setParameterType("String");

        inParameters.add(apikey);
    }

    public void addVersionParameter() {
        MethodParameterDescription version = new MethodParameterDescription();
        version.setParameterDescription("Версия");
        version.setParameterName("Version");
        version.setParameterType("String");

        inParameters.add(version);
    }

    public MethodAccessPolicy getPolicy() {
        return policy;
    }

}
