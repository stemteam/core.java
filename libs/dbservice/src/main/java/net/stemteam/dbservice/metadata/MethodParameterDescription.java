package net.stemteam.dbservice.metadata;

import net.stemteam.datatransport.transport.JaxisTypesSQLResolver;

public class MethodParameterDescription {
    private String parameterName;

    private String parameterDescription;

    private String parameterType;

    private String relatedMethodName;

    private String relatedFieldName;

    private boolean id;

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterDescription() {
        return parameterDescription;
    }

    public void setParameterDescription(String parameterDescription) {
        this.parameterDescription = parameterDescription;
    }

    public String getParameterType() {
        return parameterType;
    }

    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }

    public void setParameterType(int sqlType) {
        this.parameterType = JaxisTypesSQLResolver.resolveNavstatType(sqlType);
    }

    public String getRelatedMethodName() {
        return relatedMethodName;
    }

    public void setRelatedMethodName(String relatedMethodName) {
        this.relatedMethodName = relatedMethodName;
    }

    public String getRelatedFieldName() {
        return relatedFieldName;
    }

    public void setRelatedFieldName(String relatedFieldName) {
        this.relatedFieldName = relatedFieldName;
    }

    public boolean isId() {
        return id;
    }

    public void setId(boolean id) {
        this.id = id;
    }

}
