package net.stemteam.dbservice.main;

import net.stemteam.dbservice.cli.options.Configurator;
import net.stemteam.dbservice.cli.options.OptionsFactory;
import net.stemteam.dbservice.generator.delete.DeleteGenerator;
import net.stemteam.dbservice.generator.insert.InsertGenerator;
import net.stemteam.dbservice.generator.jaxis.IndexGenerator;
import net.stemteam.dbservice.generator.jaxis.JaxisGenerator;
import net.stemteam.dbservice.generator.metadata.MetadataSQLGenerator;
import net.stemteam.dbservice.generator.select.SelectGenerator;
import net.stemteam.dbservice.generator.update.UpdateGenerator;
import net.stemteam.dbservice.generator.wiki.WikiGenerator;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

public class Main {

    public static void main(String[] argv) throws Exception {

//	 String[] args = new String[] {
//	 "-j",
//	 "org.firebirdsql.jdbc.FBDriver",
//	 "-s",
//	 "jdbc:firebirdsql:95.213.143.62:C:\\Data\\INFOCLINICA.FDB?user=SYSDBA;password=OdVPcvc",
//	 "-u", "-r", "-d", "-c", "-w", "-m", "-p",
//	 "ru.telemediclab.infoclinic", "-sp", "auto/", "-wr",
//	 "back_infoclinic_api","-mmi","21","-mgi", "2" };
//	 for(int i=0;i<args.length;i++) {
//	     System.out.print(args[i]+" ");
//	 }
	 System.out.println();
//	String[] args = new String[] {
//		"-j",
//		"org.postgresql.Driver",
//		"-s",
//		"jdbc:postgresql://31.131.251.20/tmdb?user=tmadmin&password=pass",
//		 "-r" , "-h", "public", "-p", 
//		"ru.telemediclab.teledoctorservice.auto", "-sp", "auto/", "-m","-mmi","15","-mgi","2" };
	 String[] args = new String[] {
	 "-j",
	 "com.microsoft.sqlserver.jdbc.SQLServerDriver",
	 "-s",
	 "jdbc:sqlserver://10.2.0.97;database=medialog_CB;user=sa;password=testing",
	 "-u","-r","-d","-c", "-h", "dbo","-m", "-p",
	 "ru.telemediclab.medialog.auto", "-sp", "auto/", "-wr",
	 "back_medialog_api","-mmi","21","-mgi", "3"};
	Options options = OptionsFactory.getInstance();
	CommandLine commandLine = null;
	try {
	    CommandLineParser parser = new PosixParser();
	    commandLine = parser.parse(options, args);
	    System.out.println(commandLine.getArgList());
	} catch (ParseException pex) {
	    HelpFormatter formatter = new HelpFormatter();
	    formatter.printHelp("java -jar dbservice.jar", options, true);
	    return;
	}
	Configurator configurator = new Configurator(commandLine);
	try {
	    System.out.println("Testing DB...");
	    configurator.registerAndTestDB();
	    System.out.println("DB OK");
	} catch (Exception ex) {
	    ex.printStackTrace();
	    System.err.println(ex);
	    return;
	}
	if (configurator.isGenerateSelect()) {
	    System.out.println("Generate sources for Get and ListGet method");
	    SelectGenerator generator = new SelectGenerator(configurator);
	    generator.generate();
	    System.out.println("Sources for Get and ListGet method generated");
	}
	if (configurator.isGenerateDelete()) {
	    System.out.println("Generate sources for Delete method");
	    DeleteGenerator generator = new DeleteGenerator(configurator);
	    generator.generate();
	    System.out.println("Sources for Delete method generated");
	}
	if (configurator.isGenerateInsert()) {
	    System.out.println("Generate sources for Insert method");
	    InsertGenerator generator = new InsertGenerator(configurator);
	    generator.generate();
	    System.out.println("Sources for Insert method generated");
	}
	if (configurator.isGenerateUpdate()) {
	    System.out.println("Generate source for Update Method");
	    UpdateGenerator generator = new UpdateGenerator(configurator);
	    generator.generate();
	}
	if (configurator.isGenerateMetadata()) {
	    System.out.println("Generate sql source for metadata...");
	    MetadataSQLGenerator generator = new MetadataSQLGenerator(
		    configurator);
	    generator.generate();
	}
	System.out.println("Generating index service...");
	IndexGenerator indexGenerator = new IndexGenerator(configurator);
	indexGenerator.generate();

	System.out.println("Generating jaxis service class");
	JaxisGenerator jaxisGenerator = new JaxisGenerator(configurator);
	jaxisGenerator.generate();

	if (configurator.isGenerateWiki()) {
	    System.out.println("Generating wiki...");
	    WikiGenerator generator = new WikiGenerator(configurator);
	    generator.generate();
	}

	System.out.println("Finished!");
    }
}
