package net.stemteam.dbservice.generator.delete;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import net.stemteam.dbservice.cli.options.Configurator;
import net.stemteam.dbservice.generator.AbstractGenerator;
import net.stemteam.dbservice.helper.DatabaseHelper;
import net.stemteam.dbservice.helper.symbolic.SQLQueryFactory;
import net.stemteam.dbservice.helper.template.DeleteTemplateHelper;
import net.stemteam.dbservice.helper.template.SQLSelectByIdTemplateHelper;
import net.stemteam.dbservice.helper.template.SQLSelectListTemplateHelper;
import net.stemteam.dbservice.metadata.MethodDescription;
import net.stemteam.dbservice.metadata.MethodParameterDescription;
import net.stemteam.dbservice.model.Table;

import net.stemteam.jaxis.db.crud.mapper.TelemedicMethodsNameMapper;
import net.stemteam.jaxis.db.crud.policy.MethodAccessPolicy;

public class DeleteGenerator extends AbstractGenerator {
    private DeleteTemplateHelper templateHelper = null;

    public DeleteGenerator(Configurator congfigurator) {
        super(congfigurator);
        // TODO Auto-generated constructor stub
    }

    public void generate() throws Exception {
        System.out.println("start generating delete classes");
        if (templateHelper == null) {
            templateHelper = new DeleteTemplateHelper();
        }
        try (Connection conn = establishConnection()) {
            List<Table> tables = DatabaseHelper.getDataBaseTables(conn,
                    configurator);
            for (Table table : tables) {
                // создаем методы на основе таблиц
                String keyColumn = DatabaseHelper.getKeyColumnName(table);
                String pkg = configurator.getDefaultPackage();
                if (keyColumn != null && !keyColumn.isEmpty()) {
                    String methodName = getUniqueMethodName(
                            table.getTableName(), MethodAccessPolicy.DELETE,
                            false);
                    String sql = SQLQueryFactory.createDeleteQuery(table);
                    String source = templateHelper.generateSource(pkg,
                            methodName, table.getTableName(), sql, keyColumn);
                    sources.put(methodName, source);

                    MethodDescription desc = new MethodDescription(
                            MethodAccessPolicy.DELETE);
                    desc.setMethodName(methodName);
                    desc.setTableName(table.getTableName());
                    desc.setDescription("Удаление записи из таблицы "
                            + table.getTableName() + " по ключу");
                    desc.addApiKeyParameter();
                    desc.addTokenParameter();
                    desc.addVersionParameter();

                    MethodParameterDescription paramDesc = new MethodParameterDescription();
                    paramDesc
                            .setParameterDescription("Ключ для удаления записи");
                    paramDesc.setParameterName(keyColumn);
                    paramDesc.setParameterType(table.getColumnByName(keyColumn)
                            .getDataType());

                    desc.addInParameter(paramDesc);
                    configurator.getDescription().add(desc);
                    dumpToFile();
                    sources.clear();
                }
            }
        }
        
    }

}
