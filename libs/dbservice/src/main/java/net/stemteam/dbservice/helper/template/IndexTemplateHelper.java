package net.stemteam.dbservice.helper.template;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.SQLException;
import java.util.List;

public class IndexTemplateHelper {
	private String classTemplate;

	public IndexTemplateHelper() throws SQLException, IOException,
			FileNotFoundException {
		File file = new File("src/main/resources/template/index.template");
		if (!file.exists()) {
			file = new File("templates/index.template");
		}
		try (InputStream is = new FileInputStream(file);
				Reader streamReader = new InputStreamReader(is);
				BufferedReader reader = new BufferedReader(streamReader)) {
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append("\n");
			}
			classTemplate = builder.toString();
		}
	}

	public String generateSource(String pkg,String path, List<String> methods) {
		StringBuilder builder = new StringBuilder();
		
		
		for (String method : methods) {
			builder.append(
					"\t\t\tdataSet.createRecord().getCell(0).setValue(\"").append("/").append(path)
					.append(method).append("\");\n");
		}
		return String.format(classTemplate, pkg, builder.toString());
	}
}
