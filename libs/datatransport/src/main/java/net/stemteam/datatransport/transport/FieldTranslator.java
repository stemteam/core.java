package net.stemteam.datatransport.transport;

import java.util.HashMap;

/**
 * Транслятор имен полей
 *
 * Суть в том, чтобы преобразовывать имена полей источника в требуемые имена
 * (при необходимости такого преобразования)
 *
 * Важный момент: OCI JDBC драйвер возвращает имена полей в верхнем регистре,
 * т.е. id_mon_object возвратится как ID_MON_OBJECT
 *
 * Пример: // создаем транслятор полей FieldTranslator fieldTranslator = new
 * FieldTranslator(); // заполняем соответствия полей, необходимых к
 * переименованию (если имя поля должно остаться неизменным заполнять для него
 * не обязательно) fieldTranslator.AddItem("ID", "Id");
 * fieldTranslator.AddItem("NAME", "Name"); fieldTranslator.AddItem("PARENT_ID",
 * "Parent"); fieldTranslator.AddItem("CLEVEL", "CLevel");
 * fieldTranslator.AddItem("CCOUNT", "CCount"); fieldTranslator.AddItem("GUID",
 * "GuidAbonent"); // передаем объект в процедуру упаковки java.sql.ResultSet в
 * DataSet dataSet.PackData(rs, fieldTranslator);
 *
 * @author Roman E. Kasovskiy
 * @release medved
 */
public class FieldTranslator extends HashMap<String, String> {

    /**
     * Тип БД = Oracle
     */
    private static final int DB_TYPE_ORACLE = 1;
    private static final int DB_TYPE_POSTGRES = 2;

    /**
     * Текущий тип БД
     */
    private int dbType = DB_TYPE_ORACLE; // по умолчанию Oracle

    /**
     * Устанавливает тип трансляции из Oracle DB
     */
    public void setDbTypeOracle() {
        this.dbType = DB_TYPE_ORACLE;
    }

    /**
     * Устанавливает тип трансляции из Postgres DB
     */
    public void setDbTypePostgres() {
        this.dbType = DB_TYPE_POSTGRES;
    }

    public boolean isDbTypeOracle() {
        return this.dbType == DB_TYPE_ORACLE;
    }

    public boolean isDbTypePostgres() {
        return this.dbType == DB_TYPE_POSTGRES;
    }

    private final HashMap<String, DataColumnType> fieldTypes = new HashMap();

    public FieldTranslator() {
    }

    /**
     * Добавляет элемент сопоставления имен полей
     *
     * @param oldField старое название поля
     * @param newField новое название поля
     */
    public void AddItem(String oldField, String newField) {
        // поднимаем для регистронезависимого поиска при трансляции имени
        oldField = oldField.toUpperCase();
        if (newField.equals("*")) {
            newField = oldField;
        }
        // запоминаем в карту
        this.put(oldField, newField);
        this.fieldTypes.put(newField, null);
    }

    /**
     * Добавляет элемент сопоставления имен полей
     *
     * @param oldField старое название поля
     * @param newField новое название поля
     * @param type
     * @deprecated Следует использовать метод с указанием типа в формате
     * DataColumnType
     */
    @Deprecated
    public void AddItem(String oldField, String newField, String type) {
        // поднимаем для регистронезависимого поиска при трансляции имени
        oldField = oldField.toUpperCase();
        if (newField.equals("*")) {
            newField = oldField;
        }
        // запоминаем в карту
        this.put(oldField, newField);
        this.fieldTypes.put(newField, DataColumnType.getByName(type));
    }

    /**
     * Добавляет элемент сопоставления имен и типов полей
     * @param oldField
     * @param newField
     * @param type 
     */
    public void AddItem(String oldField, String newField, DataColumnType type) {
        // поднимаем для регистронезависимого поиска при трансляции имени
        oldField = oldField.toUpperCase();
        if (newField.equals("*")) {
            newField = oldField;
        }
        // запоминаем в карту
        this.put(oldField, newField);
        this.fieldTypes.put(newField, type);
    }

    /**
     * Транслирует старое имя поля в новое
     *
     * Если не находит трансляции - возвращает старое имя поля
     *
     * @param oldName
     * @return
     */
    public String TranslateName(String oldName) {
        // поднимаем для регистронезависимого поиска при трансляции имени
        oldName = oldName.toUpperCase();
        String s = this.get(oldName);
        if (s != null) {
            return s;
        } else {
            return oldName;
        }
    }

    public DataColumnType TranslateType(String fieldname, DataColumnType dataType) throws Exception {
        DataColumnType t = fieldTypes.get(fieldname);
        if (t == null) {
            return dataType;
        }
        return t;
    }

}
