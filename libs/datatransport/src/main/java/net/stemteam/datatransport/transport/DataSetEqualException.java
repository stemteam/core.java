package net.stemteam.datatransport.transport;

/**
 * Created by IntelliJ IDEA.
 * User: poltora
 * Date: 10.05.11
 * Time: 19:57
 * To change this template use File | Settings | File Templates.
 */

/**
 * Исключение - несоответствие DataSet'ов при добавлении друг к другу
 */
public class DataSetEqualException extends Exception{
    public DataSetEqualException (String s){
        super(s);
    }
    public DataSetEqualException() {
        super();
    }
}
