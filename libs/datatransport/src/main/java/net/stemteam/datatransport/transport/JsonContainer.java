/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.transport;

import java.util.Vector;

/**
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class JsonContainer {
    
    private Vector<String> cols;
    private Vector<String> types;
    private Vector<Vector<Object>> rows = new Vector();
    
    public Vector<String> getCols() {
        return cols;
    }
    public void setCols(Vector<String> value) {
        cols = value;
    }

    public Vector<String> getTypes() {
        return types;
    }

    public void setTypes(Vector<String> types) {
        this.types = types;
    }

    public Vector<Vector<Object>> getRows() {
        return rows;
    }

    public void setRows(Vector<Vector<Object>> data) {
        this.rows = data;
    }
    
    public JsonContainer() {
    }
    
}
