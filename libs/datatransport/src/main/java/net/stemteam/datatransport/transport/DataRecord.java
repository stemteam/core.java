package net.stemteam.datatransport.transport;

import net.stemteam.datatransport.exception.ColumnNotFoundException;
import net.stemteam.datatransport.tool.IkGeometry;
import java.math.BigDecimal;
import java.util.Vector;

/**
 * Запись датасета (она же вектор объектов)
 *
 * @author medved
 */
public class DataRecord {

    // данные полей
    private Vector<DataCell> cells;

    /**
     * Возвращает список полей записи
     *
     * @return
     */
    public Vector<DataCell> getCells() {
        return cells;
    }

    /**
     * Устанавливает список полей записи
     *
     * @param cells
     * @throws Exception Возникает при попытке установить поля записи в null
     */
    public void setCells(Vector<DataCell> cells) throws Exception {
        // запрещаем пустые поля
        if (cells == null) {
            throw new Exception("DataRecord: Ячейки не могут быть NULL");
        }
        this.cells = cells;
    }

    /**
     * Конструктор (инициализирует вектор)
     */
    public DataRecord() {
        this.cells = new Vector();
    }

    /**
     * Возвращает данные записи в виде строки
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder stringData = new StringBuilder();
        boolean is_first = true;
        for (DataCell cell : this.cells) {
            if (is_first) {
                stringData.append(cell.toString());
                is_first = false;
            } else {
                stringData.append(DataSet.COLUMN_SEPARATOR);
                stringData.append(cell.toString());
            }
        }
        return stringData.toString();
    }

    /**
     * Возвращает данные записи в виде строки
     *
     * @return
     */
    public String toDump() {
        StringBuilder stringData = new StringBuilder();
        boolean is_first = true;
        for (DataCell cell : this.cells) {
            if (is_first) {
                stringData.append(cell.toDump());
                is_first = false;
            } else {
                stringData.append(DataSet.COLUMN_SEPARATOR);
                stringData.append(cell.toDump());
            }
        }
        return stringData.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="Методы доступа">
    public Integer getInteger(int Index) throws ColumnNotFoundException {
        return getCell(Index).getInteger();
    }

    public Integer getInteger(String columnName) throws ColumnNotFoundException {
        return getCell(columnName).getInteger();
    }

    public Long getLong(int Index) throws ColumnNotFoundException {
        return getCell(Index).getLong();
    }

    public Long getLong(String columnName) throws ColumnNotFoundException {
        return getCell(columnName).getLong();
    }

    /**
     * Возвращает значение ячейки
     *
     * @param Index индекс
     * @return
     * @throws ColumnNotFoundException
     */
    public Object getValue(int Index) throws ColumnNotFoundException {
        return getCell(Index).getValue();
    }

    /**
     * Возвращает значение ячейки
     *
     * @param columnName название поля
     * @return
     * @throws ColumnNotFoundException
     */
    public Object getValue(String columnName) throws ColumnNotFoundException {
        return getCell(columnName).getValue();
    }

    public String getString(int Index) throws ColumnNotFoundException {
        return getCell(Index).getString();
    }

    public String getString(String columnName) throws ColumnNotFoundException {
        return getCell(columnName).getString();
    }

    public java.sql.Timestamp getTimestamp(int Index) throws ColumnNotFoundException {
        return getCell(Index).getTimestamp();
    }

    public java.sql.Timestamp getTimestamp(String columnName) throws ColumnNotFoundException {
        return getCell(columnName).getTimestamp();
    }

    public Double getDouble(int Index) throws ColumnNotFoundException {
        return getCell(Index).getDouble();
    }

    public Double getDouble(String columnName) throws ColumnNotFoundException {
        return getCell(columnName).getDouble();
    }

    public Boolean getBoolean(int Index) throws ColumnNotFoundException {
        return getCell(Index).getBoolean();
    }

    public Boolean getBoolean(String columnName) throws ColumnNotFoundException {
        return getCell(columnName).getBoolean();
    }

    public Float getFloat(int Index) throws ColumnNotFoundException {
        return getCell(Index).getFloat();
    }

    public Float getFloat(String columnName) throws ColumnNotFoundException {
        return getCell(columnName).getFloat();
    }

    public Number getNumber(int Index) throws ColumnNotFoundException {
        return getCell(Index).getNumber();
    }

    public Number getNumber(String columnName) throws ColumnNotFoundException {
        return getCell(columnName).getNumber();
    }

    public BigDecimal getBigDecimal(int Index) throws ColumnNotFoundException {
        return getCell(Index).getBigDecimal();
    }

    public BigDecimal getBigDecimal(String columnName) throws ColumnNotFoundException {
        return getCell(columnName).getBigDecimal();
    }

    public IkGeometry getGeometry(int index) throws ColumnNotFoundException {
        return getCell(index).getGeometry();
    }

    public IkGeometry getGeometry(String columnName) throws ColumnNotFoundException {
        return getCell(columnName).getGeometry();
    }

    public DataCell getCell(int index) throws ColumnNotFoundException {
        try {
            return this.cells.get(index);
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new ColumnNotFoundException(String.format("Колонка с индексом %1$d не найдена в DataSet.", index));
        }
    }

    /**
     * Возвращает ячейку по имени колонки
     *
     * @param columnName имя колонки
     * @return колонка
     * @throws ColumnNotFoundException колонка с указанным именем не найдена в наборе
     */
    public DataCell getCell(String columnName) throws ColumnNotFoundException {
        for (DataCell cell : this.cells) {
            if (cell.getColumn().getName().equalsIgnoreCase(columnName)) {
                return cell;
            }
        }
        throw new ColumnNotFoundException(String.format("Колонка с именем %1$s не найдена в DataSet.", columnName));
    }

    public void addCell(DataCell cell) {
        this.cells.add(cell);
    }

    public void addCell(int index, DataCell cell) {
        this.cells.add(index, cell);
    }

    /**
     * Возвращает DataSet значение колонки по индексу колонки
     *
     * @param Index индекс колонки
     * @return DataSet
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public DataSet getDataSet(int Index) throws ColumnNotFoundException {
        return getCell(Index).getDataSet();
    }

    /**
     * Возвращает DataSet значение колонки по названию колонки
     *
     * @param columnName название колонки
     * @return DataSet
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public DataSet getDataSet(String columnName) throws ColumnNotFoundException {
        return getCell(columnName).getDataSet();
    }

    /**
     * Удаление столбца по заданному индексу
     *
     * @param index индекс для удаления
     */
    public void removeCell(int index) {
        this.cells.remove(index);
    }

    // новые методы доступа, использовать для записей, добавленных методом 
    // DataSet.appendRecord(), т.е. для записей к которых инициализированы 
    // DataCell
    //<editor-fold defaultstate="collapsed" desc="новые методы доступа">
    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnIndex индекс колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setString(int columnIndex, String value) throws ColumnNotFoundException {
        this.getCell(columnIndex).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnName название колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setString(String columnName, String value) throws ColumnNotFoundException {
        this.getCell(columnName).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnIndex индекс колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setInt(int columnIndex, Integer value) throws ColumnNotFoundException {
        this.getCell(columnIndex).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnName название колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setInt(String columnName, Integer value) throws ColumnNotFoundException {
        this.getCell(columnName).setValue(value);
    }

    public void setLong(String columnName, Long value) throws ColumnNotFoundException {
        this.getCell(columnName).setValue(value);
    }

    public void setLong(int columnIndex, Integer value) throws ColumnNotFoundException {
        this.getCell(columnIndex).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnIndex индекс колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setBoolean(int columnIndex, Boolean value) throws ColumnNotFoundException {
        this.getCell(columnIndex).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnName название колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setBoolean(String columnName, Boolean value) throws ColumnNotFoundException {
        this.getCell(columnName).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnIndex индекс колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setTimestamp(int columnIndex, java.sql.Timestamp value) throws ColumnNotFoundException {
        this.getCell(columnIndex).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnName название колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setTimestamp(String columnName, java.sql.Timestamp value) throws ColumnNotFoundException {
        this.getCell(columnName).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnIndex индекс колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setDouble(int columnIndex, Double value) throws ColumnNotFoundException {
        this.getCell(columnIndex).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnName название колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setDouble(String columnName, Double value) throws ColumnNotFoundException {
        this.getCell(columnName).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnIndex индекс колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setFloat(int columnIndex, Float value) throws ColumnNotFoundException {
        this.getCell(columnIndex).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnName название колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setFloat(String columnName, Float value) throws ColumnNotFoundException {
        this.getCell(columnName).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnIndex индекс колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setDataSet(int columnIndex, DataSet value) throws ColumnNotFoundException {
        this.getCell(columnIndex).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnName название колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setDataSet(String columnName, DataSet value) throws ColumnNotFoundException {
        this.getCell(columnName).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnIndex индекс колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setValue(int columnIndex, Object value) throws ColumnNotFoundException {
        this.getCell(columnIndex).setValue(value);
    }

    /**
     * Устанавливает значение в DataCell записи. DataCell должен быть инициализирован.
     *
     * @param columnName название колонки
     * @param value значение
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public void setValue(String columnName, Object value) throws ColumnNotFoundException {
        this.getCell(columnName).setValue(value);
    }

}
