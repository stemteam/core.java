/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.exception;

/**
 * Колонка с указанным именем или индексом не найдена в наборе колонок датасета
 * 
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class ColumnNotFoundException extends DataSetException {

    public ColumnNotFoundException(String message) {
        super(message);
    }
    
}
