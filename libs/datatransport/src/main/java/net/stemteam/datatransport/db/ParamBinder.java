package net.stemteam.datatransport.db;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Создано для того чтобы облегчить жизнь при установке параметров в 
 * PreparedStatement, CallableStatement при условии возможности NULL значений 
 * параметров
 *  
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class ParamBinder {
    
    /**
     * Sets the designated parameter to the given Java <code>boolean</code> value.
     * The driver converts this
     * to an SQL <code>BIT</code> or <code>BOOLEAN</code> value when it sends it to the database.
     *
     * NEW: If value is NULL used setNull void
     * 
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; 
     * if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setBoolean(PreparedStatement ps, int parameterIndex, Boolean x) throws SQLException {
        if (x != null) {
            ps.setBoolean(parameterIndex, x);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.BOOLEAN);
        }
    }

    /**
     * Sets the designated parameter to the given Java <code>byte</code> value.  
     * The driver converts this
     * to an SQL <code>TINYINT</code> value when it sends it to the database.
     *
     * NEW: If value is NULL used setNull void
     * 
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setByte(PreparedStatement ps, int parameterIndex, Byte x) throws SQLException {
        if (x != null) {
            ps.setByte(parameterIndex, x);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.TINYINT);
        }
    }

    /**
     * Sets the designated parameter to the given Java <code>short</code> value. 
     * The driver converts this
     * to an SQL <code>SMALLINT</code> value when it sends it to the database.
     *
     * NEW: If value is NULL used setNull void
     * 
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setShort(PreparedStatement ps, int parameterIndex, Short x) throws SQLException {
        if (x != null) {
            ps.setShort(parameterIndex, x);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.SMALLINT);
        }
    }

    /**
     * Sets the designated parameter to the given Java <code>int</code> value.  
     * The driver converts this
     * to an SQL <code>INTEGER</code> value when it sends it to the database.
     *
     * NEW: If value is NULL used setNull void
     * 
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setInt(PreparedStatement ps, int parameterIndex, Integer x) throws SQLException {
        if (x != null) {
            ps.setInt(parameterIndex, x);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.INTEGER);
        }
    }

    /**
     * Sets the designated parameter to the given Java <code>long</code> value. 
     * The driver converts this
     * to an SQL <code>BIGINT</code> value when it sends it to the database.
     *
     * NEW: If value is NULL used setNull void
     * 
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setLong(PreparedStatement ps, int parameterIndex, Long x) throws SQLException {
        if (x != null) {
            ps.setLong(parameterIndex, x);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.BIGINT);
        }
    }

    /**
     * Sets the designated parameter to the given Java <code>float</code> value. 
     * The driver converts this
     * to an SQL <code>REAL</code> value when it sends it to the database.
     *
     * NEW: If value is NULL used setNull void
     * 
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setFloat(PreparedStatement ps, int parameterIndex, Float x) throws SQLException {
        if (x != null) {
            ps.setFloat(parameterIndex, x);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.REAL);
        }
    }

    /**
     * Sets the designated parameter to the given Java <code>double</code> value.  
     * The driver converts this
     * to an SQL <code>DOUBLE</code> value when it sends it to the database.
     *
     * NEW: If value is NULL used setNull void
     * 
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setDouble(PreparedStatement ps, int parameterIndex, Double x) throws SQLException {
        if (x != null) {
            ps.setDouble(parameterIndex, x);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.DOUBLE);
        }
    }

    /**
     * Sets the designated parameter to the given <code>java.math.BigDecimal</code> value.  
     * The driver converts this to an SQL <code>NUMERIC</code> value when
     * it sends it to the database.
     *
     * NEW: If value is NULL used setNull void
     * 
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setBigDecimal(PreparedStatement ps, int parameterIndex, BigDecimal x) throws SQLException {
        if (x != null) {
            ps.setBigDecimal(parameterIndex, x);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.NUMERIC);
        }
    }

    /**
     * Sets the designated parameter to the given Java <code>String</code> value. 
     * The driver converts this
     * to an SQL <code>VARCHAR</code> or <code>LONGVARCHAR</code> value
     * (depending on the argument's
     * size relative to the driver's limits on <code>VARCHAR</code> values)
     * when it sends it to the database.
     *
     * NEW: If value is NULL used setNull void
     * 
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setString(PreparedStatement ps, int parameterIndex, String x) throws SQLException {
        if (x != null) {
            ps.setString(parameterIndex, x);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.VARCHAR);
        }
    }

    /**
     * Sets the designated parameter to the given <code>java.sql.Date</code> value
     * using the default time zone of the virtual machine that is running
     * the application. 
     * The driver converts this
     * to an SQL <code>DATE</code> value when it sends it to the database.
     *
     * NEW: If value is NULL used setNull void
     * 
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setDate(PreparedStatement ps, int parameterIndex, java.sql.Date x)
	    throws SQLException {
        if (x != null) {
            ps.setDate(parameterIndex, x);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.DATE);
        }
    }

    /**
     * Sets the designated parameter to the given <code>java.sql.Time</code> value.  
     * The driver converts this
     * to an SQL <code>TIME</code> value when it sends it to the database.
     *
     * NEW: If value is NULL used setNull void
     * 
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setTime(PreparedStatement ps, int parameterIndex, java.sql.Time x) 
	    throws SQLException {
        if (x != null) {
            ps.setTime(parameterIndex, x);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.TIME);
        }
    }

    /**
     * Sets the designated parameter to the given <code>java.sql.Timestamp</code> value.  
     * The driver
     * converts this to an SQL <code>TIMESTAMP</code> value when it sends it to the
     * database.
     *
     * NEW: If value is NULL used setNull void
     * 
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value 
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>     */
    public static void setTimestamp(PreparedStatement ps, int parameterIndex, java.sql.Timestamp x)
	    throws SQLException {
        if (x != null) {
            ps.setTimestamp(parameterIndex, x);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.TIMESTAMP);
        }
    }
    
    public static void setTimestamp(PreparedStatement ps, int parameterIndex, java.sql.Timestamp x, boolean isPostgres)
	    throws SQLException {
        if (x != null) {
            ps.setTimestamp(parameterIndex, x);
        } else {
            if (isPostgres) {
                // если постгресу указать java.sql.Types.TIMESTAMP - оно будет понимать тип параметра как timestamp with timezone, а если java.sql.Types.OTHER - то будетт понимать как timestamp without time zone
                ps.setNull(parameterIndex, java.sql.Types.OTHER);
            } else {
                ps.setNull(parameterIndex, java.sql.Types.TIMESTAMP);
            }
        }
    }

    /**
     * Sets the designated parameter to the given input stream, which will have 
     * the specified number of bytes.
     * When a very large ASCII value is input to a <code>LONGVARCHAR</code>
     * parameter, it may be more practical to send it via a
     * <code>java.io.InputStream</code>. Data will be read from the stream
     * as needed until end-of-file is reached.  The JDBC driver will
     * do any necessary conversion from ASCII to the database char format.
     * 
     * <P><B>Note:</B> This stream object can either be a standard
     * Java stream object or your own subclass that implements the
     * standard interface.
     * 
     * NEW: If value is NULL used setNull void
     *
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the Java input stream that contains the ASCII parameter value
     * @param length the number of bytes in the stream 
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setAsciiStream(PreparedStatement ps, int parameterIndex, java.io.InputStream x, int length)
	    throws SQLException {
        if (x != null) {
            ps.setAsciiStream(parameterIndex, x, length);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.LONGVARCHAR);
        }
    }

    /**
     * Sets the designated parameter to the given input stream, which will have 
     * the specified number of bytes.
     * When a very large binary value is input to a <code>LONGVARBINARY</code>
     * parameter, it may be more practical to send it via a
     * <code>java.io.InputStream</code> object. The data will be read from the 
     * stream as needed until end-of-file is reached.
     * 
     * <P><B>Note:</B> This stream object can either be a standard
     * Java stream object or your own subclass that implements the
     * standard interface.
     * 
     * NEW: If value is NULL used setNull void
     *
     * @param ps the instance of PreparedStatement
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the java input stream which contains the binary parameter value
     * @param length the number of bytes in the stream 
     * @exception SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or 
     * this method is called on a closed <code>PreparedStatement</code>
     */
    public static void setBinaryStream(PreparedStatement ps, int parameterIndex, java.io.InputStream x, 
			 int length) throws SQLException {
        if (x != null) {
            ps.setBinaryStream(parameterIndex, x, length);
        } else {
            ps.setNull(parameterIndex, java.sql.Types.LONGVARBINARY);
        }
    }
    
}
