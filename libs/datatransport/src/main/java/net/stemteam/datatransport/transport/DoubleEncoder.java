package net.stemteam.datatransport.transport;

import java.text.DecimalFormatSymbols;
import java.text.ParseException;

/**
 *
 * @author root
 */
public class DoubleEncoder {

    public static Double decodeDouble(String str) throws ParseException{
        //char chDec = new DecimalFormatSymbols().getDecimalSeparator();
        String safe = str;
        safe = safe.replace(',', '.'); // floating point literals according to the Java Language Specification
        //safe = safe.replace('.', chDec);
        return Double.valueOf(safe);
    }

}
