/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.examples;

import net.stemteam.datatransport.transport.*;

/**
 * Пример работы с вложенными датасетами. Для запуска используйте Run File.
 * Можно использовать для проверки работоспособности методов запаковки датасета 
 * в транспорт, распаковки из транспорта, обращение к колонкам по имени и т.д.
 * 
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class NestedDataSetExample {
    
    public static void main(String[] args) throws Exception {
        
        // создаем вложенный датасет 
        DataSet nestedDataSet = createNestedDataSet();
        
        // создаем датасет, в который будем вкладывать
        DataSet dataSet = new DataSet();
        // создаем колонки
        dataSet.createColumn("Info", DataColumnType.STRING);
        dataSet.createColumn("Data", DataColumnType.DATASET);
        // создаем запись в датасете (метод может быть вызван только после создания колонок)
        DataRecord record = dataSet.createRecord();
        record.setString("Info", "Законы Ньютона");
        record.setDataSet("Data", nestedDataSet);
        
        // печатаем датасет
        System.out.println("-- Подготовлен датасет -----------");
        System.out.println("Info:");
        System.out.println(dataSet.getRecords().get(0).getString("Info"));
        System.out.println("Data:");
        System.out.println(dataSet.getRecords().get(0).getDataSet("Data").toDump());
        
        // зарпаковываем его в транспорт
        System.out.println("-- Сформирован транспорт -----------");
        DataTransport tr = new DataTransport();
        tr.applyDataSet(dataSet);
        System.out.println("hash:");
        System.out.println(tr.getHash());
        System.out.println("data:");
        System.out.println(tr.getData());
        System.out.println("error:");
        System.out.println(tr.getError());
        
        // распаковываем из транспорта датасет
        DataTransport trNew = new DataTransport();
        // из хэша и даты
        trNew.applyWebRequest(tr.getHash(), tr.getData());
        // или можно еще так
        trNew.fromString(tr.toString());
        // получаем датасет
        DataSet dsNew = trNew.getDataSet();
        
        // проверяем что все распаковано корректно
        System.out.println("-- Получен датасет -----------");
        System.out.println("Info:");
        System.out.println(dsNew.getRecords().get(0).getString("Info"));
        System.out.println("Data:");
        System.out.println(dsNew.getRecords().get(0).getDataSet("Data").toDump());
        
        System.out.println("Выполнено.");
    }
    
    /**
     * Создает наполненый данными датасет для вложения в другой
     * @return
     * @throws Exception 
     */
    private static DataSet createNestedDataSet() throws Exception {
        DataRecord record;
        DataSet nestedDataSet = new DataSet();
        // создаем колонки
        nestedDataSet.createColumn("NUM", DataColumnType.INT16);
        nestedDataSet.createColumn("DESCRIPTION", DataColumnType.STRING);
        // создаем записи в датасете (метод может быть вызван только после создания колонок)
        record = nestedDataSet.createRecord();
        record.setInt("NUM", 1);
        record.setString("DESCRIPTION", "Существуют такие системы отсчёта, называемые инерциальными, относительно которых материальная точка при отсутствии внешних воздействий сохраняет величину и направление своей скорости неограниченно долго.");
        record = nestedDataSet.createRecord();
        record.setInt("NUM", 2);
        record.setString("DESCRIPTION", "В инерциальной системе отсчета ускорение, которое получает материальная точка, прямо пропорционально равнодействующей всех приложенных к ней сил и обратно пропорционально её массе.");
        record = nestedDataSet.createRecord();
        record.setInt("NUM", 3);
        record.setString("DESCRIPTION", "Действию всегда есть равное и противоположное противодействие, иначе — взаимодействия двух тел друг на друга равны и направлены в противоположные стороны.");
        return nestedDataSet;
    }
    
}
