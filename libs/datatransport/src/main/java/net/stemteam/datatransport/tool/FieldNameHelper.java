package net.stemteam.datatransport.tool;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

public class FieldNameHelper {

    /**
     * Используется для преобразование имен полей в CamelCase формат.
     *
     * @param source исходная строка
     * @param invertPk осуществлять ли преобразование IdTable => TableId
     * @return
     */
    public static String toCamelCase(String source, boolean invertPk) {
        if (invertPk) {
            if ((source != null) && (source.toUpperCase().indexOf("ID_") == 0)) {
                source = source.substring(3) + "_ID";
            }
        }
        return StringUtils.remove(WordUtils.capitalizeFully(source, new char[]{'_'}), "_");
    }
}
