/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.stemteam.datatransport.tool;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Работа с MD5
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class Md5 {

    /**
     * Вычисляет MD5 
     * @param data
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static byte[] getMd5(byte[] data) throws NoSuchAlgorithmException {
        MessageDigest md;
        md = MessageDigest.getInstance("MD5");
        md.update(data);
        return md.digest();
    }

    /**
     * Преобразовывает md5 к строковому представлению, получается HexString (32 символа)
     * @param md5
     * @return
     */
    public static String md5ToString(byte[] md5)
    {
        String result = "";
        for (int i = 0; i < md5.length; ++i)
            result += (Integer.toHexString(0x0100 + (md5[i] & 0x00FF)).substring(1));
        return result;
    }

}
