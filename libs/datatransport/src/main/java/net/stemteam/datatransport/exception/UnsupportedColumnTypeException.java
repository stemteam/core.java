/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.exception;

/**
 * Исключение: тип данных колонки не поддерживается транспортом
 * 
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class UnsupportedColumnTypeException extends Exception {

    public UnsupportedColumnTypeException(String message) {
        super(message);
    }
    
    
    
}
