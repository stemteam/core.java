package net.stemteam.datatransport.transport;

/**
 * Created by IntelliJ IDEA.
 * User: poltora
 * Date: 23.05.11
 * Time: 12:28
 * To change this template use File | Settings | File Templates.
 */
public class DataCellException extends Exception {
    public DataCellException(String s) {
        super(s);
    }
    public DataCellException(){
        super();
    }
}
