/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.stemteam.datatransport.exception;

import java.sql.SQLException;

/**
 * Ошибка закрытия соединения с БД
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class CloseDbConnectionException extends Exception {

    public CloseDbConnectionException(Throwable ex) {
        super(ex);
    }

    

}
