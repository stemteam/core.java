package net.stemteam.datatransport.transport;

/**
 * Метаданные (в нашем формате)
 *
 * @author Roman E. Kasovskiy, medved
 */
public class DataColumn {

    private String name;
    private Object defaultValue;
    private DataColumnType type;

    /**
     * Возвращает тип колонки
     *
     * @return
     */
    public DataColumnType getType() {
        return type;
    }

    /**
     * Возвращает название колонки
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Устанавливает новое имя колонки
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Возвращает внутренний тип данных
     *
     * @return
     */
    public String getDataTypeName() {
        return type.getName();
    }

    /**
     * Возвращает значение по умолчанию для колонки. Это используется в методах DataSet.createRecord() для инициализации
     * значения ячейки.
     *
     * @return
     */
    public Object getDefaultValue() {
        return defaultValue;
    }

    /**
     * Создать колонку с именем, типом и значением по умолчанию
     *
     * @param name
     * @param type
     * @param defaultValue
     */
    public DataColumn(String name, DataColumnType type, Object defaultValue) {
        this.name = name;
        this.type = type;
        this.defaultValue = defaultValue;
    }

    /**
     * Конструктор
     *
     * @param name
     * @param type
     */
    public DataColumn(String name, DataColumnType type) {
        this.name = name;
        this.type = type;
    }
}
