/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.examples;

import net.stemteam.datatransport.transport.StringEncoder;
import net.stemteam.datatransport.transport.*;
import java.util.Calendar;

/**
 * Добавление колонки в существующий набор данных DataSet с записями DataRecord
 * 
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class AddColumnExample {
    
    public static void main(String[] args) throws Exception {
        // датасет
        DataSet ds = prepareDataSetWithData();
                
        // добавляем новую колонку в существующий датасет
        ds.createColumn("NEWCOLUMN", DataColumnType.STRING);
        
        // изменяем значенее в записи
        ds.getRecords().get(0).setString("NEWCOLUMN", "Не тру?"); 
        
        // добавляем новую колонку в существующий датасет (заполняем записи указанным значением)
        ds.createColumn("STAFF", DataColumnType.STRING, "Писатель");
        
        // выводим датасет
        System.out.println(ds.toDump());
        
        String json = ds.toJson();
  //      System.out.println(json);
        
        // обратное преобразование
        DataSet dsN = DataSet.fromJson(json);
        System.out.println(dsN.toDump());

        
//        Calendar cal = Calendar.getInstance();
//        String ts = String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS.%1$tL", cal);
//        java.sql.Timestamp ts2 = StringEncoder.decodeTimeStamp(ts);
//        System.out.println(ts);
//        System.out.println(ts2);
        
//        java.sql.Timestamp ts3 = StringEncoder.decodeTimeStamp("20120517113849.540");
//        String ts4 = StringEncoder.encodeTimeStamp(ts3);
        java.sql.Timestamp ts3 = StringEncoder.decodeTimeStamp("20120517113849.540");
        String ts4 = StringEncoder.encodeTimeStamp(ts3);
        java.sql.Timestamp ts5 = StringEncoder.decodeTimeStamp(ts4);
        
        System.out.println(ts3);
        System.out.println(ts4);
        System.out.println(ts5);


//        DataTransport tr = new DataTransport();
//        tr.applyDataSet(ds);
//        System.out.println(tr.getHash());
//        System.out.println(tr.getData());

    }
    
    /**
     * Подготавливает датасет с данными
     * @return
     * @throws Exception 
     */
    private static DataSet prepareDataSetWithData() throws Exception {
        DataSet ds = new DataSet();
        ds.createColumn("ID", DataColumnType.INT32);
        ds.createColumn("NAME", DataColumnType.STRING);
        ds.createColumn("IS_TRUE", DataColumnType.BOOLEAN);
        ds.createColumn("BIRTHDAY", DataColumnType.DATETIME);
        DataRecord row = ds.createRecord();
        row.setInt("ID", 1);
        row.setString("NAME", "Александр Сергеевич Пушкин");
        row.setBoolean("IS_TRUE", false);
        row.setTimestamp("BIRTHDAY", new java.sql.Timestamp(System.currentTimeMillis()));
        row = ds.createRecord();
        row.setInt("ID", 2);
        row.setString("NAME", "Федор Михайлович Достоевский");
        row.setBoolean("IS_TRUE", true);
        row.setTimestamp("BIRTHDAY", new java.sql.Timestamp(System.currentTimeMillis()));
        row = ds.createRecord();
        row.setInt("ID", 3);
        row.setString("NAME", "Антон Павлович Чехов");
        row.setBoolean("IS_TRUE", null);
        row.setTimestamp("BIRTHDAY", new java.sql.Timestamp(System.currentTimeMillis()));
        return ds;
    }
}
