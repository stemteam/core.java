/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.stemteam.datatransport.tool;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Архиватор GZIP
 *
 * USAGE:
 *   new GzipArchiver().packFile("/table.sql", "/table.sql.gz");
 *   new GzipArchiver().unpackFile("/table.sql", "/table.sql.gz");
 *
 * @author medved
 */
public class GzipArchiver {

    /**
    * Сжатие файла методом GZIP
    * @param fromFileName - путь к файлу, которой сжимаем
    * @param toFileName - путь к файлу, в который сохраняем
    */
    public static void packFile(String fromFileName, String toFileName)
            throws Exception {
        FileInputStream inputStream = new FileInputStream(fromFileName);
        GZIPOutputStream gzipStream = new GZIPOutputStream(new FileOutputStream(toFileName));
        int READ_BLOCK_SIZE = 1024;  // размер блока чтения можно изменить
        byte[] buf = new byte[READ_BLOCK_SIZE];
        int len;
        while ((len = inputStream.read(buf)) > 0) {
            gzipStream.write(buf, 0, len);
        }
        gzipStream.flush();
        gzipStream.close();
        inputStream.close();
    }

    /**
     * Распаковка файла методом GZIP
     * @param fromFileName
     * @param toFileName
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void unpackFile(String fromFileName, String toFileName) throws FileNotFoundException, IOException{
        // Open the compressed file
        GZIPInputStream in = new GZIPInputStream(new FileInputStream(fromFileName));
        // Open the output file
        OutputStream out = new FileOutputStream(toFileName);
        // Transfer bytes from the compressed file to the output file
        int READ_BLOCK_SIZE = 1024;  // размер блока чтения можно изменить
        byte[] buf = new byte[READ_BLOCK_SIZE];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        // Close the file and stream
        in.close();
        out.close();
    }

    /**
     * Упаковывание байтового массива
     * @param array
     * @return
     * @throws IOException
     */
    public static byte[] packArray(byte[] array) throws IOException
    {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        GZIPOutputStream gzipStream = new GZIPOutputStream(byteStream);
        gzipStream.write(array);
        gzipStream.flush();
        gzipStream.close();
        return byteStream.toByteArray();
    }

    /**
     * Распаковывание байтового массива
     * @param array
     * @return распакованны массив байт
     * @throws IOException
     */
    public static byte[] unpackArray(byte[] array) throws IOException
    {
        ByteArrayInputStream byteStream = new ByteArrayInputStream(array);
        GZIPInputStream gzipStream = new GZIPInputStream(byteStream);
        ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
        // пишем в выходной поток распакованные данные
        int READ_BLOCK_SIZE = 1024;  // размер блока чтения можно изменить
        byte[] buf = new byte[READ_BLOCK_SIZE];
        int len;
        while ((len = gzipStream.read(buf)) > 0) {
            byteOutput.write(buf, 0, len);
        }
        gzipStream.close();
        return byteOutput.toByteArray();
    }

}
