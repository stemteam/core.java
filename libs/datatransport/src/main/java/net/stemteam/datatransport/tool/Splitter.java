/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.stemteam.datatransport.tool;

import java.util.Vector;

/**
 * Класс, предназначеный для разбиения строки в массив
 * @author medved
 */
public class Splitter {


    /**
     * Правильный split
     *
     * Паттерн НЕ на регулярных выражениях
     *
     * (  Есть такая тема со стандартным жабовым сплитом: он отсекает пустые
     * строки в конце, т.е. если сплитить строку
     *    String[] list = "1\t2\t\3\t\t\t\t\t\t".split("\t");
     * получится не 9 элементов, а всего 3, что неверно.
     *    Регулярные патерны также не всегда удобны, поскольку в них масса
     * зарезервированных символов которые при использовании дают иной результат)
     *
     * @param source
     * @param pattern разделитель (НЕ на регулярных выражениях)
     * @return вектор строк
     */
    public static Vector<String> Split(String source, String pattern) {
        int len = pattern.length();
        Vector<String> stringVector = new Vector<String>();
        if (source == null)
            return stringVector;
        // заменяем пустые значения на непечатаемый символ
        int idx = -1;
        int last_idx = 0;
        while ((idx = source.indexOf(pattern, last_idx)) != -1)
        {
            stringVector.add(source.substring(last_idx, idx));
            last_idx = idx + len;
        }
        // последний элемент
        stringVector.add(source.substring(last_idx));
        // возвращаем
        return stringVector;
    }

}
