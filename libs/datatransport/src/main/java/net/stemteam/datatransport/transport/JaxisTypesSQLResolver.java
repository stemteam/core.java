package net.stemteam.datatransport.transport;

import java.sql.Array;
import java.sql.SQLException;
import java.sql.Types;

public class JaxisTypesSQLResolver {

    public static DataColumnType resolveType(int sqlType) {
        switch (sqlType) {
            case Types.CHAR:
            case Types.VARCHAR:
            case Types.LONGVARCHAR:
                return DataColumnType.STRING;
            case Types.TINYINT:
            case Types.SMALLINT:
                return DataColumnType.INT16;
            case Types.INTEGER:
                return DataColumnType.INT32;
            case Types.BIGINT:
                return DataColumnType.INT64;
            case Types.DECIMAL:
            case Types.NUMERIC:
            case Types.DOUBLE:
            case Types.FLOAT:
            case Types.REAL:
                return DataColumnType.FLOAT;
            case Types.BOOLEAN:
                return DataColumnType.BOOLEAN;
            case Types.DATE:
                return DataColumnType.DATE;
            case Types.TIME:
                return DataColumnType.DATETIME;
            case Types.TIMESTAMP:
                return DataColumnType.TIMESTAMP;
            case Types.ARRAY:
                return DataColumnType.DATASET;
            default:
                return DataColumnType.STRING;
        }
    }

    public static boolean isArray(int sqlType) {
        return sqlType == Types.ARRAY;
    }

    public static DataColumnType resolveArrayBaseType(Array array) throws SQLException {
        return resolveType(array.getBaseType());
    }

}
