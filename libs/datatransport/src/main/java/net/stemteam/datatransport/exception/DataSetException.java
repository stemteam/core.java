/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.exception;

/**
 * Исключение при работе с DataSet данными
 * 
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class DataSetException extends Exception {

    public DataSetException(String msg) {
        super(msg);
    }
    
}
