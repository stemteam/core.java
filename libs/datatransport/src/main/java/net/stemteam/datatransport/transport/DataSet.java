package net.stemteam.datatransport.transport;

import net.stemteam.datatransport.tool.FieldNameHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import net.stemteam.datatransport.exception.ColumnNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.tool.Base64;
import net.stemteam.datatransport.tool.IkGeometry;
import net.stemteam.datatransport.tool.Splitter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.text.ParseException;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

/**
 * Промежуточная структура хранения данных (для приема передачи через SOAP)
 *
 * @author Roman E. Kasovskiy, medved
 */
public class DataSet {
    // колонки (название, тип, и т.д.)

    private Vector<DataColumn> columns;

    /**
     * Возвращает список колонок датасета
     *
     * @return
     */
    public Vector<DataColumn> getColumns() {
        return columns;
    }

    // данные
    private Vector<DataRecord> records;

    /**
     * Возвращает список записей датасета
     *
     * @return
     */
    public Vector<DataRecord> getRecords() {
        return records;
    }

    /**
     * Представлять ли датасет для формата JSON2 как массив, даже если в нем всего одна запись
     */
    private boolean asJson2Array = true;

    /**
     * Представлять ли датасет для формата JSON2 как массив, даже если в нем всего одна запись
     *
     * @param value
     */
    public void setAsJson2Array(boolean value) {
        this.asJson2Array = value;
    }

    /**
     * Конструктор по умолчанию
     */
    public DataSet() {
        Clear();
    }

    /**
     * Очистка датасета
     */
    private void Clear() {
        records = new Vector<DataRecord>();
        columns = new Vector<DataColumn>();
    }

    /**
     * разделитель колонок для метода toString()
     */
    public static final String COLUMN_SEPARATOR = "\t";
    /**
     * разделитель записей для метода toString()
     */
    public static final String RECORD_SEPARATOR = "\n";

    /**
     * Разделитель для типов - массивов (Int32[] и т.д.)
     */
    public static final String ARRAY_SEPARATOR = ",";

    /**
     * Возвращает метаданные (для профайлера выделено в отдельную процедуру) (процедура для профайлера)
     */
    private static ResultSetMetaData getResultSetMetaData(ResultSet resultSet) throws SQLException {
        return resultSet.getMetaData();
    }

    /**
     * Возращает объект из резултсета по индексу
     */
    private Object getResultSetObject(DataColumn column, ResultSet resultSet,
            int index) throws SQLException, DataSetException {
        if (column.getType().equals(DataColumnType.DATASET)) {
            Array array = resultSet.getArray(index);
            if (array == null) {
                return null;
            }
            DataSet ds = new DataSet();
            ds.createColumn(column.getName(), JaxisTypesSQLResolver.resolveArrayBaseType(array));

            Object[] orray = (Object[]) array.getArray();
            for (Object o : orray) {
                ds.createRecord().setValue(0, o);
            }
            return ds;
        } else {
            return resultSet.getObject(index);
        }
    }

    /**
     * Cоздает записи в методе PackData
     */
    private void PackDataCreateRows(int columnCount, ResultSet resultSet)
            throws Exception {
        while (resultSet.next()) {
            // создали запись
            DataRecord record = new DataRecord();
            // заполнили ее значениями
            for (int i = 0; i < columnCount; i++) {
                DataColumn column = columns.get(i);
                Object object = getResultSetObject(column, resultSet, i + 1);
                DataCell cell = new DataCell(column, object);
                record.addCell(cell);
            }
            // добавили
            records.add(record);
        }
    }

    /**
     * Упаковывает ResultSet в DataSet. Может использовать FieldTranslator для преобразования имен/типов полей, если
     * передан.
     *
     * @param resultSet набор данных
     * @param fieldTranslator транслятор имен и типов полей
     * @throws SQLException
     * @throws net.stemteam.datatransport.exception.DataSetException
     */
    public void packDataDbmsIndependently(ResultSet resultSet,
            FieldTranslator fieldTranslator) throws SQLException,
            DataSetException {
        // очищаем датасет
        Clear();
        ResultSetMetaData meta = getResultSetMetaData(resultSet);
        // определем кол-во колонок
        try {
            // заполняем описание колонок датасета
            createColumnsDbmsIndependently(meta, fieldTranslator);
            // заполняем данные датасета
            PackDataCreateRows(meta.getColumnCount(), resultSet);
        } catch (Exception ex) {
            throw new DataSetException(ex.toString());
        }
    }

    /**
     * Упаковывает ResultSet в DataSet. Может использовать FieldTranslator для преобразования имен/типов полей, если
     * передан.
     *
     * @param resultSet набор данных
     * @param fieldTranslator транслятор имен и типов полей
     * @return
     * @throws SQLException
     * @throws DataSetException
     */
    public DataSet pack(ResultSet resultSet, FieldTranslator fieldTranslator) throws SQLException, DataSetException {
        packDataDbmsIndependently(resultSet, fieldTranslator);
        return this;
    }

    private void createColumnsDbmsIndependently(ResultSetMetaData meta,
            FieldTranslator fieldTranslator) throws Exception {
        int columnCount = meta.getColumnCount();
        // идем по колоночкам
        for (int i = 1; i <= columnCount; i++) {
            String originalColumnName = meta.getColumnLabel(i);
            int originalColumnType = meta.getColumnType(i);
            String columnName;
            DataColumnType columnType;
            // есзи задан Fieldtranslator, берем данные из него. Иначе - из метаданных БД
            if (fieldTranslator != null) {
                columnName = fieldTranslator.TranslateName(originalColumnName);
                columnType = fieldTranslator.TranslateType(columnName, null);
                if (columnType == null) {
                    columnType = JaxisTypesSQLResolver
                            .resolveType(originalColumnType);
                }
            } else {
                columnName = originalColumnName;
                columnType = JaxisTypesSQLResolver
                        .resolveType(originalColumnType);
            }
            DataColumn column = new DataColumn(columnName, columnType);
            columns.add(column);
        }
    }

    /**
     * Добавляет датасет (объединение)
     *
     * @param dataSet присоединяемый датасет
     * @throws DataSetEqualException несовместимость DataSet'ов
     */
    public void add(DataSet dataSet) throws DataSetEqualException {
        // копирование заголовков
        if (this.columns.isEmpty()) {
            this.columns = dataSet.getColumns()/*
                     * .clone()
                     */;

            // TODO генерит исключение, проверку полей нужно переписать на
            // сравнение типов навстат
        } else {
            if (this.columns.size() != dataSet.columns.size()) {
                throw new DataSetEqualException();
            }
        }
        for (DataRecord record : dataSet.getRecords()) {
            this.records.add(record);
        }
    }

    /**
     * Преобразовывает датасет к строковому представлению. Значения экранированы.
     *
     * @return
     */
    @Override
    public String toString() {
        if (getColumns() != null) {
            // в стрингбилдерах сила :)
            StringBuilder columnsString = new StringBuilder();
            StringBuilder typesString = new StringBuilder();
            StringBuilder dataString = new StringBuilder();
            // флаг цикла
            boolean is_first = true;
            for (DataColumn col : getColumns()) {
                if (is_first) {
                    columnsString.append(col.getName());
                    typesString.append(col.getDataTypeName());
                    is_first = false;
                } else {
                    columnsString.append(DataSet.COLUMN_SEPARATOR);
                    typesString.append(DataSet.COLUMN_SEPARATOR);
                    columnsString.append(col.getName());
                    typesString.append(col.getDataTypeName());
                }
            }
            dataString.append(columnsString.toString());
            dataString.append(RECORD_SEPARATOR);
            dataString.append(typesString.toString());
            dataString.append(RECORD_SEPARATOR);
            is_first = true;
            for (DataRecord record : getRecords()) {
                if (is_first) {
                    dataString.append(record.toString());
                    is_first = false;
                } else {
                    dataString.append(RECORD_SEPARATOR);
                    dataString.append(record.toString());
                }
            }
            return dataString.toString();
        } else {
            return null;
        }
    }

    /**
     * Для использования вложенных датасетов (датасет как значение в другом датасете)
     *
     * @return Возвращает строковое (защищенное) представление датасета
     * @throws UnsupportedEncodingException - неподдерживаемая кодировка UTF-8
     */
    public String toSafeString() throws UnsupportedEncodingException {
        // в строку
        String s = this.toString();
        // экранируем (применяем Base64)
        return Base64.encode(s.getBytes("UTF-8"));
    }

    /**
     * Для использования вложенных датасетов (датасет как значение в другом датасете)
     *
     * @param safeDataSet (DataSet.toSafeString) строкое представление датасета (защищенное)
     * @return Дата сет, свормированные из строкового (защищенного) представления
     * @throws IOException ошибка перекодирования из Base64
     * @throws Exception Ошибка парсинга строки датасета
     */
    public static DataSet fromSafeString(String safeDataSet)
            throws IOException, Exception {
        byte[] decoded = Base64.decode(safeDataSet);
        String dataString = new String(decoded, "UTF-8");
        DataSet ds = new DataSet();
        ds.UnPackData(dataString);
        return ds;
    }

    /**
     * Преобразовывает датасет к строковому представлению - дамп
     *
     * @return
     */
    public String toDump() {
        StringBuilder dataString = new StringBuilder();
        dataString.append("dataset{");
        if (getColumns() != null) {
            // в стрингбилдерах сила :)
            StringBuilder columnsString = new StringBuilder();
            // флаг цикла
            boolean is_first = true;
            for (DataColumn col : getColumns()) {
                if (is_first) {
                    columnsString.append(col.getName());
                    columnsString.append(" (");
                    columnsString.append(col.getDataTypeName());
                    columnsString.append(")");
                    is_first = false;
                } else {
                    columnsString.append(DataSet.COLUMN_SEPARATOR);
                    columnsString.append(col.getName());
                    columnsString.append(" (");
                    columnsString.append(col.getDataTypeName());
                    columnsString.append(")");
                }
            }
            dataString.append(columnsString.toString());
            dataString.append(RECORD_SEPARATOR);
            is_first = true;
            for (DataRecord record : getRecords()) {
                if (is_first) {
                    dataString.append(record.toDump());
                    is_first = false;
                } else {
                    dataString.append(RECORD_SEPARATOR);
                    dataString.append(record.toDump());
                }
            }
        }
        dataString.append("}");
        return dataString.toString();
    }

    /**
     * Распаковывает данные в датасет (источник - строка)
     *
     * @param data строка данных
     */
    public void UnPackData(String data) throws Exception, DataSetException {
        Clear();
        // разливаем данные на записи
        String[] dataList = data.split(RECORD_SEPARATOR);
        // названия колонок
        String columnString = null;
        try {
            columnString = dataList[0];
        } catch (java.lang.ArrayIndexOutOfBoundsException ex) {
            throw new DataSetException(
                    "DataSet.UnPackData - ошибка получения названий колонок из датасета: датасет не содержит описания колонок. Data dump: "
                    + data);
        }
        Vector<String> columnsList = Splitter.Split(columnString,
                COLUMN_SEPARATOR);
        // типы данных
        String typeString = dataList[1];
        Vector<String> typeList = Splitter.Split(typeString, COLUMN_SEPARATOR);
        // формируем колонки
        int columnCount = columnsList.size();
        for (int i = 0; i < columnCount; i++) {
            DataColumn col = new DataColumn(columnsList.get(i), DataColumnType.getByName(typeList.get(i)));
            getColumns().add(col);
        }
        // данные
        int recordCount = dataList.length - 2;
        for (int i = 0; i < recordCount; i++) {
            // распиливаем запись
            Vector<String> rowString = Splitter.Split(dataList[i + 2],
                    DataSet.COLUMN_SEPARATOR);
            // создаем объект
            DataRecord record = new DataRecord();
            // наполняем
            Vector<DataColumn> clmns = getColumns();
            for (int j = 0; j < columnCount; j++) {
                DataColumn dc = clmns.get(j);
                String s = rowString.get(j);
                DataCell cell = null;
                // заменяем пустые строки нулами
                String ss = null;
                if (s.equals("")) {
                    cell = new DataCell(ss, dc);
                } else {
                    cell = new DataCell(s, dc);
                }
                record.getCells().add(cell);
            }
            getRecords().add(record);
        }
    }

    /**
     * Удаление строки по заданному индексу
     *
     * @param index индекс для удаления
     */
    public void removeRecord(int index) {
        this.records.remove(index);
    }

    public void removeColumns(int index) {
        // удаляем столбец в заголовке
        this.columns.remove(index);
        // удаляем стоблец в записях
        for (DataRecord dataRecord : this.records) {
            dataRecord.removeCell(index);
        }
    }

    /**
     * Удаляем колонку из набора по имени
     *
     * @param columnName
     */
    public void removeColumns(String columnName) {
        // ищем индекс
        int index = -1;
        for (int i = 0; i < columns.size(); i++) {
            if (columns.get(i).getName().equalsIgnoreCase(columnName)) {
                removeColumns(i);
                return;
            }
        }
    }

    /**
     * Создает новую запись <code>DataRecord</code> в <code>DataSet</code>. Инициализирует ячейки значений записи
     * NULL-значениями
     *
     * @return DataRecord добавленную в DataSet
     * @throws net.stemteam.datatransport.exception.DataSetException
     */
    public DataRecord createRecord() throws DataSetException {
        return createRecord(this.records.size());
    }

    /**
     * Создает новую запись <code>DataRecord</code> в <code>DataSet</code>. Инициализирует ячейки значений записи
     * NULL-значениями
     *
     * @param index индекс позиции для вставки
     * @return
     * @throws DataSetException
     */
    public DataRecord createRecord(int index) throws DataSetException {
        // создаем запись
        DataRecord row = new DataRecord();
        // создаем ячейки в ней с NULL значениями
        for (DataColumn column : this.columns) {
            DataCell cell = new DataCell(column, column.getDefaultValue());
            row.addCell(cell);
        }
        // добавляем запись в датасет
        this.records.add(index, row);
        // возвращаем запись
        return row;
    }

    /**
     * Создает новую колонку. Если в наборе уже есть записи - добавляет в каждую запись ячейку с null содержимым
     *
     * @param name название колонки
     * @param dataTypeName тип данных
     * @return созданная колонка
     */
    public DataColumn createColumn(String name, DataColumnType dataTypeName)
            throws DataSetException {
        return createColumn(name, dataTypeName, null);
    }

    /**
     * Создает новую колонку. Если в наборе уже есть записи - добавляет в каждую запись ячейку со значением, указанным
     * как значение по умолчанию содержимым
     *
     * @param name название колонки
     * @param dataTypeName тип данных
     * @param defaultValue
     * @return созданная колонка
     * @throws net.stemteam.datatransport.exception.DataSetException
     */
    public DataColumn createColumn(String name, DataColumnType dataTypeName,
            Object defaultValue) throws DataSetException {
        return createColumn(name, dataTypeName, defaultValue, this.columns.size());
    }

    /**
     * Создает новую колонку. Если в наборе уже есть записи - добавляет в каждую запись ячейку со значением, указанным
     * как значение по умолчанию содержимым
     *
     * @param name название колонки
     * @param dataTypeName тип данных
     * @param defaultValue значение по умолчанию при создании строк
     * @param index порядок колонки в наборе
     * @return
     * @throws DataSetException
     */
    public DataColumn createColumn(String name, DataColumnType dataTypeName,
            Object defaultValue, int index)
            throws DataSetException {
        DataColumn column = new DataColumn(name, dataTypeName, defaultValue);
        this.columns.add(index, column);
        // если колонка добавляется в существующий датасет - добавляем ячейки в
        // записи
        for (DataRecord row : this.records) {
            DataCell cell = new DataCell(column, defaultValue);
            row.addCell(index, cell);
        }
        return column;
    }

    /**
     * Возвращает все значения указанной колонки (по названию)
     *
     * @param columnIndex индекс колонки в наборе
     * @return
     * @throws ColumnNotFoundException колонка не найдена (message = имя)
     */
    public Object[] getColumnValues(int columnIndex)
            throws ColumnNotFoundException {
        // todo оптимизировать по скорости
        Object[] values = new Object[records.size()];
        int i = 0;
        for (DataRecord row : this.records) {
            Object value = row.getCell(columnIndex).getValue();
            values[i] = value;
            i++;
        }
        return values;
    }

    /**
     * Возвращает все значения указанной колонки (по названию)
     *
     * @param columnName название колонки
     * @return
     * @throws ColumnNotFoundException колонка не найдена (message = имя)
     */
    public Object[] getColumnValues(String columnName)
            throws ColumnNotFoundException {
        int columnIndex = getColumnIndex(columnName);
        return getColumnValues(columnIndex);
    }

    /**
     * Возвращает все значения указанной колонки (по индексу)
     *
     * @param columnIndex
     * @return
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public String[] getColumnStringValues(int columnIndex)
            throws ColumnNotFoundException {
        // todo оптимизировать по скорости
        String[] values = new String[records.size()];
        int i = 0;
        for (DataRecord row : this.records) {
            Object value = row.getCell(columnIndex).getValue();
            values[i] = (String) value;
            i++;
        }
        return values;
    }

    /**
     * Возвращает все значения указанной колонки (по имени поля)
     *
     * @param columnName название колонки
     * @return
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public String[] getColumnStringValues(String columnName)
            throws ColumnNotFoundException {
        int columnIndex = getColumnIndex(columnName);
        return getColumnStringValues(columnIndex);
    }

    /**
     * Определяет индекс колонки по названию
     *
     * @param columnName
     * @return
     * @throws ColumnNotFoundException
     */
    public int getColumnIndex(String columnName) throws ColumnNotFoundException {
        int index = 0;
        for (DataColumn col : this.columns) {
            if (col.getName().compareToIgnoreCase(columnName) == 0) {
                return index;
            }
            index++;
        }
        throw new ColumnNotFoundException(columnName);
    }

    /**
     * Возвращает массив имен колонок
     *
     * @return
     */
    public Vector<String> getColumnNames() {
        Vector<String> names = new Vector();
        for (DataColumn col : this.columns) {
            names.add(col.getName());
        }
        return names;
    }

    /**
     * Возвращает массив типов колонок
     *
     * @return
     */
    public Vector<String> getColumnTypes() {
        Vector<String> types = new Vector();
        for (DataColumn col : this.columns) {
            types.add(col.getDataTypeName());
        }
        return types;
    }

    /**
     * Возвращает транспорт в формате JSON. Формат зависит от версии.
     *
     * @return
     */
    public String toJson() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting(); // печатать красиво
        builder.serializeNulls(); // не исключать нул-значения
        Gson gson = builder.create();
        JsonContainer cont = getJsonContainer(this);
        return gson.toJson(cont);
    }

    /**
     * Преобразовывает датасет к JSON формата: [{"A":"превед","B":28,"C":100500.25
     * },{"A":"медвед\u003d2","B":29,"C":500500.25}]
     *
     * @return
     * @throws net.stemteam.datatransport.exception.ColumnNotFoundException
     */
    public String toJson2() throws DataSetException {
        GsonBuilder builder = new GsonBuilder();
        builder.serializeNulls(); // не исключать нул-значения
        Gson gson = builder.create();
        StringBuilder json2 = new StringBuilder();
        if ((this.getRecords().size() > 1) || (this.asJson2Array)) {
            json2.append("[");
        }
        boolean first = true;
        for (DataRecord r : this.getRecords()) {
            if (first) {
                first = false;
            } else {
                json2.append(",");
            }
            json2.append("{");
            boolean first2 = true;
            for (DataCell cell : r.getCells()) {
                if (first2) {
                    first2 = false;
                } else {
                    json2.append(",");
                }
                json2.append(gson.toJson(cell.getColumn().getName()));
                json2.append(":");
                if (cell.getColumn().getType().equals(DataColumnType.DATASET) || cell.getColumn().getType().equals(
                        DataColumnType.GEOMETRY)) {
                    json2.append(cell.getJson2Value());
                } else {
                    json2.append(gson.toJson(cell.getJson2Value()));
                }
            }
            json2.append("}");
        }
        // у нас может быть ситуация когда нет записей, и должен возвращаться не
        // массив. в этой ситуации мы должны вернуть пустой объект, т.е. {}
        if ((first) && (!this.asJson2Array)) {
            json2.append("{}");
        }
        if ((this.getRecords().size() > 1) || (this.asJson2Array)) {
            json2.append("]");
        }
        return json2.toString();
    }

    /**
     * Формирует контейнер для преобразования в JSON из датасета (реекурсивная)
     *
     * @param ds
     * @return
     */
    private static JsonContainer getJsonContainer(DataSet ds) {
        if (ds == null) {
            return null;
        }
        JsonContainer cont = new JsonContainer();
        cont.setCols(ds.getColumnNames());
        cont.setTypes(ds.getColumnTypes());
        for (DataRecord row : ds.getRecords()) {
            Vector r = new Vector();
            for (DataCell cell : row.getCells()) {
                // если это датасет - рекурсия
                if (!cell.getColumn().getType().equals(DataColumnType.DATASET)) {
                    r.add(cell.getJsonValue());
                } else {
                    r.add(getJsonContainer((DataSet) cell.getValue()));
                }
            }
            cont.getRows().add(r);
        }
        return cont;
    }

    /**
     * Преобразование JSON => DataSet
     *
     * @param json
     * @return
     * @throws DataSetException
     */
    public static DataSet fromJson(String json) throws DataSetException {
        if ((json == null) || ("".equals(json))) {
            return null;
        }
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting(); // печатать красиво
        builder.serializeNulls(); // не исключать нул-значения
        Gson gson = builder.create();

        JsonContainer cont = gson.fromJson(json, JsonContainer.class);

        DataSet result = new DataSet();
        int colCount = cont.getCols().size();
        for (int i = 0; i < colCount; i++) {
            result.createColumn(cont.getCols().get(i), DataColumnType.getByName(cont.getTypes().get(i)));
        }

        // данные
        JsonParser parser = new JsonParser();
        JsonElement elemRows = parser.parse(json).getAsJsonObject().get("rows");
        JsonArray rows = elemRows.getAsJsonArray();
        Iterator<JsonElement> rowIterator = rows.iterator();
        while (rowIterator.hasNext()) {
            JsonArray rowData = rowIterator.next().getAsJsonArray();
            DataRecord row = result.createRecord();
            // выковыриваем элементы
            for (int i = 0; i < colCount; i++) {
                // определяем тип колонки
                DataColumnType colType = DataColumnType.getByName(cont.getTypes().get(i));
                if (colType.equals(DataColumnType.STRING)) {
                    String s = gson.fromJson(rowData.get(i), String.class);
                    row.setValue(i, s);
                } else if (colType.equals(DataColumnType.DATETIME)) {
                    String s = gson.fromJson(rowData.get(i), String.class);
                    try {
                        row.setValue(i, StringEncoder.decodeDateTime(s));
                    } catch (ParseException ex) {
                        throw new DataSetException(ex.toString());
                    }
                } else if (colType.equals(DataColumnType.TIMESTAMP)) {
                    String s = gson.fromJson(rowData.get(i), String.class);
                    try {
                        row.setValue(i, StringEncoder.decodeTimeStamp(s));
                    } catch (ParseException ex) {
                        throw new DataSetException(ex.toString());
                    }
                } else if (colType.equals(DataColumnType.DATE)) {
                    String s = gson.fromJson(rowData.get(i), String.class);
                    try {
                        row.setValue(i, StringEncoder.decodeDate(s));
                    } catch (ParseException ex) {
                        throw new DataSetException(ex.toString());
                    }
                } else if (colType.equals(DataColumnType.INT16)) {
                    Integer ii = gson.fromJson(rowData.get(i), Integer.class);
                    row.setValue(i, ii);
                } else if (colType.equals(DataColumnType.INT32)) {
                    Integer ii = gson.fromJson(rowData.get(i), Integer.class);
                    row.setValue(i, ii);
                } else if (colType.equals(DataColumnType.INT64)) {
                    Integer ii = gson.fromJson(rowData.get(i), Integer.class);
                    row.setValue(i, ii);
                } else if (colType.equals(DataColumnType.FLOAT)) {
                    Double d = gson.fromJson(rowData.get(i), Double.class);
                    row.setValue(i, d);
                } else if (colType.equals(DataColumnType.GEOMETRY)) {
                    String s = gson.fromJson(rowData.get(i), String.class);
                    row.setValue(i, IkGeometry.geometryFromString(s));
                } else if (colType.equals(DataColumnType.BOOLEAN)) {
                    Integer ii = gson.fromJson(rowData.get(i), Integer.class);
                    if (ii != null) {
                        row.setValue(i, ii == 1);
                    }
                } else if (colType.equals(DataColumnType.DATASET)) {
                    // вложенные датасеты из JSON
                    if ((rowData.get(i) != null) && (!rowData.get(i).isJsonNull())) {
                        row.setDataSet(i, DataSet.fromJson(rowData.get(i).toString()));
                    }
                } else {
                    throw new DataSetException("Неизвестный тип данных: "
                            + colType);
                }
            }
        }
        return result;
    }

    public static DataSet fromJson2(String json) throws DataSetException {
        if (json == null) {
            return null;
        }
        JsonParser parser = new JsonParser();
        JsonElement elemRows = parser.parse(json);

        // без элементов
        if (elemRows.isJsonNull()) {
            return null;
        }

        DataSet ds = new DataSet();

        // может быть массивом (когда несколько строк в транспорте)
        if (elemRows.isJsonArray()) {
            JsonArray reco = elemRows.getAsJsonArray();
            // побежали по записям
            boolean first = true;
            for (JsonElement e : reco) {
                // преобразовываем к JSON-объекту (аналог
                // Map<String,JsonElement>)
                JsonObject obj = e.getAsJsonObject();

                if (first) {
                    // создаем поля датасета
                    ds = json2CreateColumns(obj, ds);
                    first = false;
                }

                // формируем строки
                ds = json2CreateRecord(obj, ds);
            }

        } else if (elemRows.isJsonObject()) { // может быть объектом (когда одна
            // строка)

            // преобразовываем к JSON-объекту (аналог Map<String,JsonElement>)
            JsonObject obj = elemRows.getAsJsonObject();

            // создаем поля датасета
            ds = json2CreateColumns(obj, ds);

            // формируем строки
            ds = json2CreateRecord(obj, ds);

        } else {
            throw new DataSetException("Некорретный формат JSON2 DataSet");
        }
        return ds;

    }

    /**
     * Формирует поля датасета парся первую запись JSON2 представления
     *
     * @param obj
     * @param ds
     * @return
     * @throws DataSetException
     */
    private static DataSet json2CreateColumns(JsonObject obj, DataSet ds)
            throws DataSetException {
        // создаем поля датасета
        for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
            String o = entry.getKey();
            JsonElement oo = entry.getValue();

            // у нас могут быть либо примитивы, либо датасеты
            if (oo.isJsonPrimitive()) {
                JsonPrimitive prim = oo.getAsJsonPrimitive();
                if (prim.isJsonNull()) {
                    // хрен знает какой тип если нетипизированное значение =
                    // null
                    ds.createColumn(o, DataColumnType.STRING);
                } else if (prim.isString()) {
                    ds.createColumn(o, DataColumnType.STRING);
                } else if (prim.isNumber()) {
                    ds.createColumn(o, DataColumnType.FLOAT);
                } else if (prim.isBoolean()) {
                    ds.createColumn(o, DataColumnType.BOOLEAN);
                } else {
                    ds.createColumn(o, DataColumnType.STRING);
                }
            } else if (oo.isJsonArray()) {
                ds.createColumn(o, DataColumnType.DATASET);
            } else { // строка по умолчанию для нулов и прочего
                ds.createColumn(o, DataColumnType.STRING);
            }
        }
        return ds;
    }

    /**
     * Формирует запись из JSON-объекта
     *
     * @param obj
     * @param ds
     * @return
     * @throws DataSetException
     */
    private static DataSet json2CreateRecord(JsonObject obj, DataSet ds)
            throws DataSetException {
        DataRecord row = ds.createRecord();
        for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
            String o = entry.getKey();
            JsonElement oo = entry.getValue();

            // у нас могут быть либо примитивы, либо датасеты
            if (oo.isJsonPrimitive()) {
                JsonPrimitive prim = oo.getAsJsonPrimitive();
                if (prim.isJsonNull()) {
                    // продолжаем (null и так есть в ячейке)
                } else if (prim.isString()) {
                    row.setString(o, prim.getAsString());
                } else if (prim.isNumber()) {
                    row.setDouble(o, prim.getAsDouble());
                } else if (prim.isBoolean()) {
                    row.setBoolean(o, prim.getAsBoolean());
                } else {
                    row.setString(o, prim.getAsString());
                }
            } else {
                // рекурсия
                DataSet dss = DataSet.fromJson2(oo.toString());
                row.setDataSet(o, dss);
            }
        }
        return ds;
    }

    /**
     * Преобразовывает значения колонки в массив значений с разделителем запятая
     *
     * @param columnIndex
     * @return
     * @throws ColumnNotFoundException
     */
    public String getColumnAsJson2Array(int columnIndex)
            throws DataSetException {
        GsonBuilder builder = new GsonBuilder();
        builder.serializeNulls(); // не исключать нул-значения
        Gson gson = builder.create();

        StringBuilder sb = new StringBuilder();
        boolean is_first = true;
        sb.append("[");
        for (DataRecord row : this.records) {
            if (is_first) {
                Object o = row.getCell(columnIndex).getJson2Value();
                sb.append(gson.toJson(o));
                is_first = false;
            } else {
                sb.append(DataSet.ARRAY_SEPARATOR);
                Object o = row.getCell(columnIndex).getJson2Value();
                sb.append(gson.toJson(o));
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Преобразовывает значения колонки в массив значений с разделителем запятая
     *
     * @param columnName
     * @return
     * @throws ColumnNotFoundException
     */
    public String getColumnAsJson2Array(String columnName)
            throws DataSetException {
        int columnIndex = getColumnIndex(columnName);
        return getColumnAsJson2Array(columnIndex);
    }

    /**
     * Преобразовывает имена колонок датасета к CamelCase виду
     */
    public void doCamelColumnNames() {
        for (DataColumn col : getColumns()) {
            col.setName(FieldNameHelper.toCamelCase(col.getName(), false));
        }
    }

    /**
     * Преобразовывает имена колонок датасета к CamelCase виду
     *
     * @param invertPk осуществлять ли преобразование IdTable => TableId
     */
    public void doCamelColumnNames(boolean invertPk) {
        for (DataColumn col : getColumns()) {
            col.setName(FieldNameHelper.toCamelCase(col.getName(), invertPk));
        }
    }
}
