/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.stemteam.datatransport.exception;

/**
 * Ошибка получения пула конектов к БД по JNDI имени в контексте вэбсервера
 * 
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class ConnectionPoolNotFoundException extends Exception {

    public ConnectionPoolNotFoundException(String msg) {
        super(msg);
    }

}
