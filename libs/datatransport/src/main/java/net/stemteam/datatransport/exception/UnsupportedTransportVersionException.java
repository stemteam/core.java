/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.exception;

/**
 * Исключение: неподдерживаемая версия траспорта
 * 
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class UnsupportedTransportVersionException extends Exception {
    
    public UnsupportedTransportVersionException(String version) {
        super("Версия транспорта " + version + " не поддерживается.");
    }
}
