/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.stemteam.datatransport.tool;

/**
 * Транслитератор
 * @author medved
 */
public class Transliteration {

    // GOST 16876-71
    private static final String RUSSIAN_CHARSET = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
    private static final String[] RUSSIAN_TRUNSLATE = {"a", "b", "v", "g", "d", "e", "jo", "zh", "z", "i", "jj", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "kh", "c", "ch", "sh", "shh", "\"", "y", "'", "eh", "ju", "ja",
                                                       "A", "B", "V", "G", "D", "E", "JO", "ZH", "Z", "I", "JJ", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "KH", "C", "CH", "SH", "SHH", "\"", "Y", "'", "EH", "JU", "JA"};
    /**
     * Переводит строки с символами русских букв в транслит (GOST 16876-71)
     * @param text русский текст
     * @return транслит
     */
    public static String transliterateRussian(String text) {
        if (text == null)
            return null;
        StringBuilder stringBuilder = new StringBuilder("");
        char[] chs = text.toCharArray();
        for (int i = 0; i < chs.length; i++) {
            int k = RUSSIAN_CHARSET.indexOf(chs[i]);
            if(k != -1)
                stringBuilder.append(RUSSIAN_TRUNSLATE[k]);
            else
                stringBuilder.append(chs[i]);
        }
        return stringBuilder.toString();
    }

}
