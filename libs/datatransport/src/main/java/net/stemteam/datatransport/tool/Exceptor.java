/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.stemteam.datatransport.tool;

import net.stemteam.datatransport.transport.DataTransport;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Нечто, связанное с исключениями :)
 * @author medved
 */
public class Exceptor {

    
    /**
     * Разделитель частей сообщения об ошибке при логировании и возврате
     */
    private static final String ERROR_MESSAGE_SEPARATOR = " ::: ";
       
    /**
     * Формирует строку ошибки, пишет в лог и возвращает сформированную строку
     * @param errorId уникальный идентификатор ошибки
     * @param message сообщение
     * @param ex исключение или null
     * @return сформированнная строка ошибки
     */
    public static String logError(String errorId, String message, Exception ex) {
        // формируем строку сообщения об ошибке
        StringBuilder sb = new StringBuilder();
        sb.append(errorId);
        sb.append(ERROR_MESSAGE_SEPARATOR);
        sb.append(message);
        sb.append(ERROR_MESSAGE_SEPARATOR);
        if (ex != null) {
            sb.append(ex.toString());
            sb.append(Exceptor.getErrorString(ex));
        }
        String err = sb.toString();
        // экранируем табы
        err = err.replace("\t", " ");
        // пишем сообщение в лог       
        Logger.getAnonymousLogger().log(Level.SEVERE, err, ex);
        return err;
    }
    
    /**
     * Печатает в лог сообщение об ошибке и возвращает объект транспорта с 
     * ошибкой в виде строки
     * @param errorId тип ошибки (строка)
     * @param message сообщение об ошибке
     * @param ex исключение или null
     * @return Транспорт в виде строки с текстом ошибки
     */
    public static String logAndReturnError(String errorId, String message, Exception ex) {
        // пишем сообщение в лог
        String err = logError(errorId, message, ex);
        // возвращает транспорт с ошибкой
        return DataTransport.getTransportWithError(err).toString();
    }
       
    /**
     * Возвращает строковое представление исключения
     *
     * @param ex Исключение
     * @return Строка
     */
    public static synchronized String getErrorString(Exception ex){
        StringBuilder sb = new StringBuilder();
        sb.append(ex.toString());
        sb.append("\n");
        // парсим исключение Oracle DB
        if (ex instanceof SQLException)
        {
            sb.append(parseOracleException(ex.toString()));
        }
        for (StackTraceElement el : ex.getStackTrace()) {
            sb.append(el.toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    // префикс ораклового исключения
    private static String ORA_EXCEPTION_TOKEN = "ORA-";
    // разделитель между префиксом и текстом исключения
    private static String ORA_EXCEPTION_TOKEN_SEP = ":";

    /**
     * Обработка SQL ошибки
     * Происходит разбор и генерация соответствующей ошибки ресивера по токену ORA-XXXXX
     */
    public static synchronized String parseOracleException(String oracleErrorMessage)
    {
        try {
            // ищем токен оракла в тексте ошибки
            int i = oracleErrorMessage.indexOf(ORA_EXCEPTION_TOKEN);
            if (i != -1)
            {
                // срезаем строку
                String src = oracleErrorMessage.substring(i);
                i = src.indexOf(ORA_EXCEPTION_TOKEN_SEP);
                // если не удалось выковырнуть токен - возвращаем стандартную ошибку
                if (i == -1)
                    return oracleErrorMessage;
                // выковыриваем оракловый токен
                String oraToken = src.substring(0, i);
                // отрезаем шапку (до второго токена, еси таковой есть иначе до конца)
                src = src.substring(i + 1);
                i = src.indexOf(ORA_EXCEPTION_TOKEN);
                if (i != -1)
                {
                    src = src.substring(0, i-1);
                }
                return oraToken + ORA_EXCEPTION_TOKEN_SEP + src;
            }
            else 
                return oracleErrorMessage;
        } catch (Exception ex) {
            // логируем на сервере
            Logger.getLogger(Exceptor.class.getName()).log(Level.SEVERE, null, ex);
            // возвращаем входное сообщение
            return oracleErrorMessage;
        }
    }


    
}
