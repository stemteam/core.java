package net.stemteam.datatransport.transport;

import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.exception.UnsupportedColumnTypeException;
import net.stemteam.datatransport.tool.IkGeometry;
import oracle.spatial.geometry.OraGeometry;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.spatial.geometry.JGeometry;

/**
 * Ячейка данных
 *
 * @author medved
 */
public class DataCell {

    // значение ячейки
    private Object value;
    // колонка - друг ячейки
    private DataColumn column;

    /**
     * Возвращает колонку данных, соответствующую ячейке
     *
     * @return
     */
    public DataColumn getColumn() {
        return column;
    }

    /**
     * Возвращает значение ячейки
     *
     * @return
     */
    public Object getValue() {
        return value;
    }

    /**
     * Устанавливает значение ячейки
     *
     * @param value
     */
    public void setValue(Object value) {
        // todo можно сделать проверку  и запрет (или преобразование) на 
        // установку значений, не соовтетвующих описанному в колонке типу, 
        // но может замедлить выполнение (поэтому пока не буду затеваться)
        this.value = value;
    }

    /**
     * Возвращает строковое представление ячейки
     *
     * @return
     */
    @Override
    public String toString() {
        // если нет значения - возвращаем пустую строку (она короче чем null)
        if (value == null) {
            return "";
        }
        // определяем строковое представление в зависимости от типа
        if (this.column.getType().equals(DataColumnType.STRING)) {
            try {
                return StringEncoder.encodeString(value.toString());
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(DataCell.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        } else if (this.column.getType().equals(DataColumnType.DATETIME)) {
            try {
                return StringEncoder.encodeDateTime(value);
            } catch (SQLException ex) {
                Logger.getLogger(DataCell.class.getName()).log(Level.SEVERE, null, ex);
                return ex.toString();
            }
        } else if (this.column.getType().equals(DataColumnType.TIMESTAMP)) {
            try {
                return StringEncoder.encodeTimeStamp(value);
            } catch (SQLException ex) {
                Logger.getLogger(DataCell.class.getName()).log(Level.SEVERE, null, ex);
                return ex.toString();
            }
        } else if (this.column.getType().equals(DataColumnType.DATE)) {
            try {
                return StringEncoder.encodeDate(value);
            } catch (SQLException ex) {
                Logger.getLogger(DataCell.class.getName()).log(Level.SEVERE, null, ex);
                return ex.toString();
            }

        } else if (this.column.getType().equals(DataColumnType.INT16)) {
            return value.toString();
        } else if (this.column.getType().equals(DataColumnType.INT32)) {
            return value.toString();
        } else if (this.column.getType().equals(DataColumnType.INT64)) {
            return value.toString();
        } else if (this.column.getType().equals(DataColumnType.FLOAT)) {
            return value.toString();

        } else if (this.column.getType().equals(DataColumnType.BOOLEAN)) {
            return ((Boolean) value) ? "1" : "0";

        } else if (this.column.getType().equals(DataColumnType.GEOMETRY)) {
            try {
                return ((IkGeometry) value).toString();
            } catch (Exception ex) {
                Logger.getLogger(DataCell.class.getName()).log(Level.SEVERE, null, ex);
                return ex.toString();
            }
        } else if (this.column.getType().equals(DataColumnType.DATASET)) {
            try {
                return ((DataSet) value).toSafeString();
            } catch (UnsupportedEncodingException ex) {
                return "Некорректная кодировка UTF-8 вложенного датасета в колонке " + this.column.getName();
            }
        } else {
            return String.valueOf(value);
        }
    }

    /**
     * Возвращает реальное значение. Даты кодируются в строку, булеан в инт, геометрия - в строку, датасет - в JSON
     *
     * @return
     */
    public Object getJsonValue() {
        // если нет значения - возвращаем пустую строку (она короче чем null)
        if (value == null) {
            return null;
        }
        // определяем строковое представление в зависимости от типа
        if (column.getType().equals(DataColumnType.STRING)) {
            return value;
        } else if (column.getType().equals(DataColumnType.DATETIME)) {
            try {
                return StringEncoder.encodeDateTime(value);
            } catch (SQLException ex) {
                return ex.toString();
            }
        } else if (column.getType().equals(DataColumnType.TIMESTAMP)) {
            try {
                return StringEncoder.encodeTimeStamp(value);
            } catch (SQLException ex) {
                return ex.toString();
            }
        } else if (column.getType().equals(DataColumnType.DATE)) {
            try {
                return StringEncoder.encodeDate(value);
            } catch (SQLException ex) {
                return ex.toString();
            }

        } else if (column.getType().equals(DataColumnType.INT16)) {
            return value;
        } else if (column.getType().equals(DataColumnType.INT32)) {
            return value;
        } else if (column.getType().equals(DataColumnType.INT64)) {
            return value;
        } else if (column.getType().equals(DataColumnType.FLOAT)) {
            return value;
        } else if (column.getType().equals(DataColumnType.BOOLEAN)) {
            return ((Boolean) value) ? 1 : 0;
        } else if (column.getType().equals(DataColumnType.GEOMETRY)) {
            try {
                return ((IkGeometry) value).toJson();
            } catch (Exception ex) {
                return ex.toString();
            }
        } else if (column.getType().equals(DataColumnType.DATASET)) {
            return ((DataSet) value).toJson();
        } else {
            return value;
        }
    }

    /**
     * Возвращает реальное значение. Даты кодируются в UnixTimestamp, булеан в инт, геометрия - в строку, датасет - в
     * JSON2
     *
     * @return
     */
    public Object getJson2Value() throws DataSetException {
        // если нет значения - возвращаем пустую строку (она короче чем null)
        if (value == null) {
            return null;
        }
        if (column.getType().equals(DataColumnType.STRING)) {
            return value;
        } else if (column.getType().equals(DataColumnType.DATETIME)) {
            return ((java.sql.Timestamp) value).getTime();
        } else if (column.getType().equals(DataColumnType.TIMESTAMP)) {
            return ((java.sql.Timestamp) value).getTime();
        } else if (column.getType().equals(DataColumnType.DATE)) {
            if (value instanceof java.sql.Timestamp) {
                return ((java.sql.Timestamp) value).getTime();
            } else {
                return ((java.sql.Date) value).getTime();
            }
        } else if (column.getType().equals(DataColumnType.INT16)) {
            return value;
        } else if (column.getType().equals(DataColumnType.INT32)) {
            return value;
        } else if (column.getType().equals(DataColumnType.INT64)) {
            return value;
        } else if (column.getType().equals(DataColumnType.FLOAT)) {
            return value;
        } else if (column.getType().equals(DataColumnType.BOOLEAN)) {
            return ((Boolean) value) ? 1 : 0;
        } else if (column.getType().equals(DataColumnType.GEOMETRY)) {
            try {
                return ((IkGeometry) value).toJson();
            } catch (Exception ex) {
                return ex.toString();
            }
        } else if (column.getType().equals(DataColumnType.DATASET)) {
            return ((DataSet) value).toJson2();
        } else {
            return value;
        }
    }

    /**
     * Возвращает строковое представление ячейки (для распечатки только)
     *
     * @return
     */
    public String toDump() {
        // если нет значения - возвращаем пустую строку (она короче чем null)
        if (value == null) {
            return "";
        }

        // определяем строковое представление в зависимости от типа
        if (column.getType().equals(DataColumnType.STRING)) {
            return "\"" + value.toString().replace("\"", "\\\"") + "\"";
        } else if (column.getType().equals(DataColumnType.DATETIME)) {
            try {
                return "\"" + StringEncoder.encodeDateTime(value) + "\"";
            } catch (SQLException ex) {
                return ex.toString();
            }
        } else if (column.getType().equals(DataColumnType.TIMESTAMP)) {
            try {
                return "\"" + StringEncoder.encodeTimeStamp(value) + "\"";
            } catch (SQLException ex) {
                return ex.toString();
            }
        } else if (column.getType().equals(DataColumnType.DATE)) {
            try {
                return "\"" + StringEncoder.encodeDate(value) + "\"";
            } catch (SQLException ex) {
                return ex.toString();
            }

        } else if (column.getType().equals(DataColumnType.INT16)) {
            return value.toString();
        } else if (column.getType().equals(DataColumnType.INT32)) {
            return value.toString();
        } else if (column.getType().equals(DataColumnType.INT64)) {
            return value.toString();
        } else if (column.getType().equals(DataColumnType.FLOAT)) {
            return value.toString();

        } else if (column.getType().equals(DataColumnType.BOOLEAN)) {
            return ((Boolean) value) ? "True" : "False";
        } else if (column.getType().equals(DataColumnType.DATASET)) {
            return ((DataSet) value).toDump();
        } else if (column.getType().equals(DataColumnType.GEOMETRY)) {
            try {
                return ((IkGeometry) value).toString();
            } catch (Exception ex) {
                return ex.toString();
            }
        } else if (column.getType().equals(DataColumnType.DATASET)) {
            return ((DataSet) value).toDump();
        } else {
            return String.valueOf(value);
        }
    }

    /**
     * Конструктор. Используется из PackDataSet
     *
     * @param column
     * @param value
     * @throws DataSetException
     */
    public DataCell(DataColumn column, Object value) throws DataSetException {
        if (column == null) {
            throw new DataSetException("DataColumn не может быть NULL");
        }
        this.column = column;
        // преобразуем значение из необратимых типов данных (например oracle.sql.struct)
        this.value = getTrueValue(column, value);
    }

    /**
     * Конструктор (objectString - строковое представление Навстат типа)
     *
     * Для обратного построения DataSet из строкового представления
     *
     * Параметры изменены местами по отношению к другому конструктору специально
     *
     * @param column
     * @param objectString
     */
    public DataCell(String objectString, DataColumn column) throws Exception {
        if (column == null) {
            throw new Exception("DataColumn не может быть NULL");
        }
        this.column = column;
        // отстраиваем объект по строковому представлению
        this.value = getObjectValue(column, objectString);
    }

    /**
     * Возвращает объект по его строковому представлению. Т.е. по сути парсит строку в нужный объект
     *
     * @param column
     * @param objectString строковое представление объекта
     * @return объект
     * @throws net.stemteam.datatransport.exception.UnsupportedColumnTypeException
     * @throws Exception неизвестный тип данных
     */
    public static Object getObjectValue(DataColumn column, String objectString) throws UnsupportedColumnTypeException,
            Exception {
        if (objectString == null) {
            return null;
        }
        if (column.getType().equals(DataColumnType.STRING)) {
            return StringEncoder.decodeString(objectString);
        } else if (column.getType().equals(DataColumnType.DATETIME)) {
            return StringEncoder.decodeDateTime(objectString);
        } else if (column.getType().equals(DataColumnType.TIMESTAMP)) {
            return StringEncoder.decodeTimeStamp(objectString);
        } else if (column.getType().equals(DataColumnType.DATE)) {
            return StringEncoder.decodeDate(objectString);
        } else if (column.getType().equals(DataColumnType.INT16)) {
            return Integer.valueOf(objectString);
        } else if (column.getType().equals(DataColumnType.INT32)) {
            return Integer.valueOf(objectString);
        } else if (column.getType().equals(DataColumnType.INT64)) {
            return Long.valueOf(objectString);
        } else if (column.getType().equals(DataColumnType.FLOAT)) {
            return DoubleEncoder.decodeDouble(objectString);
        } else if (column.getType().equals(DataColumnType.GEOMETRY)) {
            return IkGeometry.geometryFromString(objectString);
        } else if (column.getType().equals(DataColumnType.BOOLEAN)) {
            return objectString.equalsIgnoreCase("1");
        } else if (column.getType().equals(DataColumnType.DATASET)) {
            return DataSet.fromSafeString(objectString);
        } else {
            throw new UnsupportedColumnTypeException("Неизвестный тип данных: " + column.getDataTypeName());
        }
    }

    /**
     * Возвращает правильный объект ячейки на базе объекта, возвращенного из ResultSet. Служит для исправления
     * несоответсвия внутренних типов java и типов данных, возвращаемых JDBC-драйвером
     *
     * @param column
     * @param value
     * @return
     * @throws SQLException
     */
    private static Object getTrueValue(DataColumn column, Object value) throws DataSetException {
        if (value == null) {
            return null;
        } else // oracle.sql.STRUCT => JGeometry
        if (column.getType().equals(DataColumnType.GEOMETRY)) {
            if (value instanceof oracle.sql.STRUCT) {
                try {
                    // читаем геометрию из оракла
                    JGeometry geom = OraGeometry.getGeometry(value);
                    try {
                        // переводим в IkGeometry
                        String geoStr = OraGeometry.getGeometryString(geom);
                        IkGeometry g = IkGeometry.geometryFromString(geoStr);
                        return g;
                    } catch (Exception ex) {
                        throw new DataSetException("Не удалось преобразовать геометрию Oracle в IkGeometry в колонке "
                                + column.getName());
                    }
                } catch (SQLException ex) {
                    throw new DataSetException("Ошибка парсинга геометрии ORACLE. " + ex.toString());
                }
            } else if (value instanceof String) {
                return IkGeometry.geometryFromString(value.toString());
            } else {
                throw new DataSetException("Ошибка парсинга геометрии. Неизвестен класс возвращенного объекта");
            }
            // oracle.sql.TIMESTAMP => java.sql.Timestamp
        } else if (column.getType().equals(DataColumnType.DATETIME)) {
            if (value instanceof oracle.sql.TIMESTAMP) {
                try {
                    return ((oracle.sql.TIMESTAMP) value).timestampValue();
                } catch (SQLException ex) {
                    throw new DataSetException("Ошибка парсинга oracle.sql.TIMESTAMP. " + ex.toString());
                }
            } else {
                return (java.sql.Timestamp) value;
            }
        } else if (column.getType().equals(DataColumnType.TIMESTAMP)) {
            if (value instanceof oracle.sql.TIMESTAMP) {
                try {
                    return ((oracle.sql.TIMESTAMP) value).timestampValue();
                } catch (SQLException ex) {
                    throw new DataSetException("Ошибка парсинга oracle.sql.TIMESTAMP. " + ex.toString());
                }
            } else {
                return (java.sql.Timestamp) value;
            }
        } else {
            // другие значения не изменяем
            return value;
        }
    }

    public Integer getInteger() {
        if (value == null) {
            return null;
        }
        return (Integer) value;
    }

    public String getString() {
        if (value == null) {
            return null;
        }
        return (String) value;
    }

    public java.sql.Timestamp getTimestamp() {
        if (value == null) {
            return null;
        }
        return (java.sql.Timestamp) value;
    }

    /**
     * AM с преобразованием в Double
     *
     * @return
     */
    public Double getDouble() {
        if (value == null) {
            return null;
        }
        return (Double) value;
    }

    public Boolean getBoolean() {
        if (value == null) {
            return null;
        }
        return (Boolean) value;
    }

    public Float getFloat() {
        if (value == null) {
            return null;
        }
        return (Float) value;
    }

    public DataSet getDataSet() {
        if (value == null) {
            return null;
        }
        return (DataSet) value;
    }

    public Number getNumber() {
        if (value == null) {
            return null;
        }
        return (Number) value;
    }

    public BigDecimal getBigDecimal() {
        if (value == null) {
            return null;
        }
        return (BigDecimal) value;
    }

    public IkGeometry getGeometry() {
        if (value == null) {
            return null;
        }
        return (IkGeometry) value;
    }

    /**
     * Возвращает Long значение. Для типов Integer производится приведение к Long.
     *
     * @return
     */
    public Long getLong() {
        if (value == null) {
            return null;
        }
        if (value instanceof Integer) {
            return new Long((Integer) value);
        }
        return (Long) value;
    }
}
