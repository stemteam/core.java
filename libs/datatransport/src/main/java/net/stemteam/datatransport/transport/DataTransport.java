package net.stemteam.datatransport.transport;

import net.stemteam.datatransport.exception.UnsupportedTransportVersionException;
import net.stemteam.datatransport.tool.GzipArchiver;
import net.stemteam.datatransport.tool.Base64;
import net.stemteam.datatransport.tool.Exceptor;
import net.stemteam.datatransport.tool.Md5;
import net.stemteam.datatransport.tool.Splitter;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.sun.org.apache.xml.internal.security.utils.Base64;

/**
 * Класс-транспорт данных.
 * Глубокий смысл в передаче вложенного DataSet, заполняемого самостоятельно,
 * передаче размера хэш-кода данных в виде строки или ошибки еси таковая при
 * запросе данных имела место быть.
 *
 * @author Roman E. Kasovskiy, medved
 */

public class DataTransport implements Serializable
{
    /**
     * Версия транспорта по умолчанию. Используется экранирование строковых 
     * полей и полей дат в Base64. Затем используется компрессия GZIP, после 
     * чего сождержимое переводится в Base64 строку.
     * 
     */
    public static final String VERSION_DEFAULT = "1";

    
    // типы датасета
    public static final int TYPE_ERROR = 1;
    public static final int TYPE_DATASET = 2;
    public static final int TYPE_SIMPLE = 3;

    
    private String version;
    /**
     * Возвращает текущую версию транспорта
     * @return 
     */
    public String getVersion() {
        return version;
    }
    
    
    /**
     * Конструктор транспорта с версией по умолчанию
     */
    public DataTransport()
    {
        error = "";
        data = "";
        hash = "";
        version = VERSION_DEFAULT;
    }
    
    /**
     * Конструктор транспорта, использует версию
     * @param version - версия транспорта
     * @throws UnsupportedTransportVersionException
     */
    public DataTransport(String version) throws UnsupportedTransportVersionException
    {
        error = "";
        data = "";
        hash = "";
        // проверяем версию
        if (VERSION_DEFAULT.compareToIgnoreCase(version) != 0) {
            throw new UnsupportedTransportVersionException(version);
        }
        this.version = version;
    }

    // строка с данными (BASE 64)
    private String data;
    public String getData()
    {
        return this.data;
    }
    // сеттер обязателен (иначе не появится в SOAP), но ничего не меняет, ибо изменения здесь роптиворечат логике класса
    public void setData(String data) {} 

    // MD5 от данных (строкой BASE64)
    private String hash;
    public String getHash()
    {
        return this.hash;
    }
    // сеттер обязателен (иначе не появится в SOAP), но ничего не меняет, ибо изменения здесь проптиворечат логике класса
    public void setHash(String hash) {} 

    /**
     * Преобразовывает датасет к строковому представлению
     * @return
     */
    @Override
    public String toString() {
        String result = "";
        if (this.error != null)
            result += this.error;
        result += DataSet.COLUMN_SEPARATOR;
        if (this.hash != null)
            result += this.hash;
        result += DataSet.COLUMN_SEPARATOR;
        if (this.data != null)
            result += this.data;
        return result;
    }

    /**
     * Формирует транспорт из его строкового представления, т.е. из строки, 
     * содержащей Hash, Data, Error разделенные тегами. Обратная к 
     * DataTransport.toString().
     * @return
     */
    public void fromString(String stringData) {
        Vector<String> datas = Splitter.Split(stringData, DataSet.COLUMN_SEPARATOR);
        this.error = datas.get(0).equals("") ? null : datas.get(0);
        this.hash = datas.get(1);
        this.data = datas.get(2);
    }

    // ошибка выполнение выб-метода (если успешно - здесь пусто)
    private String error;
    public String getError() {
        return error;
    }
    public void setError(String error) {
        Clear();
        this.error = error;
    }    

    /**
     * Очистка полей
     */
    private void Clear() {
        this.error = null;
        this.data = null;
        this.hash = null;
    }

    /**
     * Вынесено в отдельную процедуру для профайлера только
     */
    private static final byte[] applyDataSetPackData(byte[] data) throws IOException{
        return GzipArchiver.packArray(data);
    }

    /**
     * Парсит датасет в структуру DataTransport для передачи
     * @param dataSet
     */
    public DataTransport applyDataSet(DataSet dataSet)
    {
        // очистка полей
        Clear();
        // получаем датасет строкой
        String result = dataSet.toString();
        // пакуем (если есть что)
        if (result != null)
        {
            // перекодирование в UTF-8
            byte[] input;
            try {
                input = result.getBytes("UTF-8");
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(DataTransport.class.getName()).log(Level.SEVERE, null, ex);
                setError(Exceptor.getErrorString(ex));
                return this;
            }

            // сжатие данных алгоритмом GZIP
            byte[] outputBytes;
            try {
                outputBytes = applyDataSetPackData(input);
            } catch (IOException ex) {
                Logger.getLogger(DataTransport.class.getName()).log(Level.SEVERE, null, ex);
                setError(Exceptor.getErrorString(ex));
                return this;
            }

            // кодирование строки данных Base64 (для передачи бинарных архивных данных)
            this.data = Base64.encode(outputBytes);

            // подсчитываем MD5 строки в Base64, резальтат кодируем в Base64
            try {
                this.hash = Base64.encode(Md5.getMd5(this.data.getBytes()));
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(DataTransport.class.getName()).log(Level.SEVERE, null, ex);
                setError(Exceptor.getErrorString(ex));
                return this;
            }
        }
        return this;
    }

    /**
     * Парсит строку в структуру DataTransport для передачи
     * Не используется hash-код и длина строки
     * Строка не переводится в Base64
     * @param s
     */
    public void applyString(String s) {
        Clear();
        this.data = s;
    }

    /**
     * Преобразовывает содержимое дататранспорта в датасет
     *
     * Перед преобразованием необходима проверка getType() == TYPE_DATASET
     *
     * @return
     * @throws Exception
     */
    public DataSet getDataSet() throws Exception {
        // проверяем тип (должен быть датасет)
        if (getType() == TYPE_DATASET) {
            DataSet dataSet = new DataSet();
            // распаковываем данные
            byte[] compressedData = Base64.decode(this.data);
            byte[] decompressedData = GzipArchiver.unpackArray(compressedData);
            // переводим в строку UTF-8
            String dataString = new String(decompressedData, "UTF-8");
            // применяем к датасету
            dataSet.UnPackData(dataString);
            // возвращаем датасет
            return dataSet;

        } else { // если другого типа - возращаем null
            throw new Exception("Данные не в формате TYPE_DATASET");
        }
    }

    /**
     * Возвращает тип данных в транспорте
     * @return
     */
    public int getType() {
        if (this.error == null)
        {
            if (this.hash == null)
                return TYPE_SIMPLE;
            else
                return TYPE_DATASET;
        } else {
            return TYPE_ERROR;
        }
    }

    /**
     * Создает транспорт из присланных данных и md5 (все кодировано в Base64)
     * @param hash
     * @param data
     * @throws Exception
     */
    public void applyWebRequest(String hash, String data) throws Exception {
        Clear();
        if (hash == null)
            throw new Exception("MD5 hash не может быть пустым");
        if (data == null)
            throw new Exception("Data не может быть пустым");
        // подсчитываем md5 hash
        String calcHash = Base64.encode(Md5.getMd5(data.getBytes()));
        // сверяем с присланным
        if (calcHash.equals(hash)){
            this.data = data;
            this.hash = hash;
        } else {
            throw new Exception("MD5 не совпадает с присланным");
        }
    }


    /**
     * Возвращает пустой транспорт со строкой ошибки. Сделано для оптимизации
     * кода, т.е. чтобы писать одну строку вместо трех
     * @param errorMessage
     * @return
     */
    public static DataTransport getTransportWithError(String errorMessage) {
        DataTransport transport = new DataTransport();
        transport.setError(errorMessage);
        return transport;
    }
    
}
