/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.examples;

import net.stemteam.datatransport.exception.ColumnNotFoundException;
import net.stemteam.datatransport.transport.DataColumn;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataRecord;
import net.stemteam.datatransport.transport.DataSet;

/**
 * Пример возбуждения исключений в транспорте и как их перехватывать.
 * 
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class ExceptionsUsage {
    
    public static void main(String[] args) {
        // формируем датасет
        DataSet ds = null;
        try {
            ds = prepareDataSetWithData();
        } catch (Exception ex) {
            System.out.println("Ошибка подготовки датасета" + ex);
            System.exit(1);
        }
        
        // печатаем датасет
        System.out.println(ds.toDump());
        
        
        // обрабатываем ошибку поиска значения по названию колонки (аналогично по индексу)
        DataRecord row = ds.getRecords().get(0);
        try {
            String name = row.getString("NAME");
            System.out.println("Правильное обращение по имени, NAME = " + name);
            name = row.getString("DESCR");
            System.out.println("Эта строчка не напечатается, потому что колонки с именем DESCR нет в наборе");
        } catch (ColumnNotFoundException ex) {
            System.out.println("Ошибка обращения по имени " + ex);
        }
        
        // обрабатываем ошибку поиска значения по индексу колонки
        try {
            Integer id = row.getInteger(0);
            System.out.println("Правильное обращение по индексу 0");
            id = row.getInteger(3);
            System.out.println("Эта строчка не напечатается, потому что колонки с индексом 3 нет в наборе");
        } catch (ColumnNotFoundException ex) {
            System.out.println("Ошибка обращения по индексу " + ex);
        }
        
        // работа с boolean (правильная)
        try {
            row = ds.getRecords().get(0);
            Boolean is_true = row.getBoolean("IS_TRUE");
            System.out.println("(row 1) is_alive = " + is_true);
            
            row = ds.getRecords().get(2);
            is_true = row.getBoolean("IS_TRUE");
            System.out.println("(row 3) is_alive = " + is_true);
        } catch (ColumnNotFoundException ex) {
            System.out.println("Ошибка чтения boolean значения " + ex);
        }
        
        // работа с boolean (неправильная)
        try {
            row = ds.getRecords().get(0);
            // заполняем значение через неправильную функцию
            row.setInt("IS_TRUE", 1);
            // читаем
            Boolean is_true = row.getBoolean("IS_TRUE");            
            System.out.println("(row 1) is_true = " + is_true);
        } catch (ColumnNotFoundException ex) {
            System.out.println("Ошибка чтения boolean значения " + ex);
        }
        
        
        
    }
    
    /**
     * Подготавливает датасет с данными
     * @return
     * @throws Exception 
     */
    private static DataSet prepareDataSetWithData() throws Exception {
        DataSet ds = new DataSet();
        ds.createColumn("ID", DataColumnType.INT32);
        ds.createColumn("NAME", DataColumnType.STRING);
        ds.createColumn("IS_TRUE", DataColumnType.BOOLEAN);
        DataRecord row = ds.createRecord();
        row.setInt("ID", 1);
        row.setString("NAME", "Александр Сергеевич Пушкин");
        row.setBoolean("IS_TRUE", false);
        row = ds.createRecord();
        row.setInt("ID", 2);
        row.setString("NAME", "Федор Михайлович Достоевский");
        row.setBoolean("IS_TRUE", true);
        row = ds.createRecord();
        row.setInt("ID", 3);
        row.setString("NAME", "Антон Павлович Чехов");
        row.setBoolean("IS_TRUE", null);
        return ds;
    }
    
}
