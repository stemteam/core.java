/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.examples;

import net.stemteam.datatransport.transport.DataColumn;
import net.stemteam.datatransport.transport.DataRecord;
import net.stemteam.datatransport.transport.DataSet;
import java.util.Vector;
import net.stemteam.datatransport.transport.DataColumnType;

/**
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class ArrayDataAccessExample {
    
    public static void main(String[] args) throws Exception {
        
        // создаем вложенный датасет 
        DataSet ds = createDataSet();
        
        System.out.println("Список значений в колонке NUM");
        for (Object o : ds.getColumnValues("NUM")) {
            System.out.println(o);
        }
        
        System.out.println("Список значений в колонке DESCRIPTION");
        for (String o : ds.getColumnStringValues("DESCRIPTION")) {
            System.out.println(o);
        }
        
        System.out.println("ColumnNotFound - исключение когда колонка не найдена");
        for (Object o : ds.getColumnValues("XXX")) {
            System.out.println(o);
        }
       
    }
    
    /**
     * Создает наполненый данными датасет для вложения в другой
     * @return
     * @throws Exception 
     */
    private static DataSet createDataSet() throws Exception {
        DataRecord record;
        DataSet nestedDataSet = new DataSet();
        // создаем колонки
        nestedDataSet.createColumn("NUM", DataColumnType.INT16);
        nestedDataSet.createColumn("DESCRIPTION", DataColumnType.STRING);
        // создаем записи в датасете (метод может быть вызван только после создания колонок)
        record = nestedDataSet.createRecord();
        record.setInt("NUM", 1);
        record.setString("DESCRIPTION", "Существуют такие системы отсчёта, называемые инерциальными, относительно которых материальная точка при отсутствии внешних воздействий сохраняет величину и направление своей скорости неограниченно долго.");
        record = nestedDataSet.createRecord();
        record.setInt("NUM", 2);
        record.setString("DESCRIPTION", "В инерциальной системе отсчета ускорение, которое получает материальная точка, прямо пропорционально равнодействующей всех приложенных к ней сил и обратно пропорционально её массе.");
        record = nestedDataSet.createRecord();
        record.setInt("NUM", 3);
        record.setString("DESCRIPTION", "Действию всегда есть равное и противоположное противодействие, иначе — взаимодействия двух тел друг на друга равны и направлены в противоположные стороны.");
        return nestedDataSet;
    }
    
}
