/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.tool;

import java.util.Vector;

/**
 * Геометрия Навстат. Функции для работы с геометрией. 
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class IkGeometry {
    
    // разделитель параметров строкового представления геометрии (getGeometryString)
    public static String GEOMETRY_PARAM_SEP = ":";
    // разделитель элементов в массиве геометрии
    public static String GEOMETRY_ARRAY_SEP = " ";

    // типы геометрии
    public static final int GTYPE_POINT = 1;
    public static final int GTYPE_CURVE = 2;
    public static final int GTYPE_POLYGON = 3;
    public static final int GTYPE_COLLECTION = 4;
    public static final int GTYPE_MULTIPOINT = 5;
    public static final int GTYPE_MULTICURVE = 6;
    public static final int GTYPE_MULTIPOLYGON = 7;
    
    
    private int dimension;
    private int type;
    private Vector<Double> coords;
    
    /**
     * Создает объект типа геометрия
     */
    public IkGeometry() {
        coords = new Vector();
        dimension = 2; // по умолчанию двумерная геотмерия
        type = GTYPE_POINT; // по умолчанию тип - точка
    }

    /**
     * Возвращает координаты точек гемотерии
     * @return 
     */
    public Vector<Double> getCoords() {
        return coords;
    }

    /**
     * Устанавливает координаты точек геометрии
     * @param coords 
     */
    public void setCoords(Vector<Double> coords) {
        this.coords = coords;
    }

    /**
     * Возвращает размерность геометрии
     * @return 
     */
    public int getDimension() {
        return dimension;
    }

    /**
     * Устанавливает размерность геометрии
     * @param dimension 
     */
    public void setDimension(int dimension) {
        if ((dimension < 2) || (dimension > 3)) {
            throw new ArrayIndexOutOfBoundsException("Размерность геометрии может быть 2 или 3");
        }
        this.dimension = dimension;
    }

    /**
     * Возвращает тип геотмерии
     * @return 
     */
    public int getType() {
        return type;
    }

    /**
     * Устанавливает тип геометрии
     * @param type 
     */
    public void setType(int type) {
        if ((type < 1) || (type > 7)) {
            throw new ArrayIndexOutOfBoundsException("Неизвестный тип геометрии");
        }
        this.type = type;
    }
    
    /**
     * Создает двумерную точечную геометрию
     * @param longitude
     * @param latitude
     * @return 
     */
    public static IkGeometry createPoint2D(double longitude, double latitude) {
        IkGeometry geom = new IkGeometry();
        geom.setDimension(2);
        geom.setType(GTYPE_POINT);
        geom.getCoords().add(longitude);
        geom.getCoords().add(latitude);
        return geom;
    }
    
    /**
     * Создает трехмерную точечную геометрию
     * @param longitude
     * @param latitude
     * @param altitude
     * @return 
     */
    public static IkGeometry createPoint3D(double longitude, double latitude, double altitude) {
        IkGeometry geom = new IkGeometry();
        geom.setDimension(3);
        geom.setType(GTYPE_POINT);
        geom.getCoords().add(longitude);
        geom.getCoords().add(latitude);
        geom.getCoords().add(altitude);
        return geom;
    }
    
    
    //<editor-fold defaultstate="collapsed" desc="getPointToPointDistanceGreatCircle">
    /**
     * This uses the "haversine" formula to calculate the great-circle distance 
     * between two points – that is, the shortest distance over the earth’s 
     * surface – giving an ‘as-the-crow-flies’ distance between the points 
     * (ignoring any hills, of course!).
     * @param lon1 (градусы и их доли)
     * @param lat1
     * @param lon2
     * @param lat2
     * @deprecated Быстрый, но неточный на больших расстояниях погрешность может достигать 1.5 км на 600
     * @return расстояние в метрах
     */
    @Deprecated
    public static double getPointToPointDistanceGreatCircle(double lon1, double lat1, double lon2, double lat2) {
        double R = 6378137.0; // радиус земли в метрах       
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
        double a = Math.sin(dLat / 2.0) * Math.sin(dLat / 2.0) + Math.sin(dLon / 2.0) * Math.sin(dLon / 2.0) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1.0 - a));
        double d = R * c;
        return d;
    }
    //</editor-fold>
    
    /**
     * Vincenty formula for distance between two Latitude/Longitude points.
     * Vincenty’s formula is accurate to within 0.5mm, or 0.000015" (!), on 
     * the ellipsoid being used. Calculations based on a spherical model, such 
     * as the (much simpler) Haversine, are accurate to around 0.3% (which is 
     * still good enough for most purposes, of course). 
     * Port from JavaScript by Andrey Nikolaev.
     * 
     * @param lon1
     * @param lat1
     * @param lon2
     * @param lat2
     * @return distance in meter
     */
    public static double getPointToPointDistanceVincenty(double lon1, double lat1, double lon2, double lat2) {
        
        final double a = 6378137.0;
        final double b = 6356752.314245;
        final double f = 1/298.257223563;  // WGS-84 ellipsoid params
        
        double L = Math.toRadians(lon2-lon1);
        double U1 = Math.atan((1-f) * Math.tan(Math.toRadians(lat1)));
        double U2 = Math.atan((1-f) * Math.tan(Math.toRadians(lat2)));
        double sinU1 = Math.sin(U1), cosU1 = Math.cos(U1);
        double sinU2 = Math.sin(U2), cosU2 = Math.cos(U2);
        
        double lambda = L, lambdaP, iterLimit = 100;
        double cosSqAlpha, cos2SigmaM, cosSigma, sinSigma, sinLambda, cosLambda, sigma;
        do {
            sinLambda = Math.sin(lambda);
            cosLambda = Math.cos(lambda);
            sinSigma = Math.sqrt((cosU2*sinLambda) * (cosU2*sinLambda) + (cosU1*sinU2-sinU1*cosU2*cosLambda) * (cosU1*sinU2-sinU1*cosU2*cosLambda));
            if (sinSigma == 0) { // co-incident points
                return 0;
            }
            cosSigma = sinU1*sinU2 + cosU1*cosU2*cosLambda;
            sigma = Math.atan2(sinSigma, cosSigma);
            double sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
            cosSqAlpha = 1 - sinAlpha*sinAlpha;
            cos2SigmaM = cosSigma - 2*sinU1*sinU2/cosSqAlpha;
            
            
            if (Double.isNaN(cos2SigmaM)) cos2SigmaM = 0;  // equatorial line: cosSqAlpha=0 (§6)
            double C = f/16*cosSqAlpha*(4+f*(4-3*cosSqAlpha));
            lambdaP = lambda;
            lambda = L + (1-C) * f * sinAlpha * (sigma + C*sinSigma*(cos2SigmaM+C*cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)));
        } while (Math.abs(lambda-lambdaP) > 1e-12 && --iterLimit>0);
        
        if (iterLimit==0) return Double.NaN;  // formula failed to converge

        double uSq = cosSqAlpha * (a*a - b*b) / (b*b);
        double A = 1 + uSq/16384*(4096+uSq*(-768+uSq*(320-175*uSq)));
        double B = uSq/1024 * (256+uSq*(-128+uSq*(74-47*uSq)));
        double deltaSigma = B*sinSigma*(cos2SigmaM+B/4*(cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)- B/6*cos2SigmaM*(-3+4*sinSigma*sinSigma)*(-3+4*cos2SigmaM*cos2SigmaM)));
        double s = b*A*(sigma-deltaSigma);
  
        return s;
  
//  // note: to return initial/final bearings in addition to distance, use something like:
//  var fwdAz = Math.atan2(cosU2*sinLambda,  cosU1*sinU2-sinU1*cosU2*cosLambda);
//  var revAz = Math.atan2(cosU1*sinLambda, -sinU1*cosU2+cosU1*sinU2*cosLambda);
//  return { distance: s, initialBearing: fwdAz.toDeg(), finalBearing: revAz.toDeg() };
                
    }
    
    
    //<editor-fold defaultstate="collapsed" desc="getAzimuth">
    /**
     * Возвращает азимут
     * @param lon1 (градусы и их доли)
     * @param lat1
     * @param lon2
     * @param lat2
     * @return 
     */
    public static double getAzimuth(double lon1, double lat1, double lon2, double lat2) {
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        double cl1 = Math.cos(lat1);
        double cl2 = Math.cos(lat2);
        double sl1 = Math.sin(lat1);
        double sl2 = Math.sin(lat2);
        double delta = lon2 - lon1;
        double cdelta = Math.cos(delta);
        double sdelta = Math.sin(delta);
        double x = (cl1 * sl2) - (sl1 * cl2 * cdelta);
        if (x == 0.0) {
          return 0;
        }
        double y = sdelta * cl2;
        double z = Math.toDegrees(Math.atan(-y/x));
        if (x < 0) {
            z = z + 180.0;
        }
        z = -((z + 180.0) % 360.0) - 180.0;
        z = Math.toRadians(z);
        double angle = z - (2.0 * Math.PI) * Math.floor(z/(2.0 * Math.PI));
        angle = Math.round(Math.toDegrees(angle));
        return angle;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="getPointToVectorDistance">
    /**
     * Определение расстояния между точкой (x0, y0) и отрезком (x1, y1) - (x2, y2). 
     * Если из точки нельзя опустить перпендикуляр на отрезок, расстояние 
     * считается до ближайшей точки отрезка.
     * Ура! В процессе доступна точка пересечения с отрезком. 
     * TODO не учитывается ситуация отрезка, параллельного осям координат (можно оптимизировать)
     * @param x0 абсцисса точки
     * @param y0 ордината точки
     * @param x1 абсцисса начала отрезка
     * @param y1 ордината начала отрезка
     * @param x2 абсцисса конца отрезка
     * @param y2 ордината конца отрезка
     * @return расстояние
     */
    public static double getPointToVectorDistance(double x0, double y0, double x1, double y1, double x2, double y2) {
       
        // определяем коэффициенты прямой : Ax + By + C = 0 или y = (-A/B)x - C/B
        double A = y2 - y1;
        double B = x1 - x2;
        double C = y1*x2 - x1*y2;
        
        // определяем уравнение перпендикулярной прямой; ее коэффициент перед x будет (B/A), т.е. y = (B/A)x + K
        // подставим в это уравнение нашу точку, найдем K
        double K = y0 - (B / A) * x0;
        
        // находим точку пересечения прямых
        // решаем систему уравнений
        //   y = (B/A)x + K;
        //   y = (-A/B)x - C/B
        //   -- итого --
        //   x = (-K - C/B) / (B/A + A/B)
        //   y = (B/A)x + K;
        double x4 = (-K -C/B) / (B/A + A/B);
        double y4 = (B/A)*x4 + K;
        
        // определяем, лежит ли эта точка на отрезке
        boolean inline = false;
        if (x1 < x2) {
            if ((x4 >= x1) & (x4 <= x2))
                inline = true;
        } else {
            if ((x4 >= x2) & (x4 <= x1))
                inline = true;
        }
        
        // если лежит на отрезке - определяем расстояние до прямой и возвращаем его
        double distance;
        if (inline) {
            distance = Math.abs(A*x0 + B*y0 + C) / Math.sqrt(A*A + B*B);
            return distance;
        }
        
        // если не лежит - считаем расстояния до концов отрезков и возвращаем меньшее
        double distTo1 = Math.sqrt((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1));
        double distTo2 = Math.sqrt((x0 - x2) * (x0 - x2) + (y0 - y2) * (y0 - y2));
        return Math.min(distTo1, distTo2);
    }
    //</editor-fold>
 
    /**
     * Возвращает строковое представление геометрии (для транспорта, неэкранированное)
     * @return 
     */
    @Override
    public String toString() {
        return geometryToString(this);
    }
    
    /**
     * Возвращает строковое представление геометрии (для транспорта, неэкранированное)
     * @param geom
     * @return 
     */
    public static String geometryToString(IkGeometry geom) {
        if (geom == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(geom.getDimension());
        sb.append(GEOMETRY_PARAM_SEP);
        sb.append(geom.getType());
        sb.append(GEOMETRY_PARAM_SEP);
        // точечная геометрия | полигон | полилиния
        if ((geom.getType() == GTYPE_POINT) || (geom.getType() == GTYPE_POLYGON) || (geom.getType() == GTYPE_CURVE)) {
            boolean first = true;
            for (Double coord : geom.getCoords()) {
                if (first)
                {
                    sb.append(Math.round(coord * 1000000.0) / 1000000.0);
                    first = false;
                } else {
                    sb.append(GEOMETRY_ARRAY_SEP);
                    sb.append(Math.round(coord * 1000000.0) / 1000000.0);
                }
            }
            return sb.toString();
        }
        // для неподдерживаемых типов геометрии генерируем исключение
        throw new IllegalArgumentException("Неподдерживаемый тип геометрии");
    }
    
    /**
     * Парсит строку в Ikeometry
     * @param geom
     * @return 
     */
    public static IkGeometry geometryFromString(String geom) {
        Vector<String> geometryList = Splitter.Split(geom, GEOMETRY_PARAM_SEP);
        IkGeometry g = new IkGeometry();
        g.setDimension(Integer.valueOf(geometryList.get(0)));
        g.setType(Integer.valueOf(geometryList.get(1)));
        // ординаты
        Vector<String> coords = Splitter.Split(geometryList.get(2), GEOMETRY_ARRAY_SEP);
        for (String coord : coords) {
            g.getCoords().add(Double.valueOf(coord));
        }
        return g;
    }
    
    /**
     * Возвращает MBR (Minimum bounding rectangle) для текущей геометрии для 
     * 2D случая.
     * @return 
     */
    public IkGeometry getMBR() {
        if (this.getType() == GTYPE_POINT) {
            return this;
        }
        if (this.getDimension() != 2) {
            throw new IllegalArgumentException("Поддерживается только 2D геометрия");
        }
        IkGeometry mbr = new IkGeometry();
        mbr.setType(GTYPE_POLYGON);
        mbr.setDimension(2);
        // определяем размерность
        double lonMin = 1000, lonMax = -1000, latMin = 1000, latMax = -1000;
        int size = this.getCoords().size();
        for (int i=0; i<size; i++) {
            double coord = this.getCoords().get(i);
            // долгота
            if ((i % 2) == 0) {
                lonMin = Math.min(coord, lonMin);
                lonMax = Math.max(coord, lonMax);
            }
            // широта
            if ((i % 2) == 1) {
                latMin = Math.min(coord, latMin);
                latMax = Math.max(coord, latMax);
            }
        }
        mbr.getCoords().add(lonMin);
        mbr.getCoords().add(latMin);
        mbr.getCoords().add(lonMin);
        mbr.getCoords().add(latMax);
        mbr.getCoords().add(lonMax);
        mbr.getCoords().add(latMax);
        mbr.getCoords().add(lonMax);
        mbr.getCoords().add(latMin);
        mbr.getCoords().add(lonMin);
        mbr.getCoords().add(latMin);
        return mbr;
    }
    
    /**
     * Считаем центройд (точка - центр геометрии)
     * @return 
     */
    public IkGeometry getCentroid() {
        if (this.getType() == GTYPE_POINT) {
            return this;
        }
        IkGeometry mbr = this.getMBR();
        if (mbr.getType() == GTYPE_POINT) {
            return mbr;
        }
        double avgLon = (mbr.getCoords().get(0) + mbr.getCoords().get(4)) / 2.0;
        double avgLat = (mbr.getCoords().get(1) + mbr.getCoords().get(3)) / 2.0;
        return IkGeometry.createPoint2D(avgLon, avgLat);
    }
    
    /**
     * Формирует буфер вокруг точки с указанным радиусом. Искажение вычисляется
     * по координатам в зависимости от местоположения с шагом 1 градус (фиг его 
     * знает достаточно этого или нет).
     * @param point
     * @param radiusInMeter радиус, метры
     * @return 
     */
    public static IkGeometry getPointBuffer(IkGeometry point, double radiusInMeter) {
        IkGeometry buffer = new IkGeometry();
        buffer.setDimension(2);
        buffer.setType(GTYPE_POLYGON);
        double X0 = point.getCoords().get(0);
        double Y0 = point.getCoords().get(1);
        
        // Вычисляем искажение. Для этого отрисовываем справа и слева, сверху и 
        // снизу равноудаленные точки и считаем расстояние между ними. 
        // После чего приводим расстояния на экваторе и высчитываем соотношение.
        // todo учитывать случай попадания на полюсы земли и экватор
        double hAxis = IkGeometry.getPointToPointDistanceVincenty(X0 - 0.5, Y0, X0 + 0.5, Y0);
        double vAxis = IkGeometry.getPointToPointDistanceVincenty(X0, Y0 - 0.5, X0, Y0 + 0.5); 
        double hCoeff = hAxis / 111111.11;
        double vCoeff = vAxis / 111111.11;
        
        // переводим радиус в градусы для оценки величины круга
        double radiusInDegrees = radiusInMeter / 111111.11;
        // угловой шаг (градусы)
        double angleStep = 360.0 / radiusInMeter;
        if (angleStep > 36.0) {
            angleStep = 36.0; // десять точек для малых радиусов
        }
        double angle = 0;
        while (angle < 360) {
            double X = radiusInDegrees * Math.cos(Math.toRadians(angle)) + X0;
            double Y = radiusInDegrees * Math.sin(Math.toRadians(angle)) + Y0;
            // применяем искажение (сжатие)
            double deltaX = (X - X0) * (1.0 / hCoeff);
            X = X0 + deltaX;
            double deltaY = (Y - Y0) * (1.0 / vCoeff);
            Y = Y0 + deltaY;
            // округление
            X = Math.round(X * 1000000.0) / 1000000.0;
            Y = Math.round(Y * 1000000.0) / 1000000.0;
            buffer.getCoords().add(X);
            buffer.getCoords().add(Y);
            angle += angleStep;
        }
        return buffer;
    }
    
    /**
     * Вычисляет площадь полигона. Работает некорректно для полигонов с 
     * самопересечениями.
     * @param polygone полигон для вычисления площади.
     * @return 
     */
    public static double getSquare(IkGeometry polygone) {
        if (polygone.getType() != GTYPE_POLYGON) {
            throw new IllegalArgumentException("Поддерживается только полигон.");
        }
        if (polygone.getDimension() != 2) {
            throw new IllegalArgumentException("Поддерживается только 2-мерный полигон.");
        }
        
        int size = polygone.getCoords().size() / 2;
        
        // для вырожденных случаев возвращаем 0
        if (size < 3) {
            return 0;
        }
        
        // TODO переписать цикл на loop
        double result = 0;
        int index = 0;        
        for (int i = 1; i<size; i++) {
            // текущие значения
            double x = polygone.getCoords().get(index);
            index++;
            double y = polygone.getCoords().get(index);
            index++;            
            // следующие значения
            double xNext = polygone.getCoords().get(index);
            double yNext = polygone.getCoords().get(index+1);
            
            result += (x + xNext) * (y - yNext);
        }
        result = 0.5 * Math.abs(result);
        return result;
    }
    
    
    //<editor-fold defaultstate="collapsed" desc="Example of usage">
    public static void main(String[] args) {
        
//        IkGeometry ggg = IkGeometry.createPoint2D(30.0, 30.0);
//        IkGeometry buffer = IkGeometry.getPointBuffer(ggg, 4000);
//        System.out.println(buffer.getCoords().size());
//        if (1 == 1) return;
        
        
        //IkGeometry gg = IkGeometry.geometryFromString("2:3:0 0 0 1 1 1 1 0 0 0"); // квадрат
        
        
        IkGeometry gg = IkGeometry.geometryFromString("2:3:1 1 1 3 5 1 5 3 1 1"); // бантик, площадь 6
        System.out.println(IkGeometry.getSquare(gg));
        System.out.println(gg.getCentroid().toString());
        System.out.println(gg.getMBR().toString());
        if (1 == 1) return;
        
        
        // создаем геометрию
        IkGeometry geom = IkGeometry.createPoint2D(44.4444444, 33.333333);
        String geoStr = geom.toString();
        System.out.println(geoStr);
        
        // парсим геометрию из строки
        IkGeometry g = IkGeometry.geometryFromString(geoStr);
        System.out.println(g.toString());

        // геометрические функции
        
        // Считаем расстояние между точками
        //39,272878	51,79131
        //39,285833	51,786312        
        // OracleSpatial - дистанс 1052.76104525989 метра
        
        double lon1 = 39.272878;
        double lat1 = 51.79131;
        
        double lon2 = 29.285833;
        double lat2 = 51.786312;
       
        // расстояние между точками, метры (Great Circle)
        System.out.println("distance: " + IkGeometry.getPointToPointDistanceGreatCircle(lon1, lat1, lon2, lat2));   // работает в 15 раз быстрее чем из Geo Tool библиотеки, но менее точно
        
        // азимут (рукопись)
        System.out.println("azimuth: " + IkGeometry.getAzimuth(lon1, lat1, lon2, lat2));

        // WGS 84 Vincenty formula
        System.out.println("distance: " + IkGeometry.getPointToPointDistanceVincenty(lon1, lat1, lon2, lat2));  
        
        // speed test
        long st = System.currentTimeMillis();
        double dist = 0.0;
        // 1000000
        for (double d = 0.0; d < 1000; d += 0.001) {
//            dist = IkGeometry.getPointToPointDistanceVincenty(lon1, lat1, lon2 + d, lat2 + d);      // 2701
//            dist = IkGeometry.getPointToPointDistanceGreatCircle(lon1, lat1, lon2 + d, lat2 + d);   // 834
        }
        System.out.println(dist + " : " + (System.currentTimeMillis() - st) + " ms");
        
        // буфер точки
        System.out.println(getPointBuffer(IkGeometry.createPoint2D(9, 80), 100).toString());
    }
    //</editor-fold>

    public Object toJson() {      
        // новая свистоперделка
        GeomJsonContainer cont = new GeomJsonContainer();
        cont.setType(type);
        cont.setDim(dimension);        
        Vector points = new Vector();
        int index = 0;
        Vector<Double> point = new Vector<Double>();
        for (Double d : coords) {
            index++;
            if (index == dimension) {
                point.add(d);
                
                // меняем широту и долготу местами (так надо)
                if (dimension > 1) {
                    Double dd = point.get(0);
                    point.set(0, point.get(1));
                    point.set(1, dd);
                }
                
                
                points.add(point);
                point = new Vector();
                index = 0;
            } else {
                point.add(d);
            }
        }
        cont.setPoints(points);        
        return cont;
    }
    
    /**
     * Класс для корректного преобразования полей геометриив JSON в новых версиях
     */
    public static class GeomJsonContainer {
        private int type;
        private int dim;
        private Vector points;
        
        public GeomJsonContainer() {
            points = new Vector();
        }

        public int getDim() {
            return dim;
        }

        public void setDim(int dim) {
            this.dim = dim;
        }

        public Vector getPoints() {
            return points;
        }

        public void setPoints(Vector points) {
            this.points = points;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
        
    }   
    
}
