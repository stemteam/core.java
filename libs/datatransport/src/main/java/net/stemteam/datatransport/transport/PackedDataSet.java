/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.transport;

import net.stemteam.datatransport.exception.DataSetException;

/**
 * Упакованный датасет. Используется для гарантированной передачи по сети.
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class PackedDataSet {

    private static final String LENGTH_SEPARATOR = "|";

    private StringBuilder data = new StringBuilder();
    private int length = -1;

    /**
     * Упаковка датасета в строку с размером
     *
     * @param ds
     * @return
     */
    public static String pack(DataSet ds) {
        StringBuilder sb = new StringBuilder();
        final String dataSetString = ds.toString();
        sb.append(dataSetString.length()).append(LENGTH_SEPARATOR).append(dataSetString);
        return sb.toString();
    }

    /**
     * Распаковка строки в датасет
     *
     * @return
     * @throws Exception
     */
    public DataSet unpack() throws Exception {
        DataSet ds = new DataSet();
        ds.UnPackData(data.toString());
        return ds;
    }

    /**
     * Добавить часть данных к датасету (для формирования по частям)
     *
     * @param s
     * @return
     */
    public boolean append(String s) {
        if (length == -1) {
            int l = s.indexOf(LENGTH_SEPARATOR);
            if (l != -1) {
                String ss = s.substring(0, l);
                length = Integer.valueOf(ss);
                data.append(s.substring(l + 1));
            }
        } else {
            data.append(s);
        }
        return valid();
    }

    /**
     * Если содержит лишь часть датасета - вернет false
     *
     * @return
     */
    public boolean valid() {
        return length == data.length();
    }

    public static void main(String[] args) throws DataSetException, Exception {
        // тестовый набор
        DataSet ds = new DataSet();
        ds.createColumn("Id", DataColumnType.INT32);
        ds.createColumn("Name", DataColumnType.STRING);
        DataRecord row = ds.createRecord();
        row.setInt("Id", 1);
        row.setString("Name", "Иван Петров");

        // пакуем и смотрим
        String packedString = PackedDataSet.pack(ds);
        System.out.println(packedString);

        // распаковываем запакованный одной чатью
        PackedDataSet pds = new PackedDataSet();
        System.out.println("valid: " + pds.append(packedString));
        DataSet ds2 = pds.unpack();
        System.out.println("ds2: " + ds2.toString());

        // распаковываем из двух частей
        String part1 = packedString.substring(0, 15);
        String part2 = packedString.substring(15);
        PackedDataSet pds2 = new PackedDataSet();
        System.out.println("pds2.append part1: " + pds2.append(part1));
        System.out.println("pds2.append part2: " + pds2.append(part2));
        System.out.println("pds2.unpack: " + pds2.unpack());

    }

}
