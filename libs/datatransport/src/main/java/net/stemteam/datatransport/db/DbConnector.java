/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.stemteam.datatransport.db;

import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * <p>Получалка конектов к БД по JNDI имени пула коннектов</p>
 * <p>Пулл конектов должен быть создан в контексте сервера</p>
 * @author Andrey Nikolaev <andrey@infokinetika.ru>
 */
public class DbConnector {

    private static HashMap map;

    private static synchronized DataSource getDataSource(String jndiPoolName) throws ConnectionPoolNotFoundException {
        DataSource dataSource = null;
        if (map == null) {
            map = new HashMap();
        }
        Object obj = map.get(jndiPoolName);
        if (obj == null) {
            try {
                Context ctx = new InitialContext();
                dataSource = (DataSource) ctx.lookup(jndiPoolName);
            } catch (NamingException ex) {
                throw new ConnectionPoolNotFoundException(String.format("DataSource with JNDI-name %1$s is undefined.", jndiPoolName));
            }
            map.put(jndiPoolName, dataSource);
        } else {
            dataSource = (DataSource) obj;
        }
        return dataSource;
    }

    /**
     * <p>Возвращает логический (wrapped) коннект из JDBC-пула контекста сервера приложений.</p>
     * <p>Выставляется AutoCommit = false - по умолчанию</p>
     * @param jndiPoolName JNDI имя пула конектов
     * @return java.sql.Connection (logical)
     * @throws SQLException при получении соединения из пула
     * @throws ConnectionPoolNotFoundException при отсутсвии пула с указанным JNDI именем
     */
    public static synchronized Connection getConnection(String jndiPoolName) throws SQLException, ConnectionPoolNotFoundException
    {
        return getConnection(jndiPoolName, false);
    }

    /**
     * Возвращает соединение из пула конектов по JNDI имени
     * @param jndiPoolName JNDI имя пула конектов
     * @param isAutoCommit выставлять ли флаг автоматической фиксации транзакций
     * @return java.sql.Connection (logical)
     * @throws SQLException при получении соединения из пула
     * @throws ConnectionPoolNotFoundException при отсутсвии пула с указанным JNDI именем
     */
    public static synchronized Connection getConnection(String jndiPoolName, boolean isAutoCommit) throws SQLException, ConnectionPoolNotFoundException
    {
        DataSource ds = getDataSource(jndiPoolName);
        Connection result = ds.getConnection();
        result.setAutoCommit(isAutoCommit);
        return result;
    }

    /**
     * Освобождает логический коннект (возвращает в пулл)
     * @param wrappedConnection
     * @throws CloseDbConnectionException
     */
    public static synchronized void closeConnection(Connection wrappedConnection) throws CloseDbConnectionException {
        if (wrappedConnection != null) {
            try {
                wrappedConnection.close();
            } catch (SQLException ex) {
                throw new CloseDbConnectionException(ex);
            } finally {
                wrappedConnection = null;
            }
        }
    }


    /**
     * Если нужно получить физический конект из логического
     *
     * Obtaining a Physical Connection From a Wrapped Connection

    The DataSource implementation in the Application Server provides a getConnection method that retrieves the JDBC driver’s SQLConnection from the Application Server’s Connection wrapper. The method signature is as follows:

    public java.sql.Connection getConnection(java.sql.Connection con)
    throws java.sql.SQLException

    For example:

    InitialContext ctx = new InitialContext();
    com.sun.appserv.jdbc.DataSource ds = (com.sun.appserv.jdbc.DataSource)
       ctx.lookup("jdbc/MyBase");
    Connection con = ds.getConnection();
    Connection drivercon = ds.getConnection(con);
    // Do db operations.
    // Do not close driver connection.
    con.close(); // return wrapped connection to pool.
     */
    
    /**
     * Проверяет что бд Oracle. Определяет тип БД по метаданным открытого соединения
     * @param conn
     * @return
     * @throws SQLException 
     */
    public static boolean isDbTypeOracle(Connection conn) throws SQLException {
        if (conn == null) {
            throw new SQLException("Connection is NULL");
        }
        String productName = conn.getMetaData().getDatabaseProductName();
        if (productName == null) {
            throw new SQLException("Connection.getDatabaseProductName returns NULL");
        }
        productName = productName.toLowerCase();
        if (productName.indexOf("oracle") > -1) {
            return true;
        } 
        return false;
    }
    
    /**
     * Проверяет что бд Postgres. Определяет тип БД по метаданным открытого соединения
     * @param conn
     * @return
     * @throws SQLException 
     */
    public static boolean isDbTypePostgres(Connection conn) throws SQLException {
        if (conn == null) {
            throw new SQLException("Connection is NULL");
        }
        String productName = conn.getMetaData().getDatabaseProductName();
        if (productName == null) {
            throw new SQLException("Connection.getDatabaseProductName returns NULL");
        }
        productName = productName.toLowerCase();
        if (productName.indexOf("postgres") > -1) {
            return true;
        } 
        return false;
    }
   
 }
