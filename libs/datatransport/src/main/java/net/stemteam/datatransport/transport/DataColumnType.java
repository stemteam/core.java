/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.datatransport.transport;

/**
 * Тип колонки в транспорте
 */
public enum DataColumnType {

    STRING("String", String.class),
    INT16("Int16", Integer.class),
    INT32("Int32", Integer.class),
    INT64("Int64", Long.class),
    FLOAT("Decimal", Double.class),
    BOOLEAN("Boolean", Boolean.class),
    DATETIME("DateTime", java.sql.Timestamp.class),
    DATE("Date", java.sql.Timestamp.class),
    TIMESTAMP("TimeStamp", java.sql.Timestamp.class),
    GEOMETRY("Geometry", net.stemteam.datatransport.tool.IkGeometry.class),
    DATASET("DataSet", net.stemteam.datatransport.transport.DataSet.class);

    private final String name;
    private final Class javaClass;

    DataColumnType(String name, Class javaClass) {
        this.name = name;
        this.javaClass = javaClass;
    }
    
    /**
     * Возвращает название типа колонки
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Возвращает класса java для типа
     * @return 
     */
    public Class getJavaClass() {
        return javaClass;
    }
    
    /**
     * Доступ к элементу по имени
     * @param name имя
     * @return 
     */
    public static DataColumnType getByName(String name) {
        for (DataColumnType v : values()) {
            if (v.getName().equals(name)) {
                return v;
            }
        }
        return null;
    }

}
