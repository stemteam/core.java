package net.stemteam.datatransport.transport;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Назначение сего - трансляция в безопасные строки и обратно
 * 
 * (безопасные строки - это строки, которые не порушат протокол дататранспорта)
 *
 * @author medved
 */
public class StringEncoder {
       
    private static final String ED_FTM = "%1$ty%1$tm%1$td%1$tH%1$tM%1$tS";
    private static final String EDTMS_FTM = "%1$ty%1$tm%1$td%1$tH%1$tM%1$tS.";
    
    private static final String DD_FTM = "yyMMddHHmmss";
    private static final String DDTMS_FTM = "yyMMddHHmmss.SSS";
    
    private static final String DL_FTM = "yyyyMMddHHmmss";
    private static final String DLTMS_FTM = "yyyyMMddHHmmss.SSS";
    
    private static final String ENC_UTF8 = "UTF-8";

    /**
     * Преобразовывает DateTime в строку
     * @param timeStamp
     * @return
     * @throws SQLException
     */
    public static String encodeDateTime(Object timeStamp) throws SQLException {
        if (timeStamp == null)
            return null;
        java.sql.Timestamp d = null;
        if (timeStamp instanceof oracle.sql.TIMESTAMP)
            d = ((oracle.sql.TIMESTAMP)timeStamp).timestampValue(); // deprecated
        else if(timeStamp instanceof String){
//            d = decodeTimestamp((String) timeStamp);
            return (String) timeStamp;
        } else {
            d = (Timestamp) timeStamp;
        }
         // дата в строку
        int nano = d.getNanos();
        String ss = null;
        if (nano == 0){
            ss = String.format(ED_FTM, d);
        } else {
            ss = String.format("%1$ty%1$tm%1$td%1$tH%1$tM%1$tS.%1$tL", d);
// было до этого
//            ss = String.format(EDTMS_FTM, d);
//            int i = (int) Math.round(nano / 1000000.0);
//            ss += i;
        }
        return ss;
    }

    /**
     * Преобразовывает строку в DateTime
     * @param str
     * @return
     */
    public static java.sql.Timestamp decodeDateTime(String str) throws ParseException{
        if (str == null)
            return null;
        if (str.equals(""))
            return null;
        SimpleDateFormat format = null;
        if (str.length() == 12){
            format = new SimpleDateFormat(DD_FTM);
        } else {
            format = new SimpleDateFormat(DDTMS_FTM);
        }
        Date d = format.parse(str);
        java.sql.Timestamp ts = new java.sql.Timestamp(d.getTime());
        return ts;
    }

    /**
     * Преобразовывает TimeStamp в строку
     * @param timeStamp
     * @return
     * @throws SQLException
     */
    public static String encodeTimeStamp(Object timeStamp) throws SQLException {
        if (timeStamp == null)
            return null;
        java.sql.Timestamp d = null;
        if (timeStamp instanceof oracle.sql.TIMESTAMP)
            d = ((oracle.sql.TIMESTAMP)timeStamp).timestampValue(); // deprecated
        else if(timeStamp instanceof String){
//            d = decodeTimestamp((String) timeStamp);
            return (String) timeStamp;
        } else {
            d = (Timestamp) timeStamp;
        }
         // дата в строку
        int nano = d.getNanos();
        String ss = null;
        if (nano == 0){
// было до этого
//            ss = String.format(ED_FTM, d);
            ss = String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS", d);
        } else {
            ss = String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS.%1$tL", d);
// было до этого
//            ss = String.format(EDTMS_FTM, d);
//            int i = (int) Math.round(nano / 1000000.0);
//            ss += i;
        }
        return ss;
    }

    /**
     * Преобразовывает строку в TimeStamp
     * @param str
     * @return
     */
    public static java.sql.Timestamp decodeTimeStamp(String str) throws ParseException{
        if (str == null)
            return null;
        if (str.equals(""))
            return null;
        SimpleDateFormat format = null;       
        if (str.indexOf(".") == -1){
            format = new SimpleDateFormat(DL_FTM);
        } else {
            format = new SimpleDateFormat(DLTMS_FTM);
        }
        Date d = format.parse(str);
        java.sql.Timestamp ts = new java.sql.Timestamp(d.getTime());
        return ts;
    }    
    
    /**
     * Преобразовывает Date в строку
     */
    public static String encodeDate(Object date) throws SQLException {
        if (date == null)
            return null;
        if ((date instanceof java.sql.Timestamp) || (date instanceof oracle.sql.TIMESTAMP)) {
            return encodeDateTime(date); // было замечено за ResultSet getMetaData ошибочное определение типа
        } else {
            return String.format(ED_FTM, (java.sql.Date) date);
        }
    }



    /**
     * Преобразовывает строку в Date
     */
    public static java.sql.Date decodeDate(String str) throws ParseException {
        if (str == null)
            return null;
        if (str.equals(""))
            return null;

        SimpleDateFormat format = null;
        format = new SimpleDateFormat(DD_FTM);
        Date d = format.parse(str);
        return new java.sql.Date(d.getTime());
    }

    /**
     * Кодирует строку в строковое представление
     *
     * Эта казалось бы безсмысленная операция должна исключить попадание в текст
     * символов-разделителей транспорта
     *
     * @param value
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String encodeString(String value) throws UnsupportedEncodingException {
        if (value == null)
            return null;
        String s = net.stemteam.datatransport.tool.Base64.encode(value.getBytes(ENC_UTF8));
        return s;
    }

    /**
     * Раскодирует строковое представление в строку
     * @param value
     * @return
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    public static String decodeString(String value) throws UnsupportedEncodingException, IOException {
        if (value == null)
            return null;
        String s = new String(net.stemteam.datatransport.tool.Base64.decode(value), ENC_UTF8);
        return s;
    }

}
