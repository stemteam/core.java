package oracle.spatial.geometry;

import net.stemteam.datatransport.exception.ColumnNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Vector;
import oracle.sql.STRUCT;
import net.stemteam.datatransport.tool.Splitter;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.datatransport.transport.DataRecord;
import net.stemteam.datatransport.transport.DoubleEncoder;

/**
 * TODO в пакете oracle.spatial.geometry поскольку хак доступа к protected свойствам JGeometry
 * 
 *
 * В геометрии - сила, особенно еси оно гис
 * @author Roman E. Kasovskiy, medved
 */
public class OraGeometry extends JGeometry
{
    public static Object mutex = new Object();

    public OraGeometry() {
        // create a dummy rectangle JGeometry instance
        super(0., 0., 0., 0., 0);
    }

    public static oracle.sql.STRUCT store2(JGeometry geom, Connection connection) throws java.sql.SQLException {
        synchronized (mutex) {
            clearDBDescriptors();
            return JGeometry.store(geom, connection);
        }
    }

    public static JGeometry load2(oracle.sql.STRUCT struct) throws SQLException {
        synchronized (mutex) {
            clearDBDescriptors();
            return JGeometry.load(struct);
        }
    }

    public static void clearDBDescriptors() {
        synchronized (mutex) {
            geomDesc = null;
            pointDesc = null;
            elemInfoDesc = null;
            ordinatesDesc = null;
        }
    }

    /**
     * Возвращает название типа геометрии
     * @param geomType Тип геометрии (JGeometry.GTYPE_*)
     * @return строка описания геометрии
     */
    public static String GeomTypeToString(int geomType)
    {

        switch (geomType)
        {
            case JGeometry.GTYPE_POINT : return "GTYPE_POINT";
            case JGeometry.GTYPE_CURVE : return "GTYPE_CURVE";
            case JGeometry.GTYPE_POLYGON : return "GTYPE_POLYGON";
            case JGeometry.GTYPE_COLLECTION : return "GTYPE_COLLECTION";
            case JGeometry.GTYPE_MULTIPOINT : return "GTYPE_MULTIPOINT";
            case JGeometry.GTYPE_MULTICURVE : return "GTYPE_MULTICURVE";
            case JGeometry.GTYPE_MULTIPOLYGON : return "GTYPE_MULTIPOLYGON";
            default : return "Неизвестная геометрия!";
        }
    }

    // разделитель параметров строкового представления геометрии (getGeometryString)
    public static String GEOMETRY_PARAM_SEP = ":";
    // разделитель элементов в массиве геометрии
    public static String GEOMETRY_ARRAY_SEP = " ";

    /**
     * Преобразовывает геометрию в строковое представление
     *
     * формат строки:
     *
     * число_измерений тип_геометрии массив_координат
     *
     * Округление координат до 6-го знака
     *
     * Если тип геометрии - трехмерная точка с нулевой высотой - преобразовываем в двумерную точку
     *
     * @param geometry геометрия
     * @return строка
     */
    public static String getGeometryString(JGeometry geometry) throws Exception
    {
        StringBuilder geometryString = new StringBuilder();
        double[] d;
        int i;
        int dimension = geometry.getDimensions();
        int type = geometry.getType();
        boolean is_first = false;
        double coord;
        // число измерений
        geometryString.append(dimension);
        // тип геометрии
        geometryString.append(GEOMETRY_PARAM_SEP);
        geometryString.append(geometry.getType());
        // точечная часть геометрии (x,y,z)
        geometryString.append(GEOMETRY_PARAM_SEP);
        if (type == JGeometry.GTYPE_POINT)
        {
            d = geometry.getPoint();
            if (d != null)
            {
                is_first = true;
                for (i=0; i<dimension; i++)
                {
                    // округление
                    coord = d[i];
                    coord = Math.round(coord * 1000000.0) / 1000000.0;
                    if (is_first)
                    {
                        geometryString.append(String.valueOf(coord));
                        is_first = false;
                    } else {
                        if ((i == 2) & (coord == 0.0))
                        {
                            // Если тип геометрии - трехмерная точка с нулевой высотой - преобразовываем в двумерную точку
                            geometryString = geometryString.replace(0, 1, "2");
                        } else {
                            geometryString.append(GEOMETRY_ARRAY_SEP);
                            geometryString.append(String.valueOf(coord));
                        }
                    }
                }
            }
        } else if (type == JGeometry.GTYPE_POLYGON) { // разбор полигональной геометрии
            d = geometry.getOrdinatesArray();
            if (d != null)
            {
                is_first = true;
                i = 0;
                for (double xcoord : d)
                {
                    i++;
                    // округляем координаты до 6-го знака
                    xcoord = Math.round(xcoord * 1000000.0) / 1000000.0;
                    if (is_first)
                    {
                        geometryString.append(String.valueOf(xcoord));
                        is_first = false;
                    } else {
                        geometryString.append(GEOMETRY_ARRAY_SEP);
                        geometryString.append(String.valueOf(xcoord));
                    }
                }
            }
        } else if (type == JGeometry.GTYPE_CURVE) { // разбор полилинии геометрии
            d = geometry.getOrdinatesArray();
            if (d != null)
            {
                is_first = true;
                i = 0;
                for (double xcoord : d)
                {
                    i++;
                    // округляем координаты до 6-го знака
                    xcoord = Math.round(xcoord * 1000000.0) / 1000000.0;
                    if (is_first)
                    {
                        geometryString.append(String.valueOf(xcoord));
                        is_first = false;
                    } else {
                        geometryString.append(GEOMETRY_ARRAY_SEP);
                        geometryString.append(String.valueOf(xcoord));
                    }
                }
            }
        } else {
            throw new Exception("Не поддерживаемый тип геометрии.");
        }
        return geometryString.toString();
    }

    /**
     * Преобразовывает структуру в геометрию
     * @param geometrySTRUCT
     * @return JGeometry
     * @throws SQLException
     */
    public static JGeometry getGeometry(Object geometrySTRUCT) throws SQLException{
        STRUCT struct = (oracle.sql.STRUCT) geometrySTRUCT;
        JGeometry geometry = JGeometry.load(struct);
        return geometry;
    }

    /**
     * Распаковывает строку в геометрию
     * @param geometryString
     * @return JGeometry
     */
    public static JGeometry getGeometryObject(String geometryString) throws Exception{
        JGeometry geometry = null;
        double X = 0;
        double Y = 0;
        double Z = 0;
        Vector<String> pointList = null;
        int[] polyElemInfo = {1,1003,1}; // для полигонов (клиент не передает)
        int[] curveElemInfo = {1,2,1}; // для кривых  (клиент не передает)
        double[] ordinates = null;
        int pointCount = 0;
        if (geometryString == null)
            return null;
        // парсим
        Vector<String> geometryList = Splitter.Split(geometryString, GEOMETRY_PARAM_SEP);
        // SRID (клиент не передает, присваиваем по умолчанию)
        Integer srid = 4326;
        // размерность
        Integer dimension = Integer.valueOf(geometryList.get(0));
        // тип геометрии
        Integer type = Integer.valueOf(geometryList.get(1));
        // точечная часть
        if (type == JGeometry.GTYPE_POINT)
        {
            pointList = Splitter.Split(geometryList.get(2), GEOMETRY_ARRAY_SEP);
            pointCount = pointList.size();
            if (pointCount != dimension)
                throw new Exception("Некорректная геометрия: размерность и кол-во координат не совпадает.");
            if (pointCount == 3){
                X = DoubleEncoder.decodeDouble(pointList.get(0));
                Y = DoubleEncoder.decodeDouble(pointList.get(1));
                Z = DoubleEncoder.decodeDouble(pointList.get(2));
            } else if (pointCount == 2) {
                X = DoubleEncoder.decodeDouble(pointList.get(0));
                Y = DoubleEncoder.decodeDouble(pointList.get(1));
            } else if (pointCount == 1) {
                X = DoubleEncoder.decodeDouble(pointList.get(0));
            }
            geometry = new JGeometry(type, srid, X, Y, Z, null, null);
        } else if (type == JGeometry.GTYPE_POLYGON) { // полигональная геометрия
            pointList = Splitter.Split(geometryList.get(2), GEOMETRY_ARRAY_SEP);
            pointCount = pointList.size();
            ordinates = new double[pointCount];
            for (int i=0; i<pointCount; i++) {
                ordinates[i] = DoubleEncoder.decodeDouble(pointList.get(i));
            }
            geometry = new JGeometry(type, srid, X, Y, Z, polyElemInfo, ordinates);
        } else if (type == JGeometry.GTYPE_CURVE) { // кривая
            pointList = Splitter.Split(geometryList.get(2), GEOMETRY_ARRAY_SEP);
            pointCount = pointList.size();
            ordinates = new double[pointCount];
            for (int i=0; i<pointCount; i++) {
                ordinates[i] = DoubleEncoder.decodeDouble(pointList.get(i));
            }
            geometry = new JGeometry(type, srid, X, Y, Z, curveElemInfo, ordinates);
        } else {
            throw new Exception("Не поддерживаемый тип геометрии.");
        }
        return geometry;
    }

    //final static double RoundPrec = 1000000;

    public static JGeometry getGeometryFromDataset(DataSet set) throws ColumnNotFoundException
    {
        int[] elemInfo = {1,1003,1};

        Vector<DataRecord> records = set.getRecords();
        //double[] ordinates = {39.001, 45.001, 39.005, 45.005, 39.007, 45.008, 39.001, 45.001};
        double[] ordinates = new double[records.size() * 2];
        int i = 0;
        for (DataRecord record : records)
        {
            //ordinates[i++] = Math.round(record.getDouble(0) * RoundPrec) / RoundPrec;
            //ordinates[i++] = Math.round(record.getDouble(1) * RoundPrec) / RoundPrec;
            ordinates[i++] = record.getDouble(0);
            ordinates[i++] = record.getDouble(1);
        }
        JGeometry polygon2D = new JGeometry(JGeometry.GTYPE_POLYGON, 4326, elemInfo, ordinates);
        return polygon2D;
    }
    
    /**
     * Формирует полилинию из датасета
     * @param set датасет
     * @return полилиния
     */
    public static JGeometry getPolylineFromDataset(DataSet set) throws ColumnNotFoundException
    {
        int[] elemInfo = {1,2,1};

        Vector<DataRecord> records = set.getRecords();
        //double[] ordinates = {39.001, 45.001, 39.005, 45.005, 39.007, 45.008, 39.001, 45.001};
        double[] ordinates = new double[records.size() * 2];
        int i = 0;
        for (DataRecord record : records)
        {
            //ordinates[i++] = Math.round(record.getDouble(0) * RoundPrec) / RoundPrec;
            //ordinates[i++] = Math.round(record.getDouble(1) * RoundPrec) / RoundPrec;
            ordinates[i++] = record.getDouble(0);
            ordinates[i++] = record.getDouble(1);
        }
        JGeometry polygon2D = new JGeometry(JGeometry.GTYPE_CURVE, 4326, elemInfo, ordinates);
        return polygon2D;
    }

}
